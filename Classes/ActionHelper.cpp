#include "ActionHelper.h"
#include "ShakeAction.h"

Action* ActionHelper::ActionMoveAroundPoint(float duration, Vec2 point)
{
	ccBezierConfig bezier;
	bezier.controlPoint_1 = Vec2(0, 200);
	bezier.controlPoint_2 = Vec2(300, 400);
	bezier.endPosition = Vec2(600, 600);

	return BezierTo::create(duration, bezier);
}

Action* ActionHelper::Shaking(float time, float strength)
{
	return ShakeAction::create(time, strength);
}