#ifndef __ACTIONHELPER_H__
#define __ACTIONHELPER_H__

#include "Config.h"

class ActionHelper
{
public:
	static Action* ActionMoveAroundPoint(float duration, Vec2 point);
	static Action* Shaking(float time, float strength);
};

#endif