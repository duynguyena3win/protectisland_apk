#include "AircraftCarrierShip.h"

AircraftCarrierShip* AircraftCarrierShip::create(Layer* layer)
{
	auto rk = new AircraftCarrierShip();
	rk->m_pLayer = layer;
	if (rk->init())
	{
		return rk;
	}
	CC_SAFE_DELETE(rk);
	return NULL;
}

bool AircraftCarrierShip::init()
{
	if (!GameEntity::init())
	{
		return false;
	}

	m_pBodyEntity = Sprite::create(SPRITE_ENEMIES_AIRCRAFTCARRIERSHIP);
	m_pNode->addChild(m_pBodyEntity);

	m_iID = ID_AIRCRAFTCARRIERSHIP;

	m_pNode->setPosition(Vec2(-2000, -2000));
	return true;
}

void AircraftCarrierShip::setRotate(Vec2 point)
{

}

Action* AircraftCarrierShip::createAction(Vec2 target)
{
	return NULL;
}

void AircraftCarrierShip::setState(int state)
{
	if (m_pState == state)
		return;

	m_pState = state;

	switch (state)
	{
	case IDLE_ENTITY:
		setActive(false);
		break;

	case ACTIVE_ENTITY:
		setActive(true);
		break;

	case FINISHED_ENTITY:
		setActive(false);
		break;

	case DESTROY_ENTITY:
		setActive(false);
		break;
	default:
		break;
	}
}

void AircraftCarrierShip::destroy()
{
	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	//
}

void AircraftCarrierShip::finish()
{
	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	//
}

void AircraftCarrierShip::shooting()
{

}