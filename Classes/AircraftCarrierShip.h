#ifndef __AIRCRAFTCARRIERSHIP_H__
#define __AIRCRAFTCARRIERSHIP_H__

#include "GameEntity.h"

class AircraftCarrierShip : public GameEntity
{
public:
	static AircraftCarrierShip* create(Layer* layer);
	virtual bool init();
	void setRotate(Vec2 point);
	Action* createAction(Vec2 target = Vec2(0,0));
	void setState(int state);
	virtual void destroy();
	virtual void finish();
	virtual void shooting();
};

#endif