#ifndef __BADSTATE_H__
#define __BADSTATE_H__

#include "Config.h"
#include "ItemState.h"

class BadState : public ItemState
{
public:
	BadState();
	~BadState();
};

#endif