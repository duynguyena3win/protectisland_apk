#include "BarItems.h"
#include "Utils.h"
#include "MapGame.h"
#include "UserProfile.h"

BarItems* BarItems::s_pInstance = 0;

BarItems* BarItems::getInstance(Layer* layer)
{
	if (!s_pInstance && layer)
	{
		s_pInstance = new BarItems();
		s_pInstance->m_pLayer = layer;
		s_pInstance->m_pZOrder = LLG_HUBMENU;

		if (!s_pInstance->init())
		{
			CC_SAFE_DELETE(s_pInstance);
			return NULL;
		}
	}
	return s_pInstance;
}

bool BarItems::initMenuTargetItem()
{
	auto menuItems = m_pNode->getChildByName("MENU_ITEMS");
	if (menuItems)
		menuItems->setVisible(false);

	auto menuTargetItems = m_pNode->getChildByName("MENU_TARGETITEMS");
	if (!menuTargetItems)
	{
		vector<Vec2> listPos = Utils::getPositionRound(m_pNode->getContentSize(), 2);
		cocos2d::Vector<MenuItem*> listButton;

		//Button Sell
		auto btnSell = Utils::addButton(SPRITE_BUTTON_SELL_ENABLE, SPRITE_BUTTON_SELL_DISABLE, SPRITE_BUTTON_SELL_DISABLE, [this](Ref* ref) {
			MenuItem* btn = (MenuItem*) ref;
			if (m_pTargetItem)
				_delegate->targetItemCallback(btn->getTag(), m_pTargetItem);
		});
		btnSell->setPosition(listPos[0]);
		btnSell->setTag(BTG_SELL);
		btnSell->setScale(SCALE_BARITEM_BUTTON_ICON_DISABLE);
		listButton.pushBack((MenuItem*)btnSell);

		//Button Upgrade
		auto btnUpgrade = Utils::addButton(SPRITE_BUTTON_UPGRADE_ENABLE, SPRITE_BUTTON_UPGRADE_DISABLE, SPRITE_BUTTON_UPGRADE_DISABLE, [this](Ref* ref) {
			MenuItem* btn = (MenuItem*)ref;
			if (m_pTargetItem)
				_delegate->targetItemCallback(btn->getTag(), m_pTargetItem);
		});
		btnUpgrade->setPosition(listPos[1]);
		btnUpgrade->setTag(BTG_UPGRADE);
		btnUpgrade->setScale(SCALE_BARITEM_BUTTON_ICON_DISABLE);
		listButton.pushBack((MenuItem*)btnUpgrade);

		auto menu = Menu::createWithArray(listButton);
		menu->setPosition(Vec2::ZERO);
		m_pNode->addChild(menu, 1, "MENU_TARGETITEMS");
	}
	else
		menuTargetItems->setVisible(true);
	return true;
}

void BarItems::loadButtonMenu()
{
	std::vector<int> btn_Items;
	auto myProfile = UserProfile::Get();

	switch (m_iTypeMenu)
	{
	case MI_2X2:
		if (myProfile->getLevelById(ID_CANON203MM) > 0)
			btn_Items.push_back(ID_CANON203MM);
	case MI_1X2:
	case MI_2X1:
		if (myProfile->getLevelById(ID_CANON37MM) > 0)
			btn_Items.push_back(ID_CANON37MM);
		if (myProfile->getLevelById(ID_ROCKETS300) > 0)
			btn_Items.push_back(ID_ROCKETS300);
	case MI_1X1:
		if (myProfile->getLevelById(ID_FOGGUN) > 0)
			btn_Items.push_back(ID_FOGGUN);
		if (myProfile->getLevelById(ID_MACHINEGUN) > 0)
			btn_Items.push_back(ID_MACHINEGUN);
		break;
	case MI_ITEM:
		if (initMenuTargetItem())
			return;
		break;
	}

	auto menuItems = m_pNode->getChildByName("MENU_TARGETITEMS");
	if (menuItems)
		menuItems->setVisible(false);

	auto menuLatest = m_pNode->getChildByName("MENU_ITEMS");

	vector<Vec2> listPos = Utils::getPositionRound(m_pNode->getContentSize(), btn_Items.size());

	cocos2d::Vector<MenuItem*> listButton;
	for (int i = 0; i < btn_Items.size(); i++)
	{
		MenuItem* temp = 0;
		switch (btn_Items[i])
		{
		case ID_FOGGUN:
		{
						  if (menuLatest)
							  temp = (MenuItem*)menuLatest->getChildByName("MENU_ITEM_FOGGUN");
						  if (!temp)
						  {
							  temp = Utils::addButton(LIST_ICON_NORMAL[4], LIST_ICON_NORMAL[4], LIST_ICON_NORMAL[4], [this](Ref* ref)
							  {
								  auto obj = (MenuItem*)ref;
								  auto ind = obj->getTag();
								  _delegate->buildCallback(ind);
							  });
							  temp->setScale(SCALE_BARITEM_BUTTON_ICON_DISABLE);
							  temp->setName("MENU_ITEM_FOGGUN");
							  temp->setTag(ID_FOGGUN);
							  listButton.pushBack((MenuItem*)temp);
						  }
						  else
							  listButton.pushBack((MenuItem*)temp);
		}
			break;
		case ID_MACHINEGUN:
		{
							  if (menuLatest)
								  temp = (MenuItem*)menuLatest->getChildByName("MENU_ITEM_MACHINEGUN");
							  if (!temp)
							  {
								  temp = Utils::addButton(LIST_ICON_NORMAL[0], LIST_ICON_NORMAL[0], LIST_ICON_NORMAL[0], [this](Ref* ref)
								  {
									  auto obj = (MenuItem*)ref;
									  auto ind = obj->getTag();
									  _delegate->buildCallback(ind);
								  });
								  temp->setScale(SCALE_BARITEM_BUTTON_ICON_DISABLE);
								  temp->setName("MENU_ITEM_MACHINEGUN");
								  temp->setTag(ID_MACHINEGUN);
								  listButton.pushBack((MenuItem*)temp);
							  }
							  else
								  listButton.pushBack((MenuItem*)temp);
		}
			break;

		case ID_CANON37MM:
		{
							 if (menuLatest)
								 temp = (MenuItem*)menuLatest->getChildByName("MENU_ITEM_CANON37MM");
							 if (!temp)
							 {
								 temp = Utils::addButton(LIST_ICON_NORMAL[1], LIST_ICON_NORMAL[1], LIST_ICON_NORMAL[1], [this](Ref* ref)
								 {
									 auto obj = (MenuItem*)ref;
									 auto ind = obj->getTag();
									 _delegate->buildCallback(ind);
								 });
								 temp->setScale(SCALE_BARITEM_BUTTON_ICON_DISABLE);
								 temp->setName("MENU_ITEM_CANON37MM");
								 temp->setTag(ID_CANON37MM);
								 listButton.pushBack((MenuItem*)temp);
							 }
							 else
								 listButton.pushBack((MenuItem*)temp);
		}
			break;

		case ID_CANON203MM:
		{
							  if (menuLatest)
								  temp = (MenuItem*)menuLatest->getChildByName("MENU_ITEM_CANON203MM");
							  if (!temp)
							  {
								  temp = Utils::addButton(LIST_ICON_NORMAL[2], LIST_ICON_NORMAL[2], LIST_ICON_NORMAL[2], [this](Ref* ref)
								  {
									  auto obj = (MenuItem*)ref;
									  auto ind = obj->getTag();
									  _delegate->buildCallback(ind);
								  });
								  temp->setScale(SCALE_BARITEM_BUTTON_ICON_DISABLE);
								  temp->setName("MENU_ITEM_CANON203MM");
								  temp->setTag(ID_CANON203MM);
								  listButton.pushBack((MenuItem*)temp);
							  }
							  else
								  listButton.pushBack((MenuItem*)temp);
		}
			break;

		case ID_ROCKETS300:
		{
							  if (menuLatest)
								  temp = (MenuItem*)menuLatest->getChildByName("MENU_ITEM_ROCKETS300");
							  if (!temp)
							  {
								  temp = Utils::addButton(LIST_ICON_NORMAL[3], LIST_ICON_NORMAL[3], LIST_ICON_NORMAL[3], [this](Ref* ref)
								  {
									  auto obj = (MenuItem*)ref;
									  auto ind = obj->getTag();
									  _delegate->buildCallback(ind);
								  });
								  temp->setScale(SCALE_BARITEM_BUTTON_ICON_DISABLE);
								  temp->setName("MENU_ITEM_ROCKETS300");
								  temp->setTag(ID_ROCKETS300);
								  listButton.pushBack((MenuItem*)temp);
							  }
							  else
								  listButton.pushBack((MenuItem*)temp);
		}
			break;
		}

		temp->setPosition(listPos[i]);
	}
	
	if (menuLatest)
		m_pNode->removeChildByName("MENU_ITEMS", false);

	auto menu = Menu::createWithArray(listButton);
	menu->setPosition(Vec2::ZERO);
	m_pNode->addChild(menu, 1, "MENU_ITEMS");
}

bool BarItems::init()
{
	m_pNode = Node::create();
	m_pNode->setUserData(this);
	m_pNode->setTag(TAG_DEFAULT);
	m_pLayer->addChild(m_pNode, m_pZOrder);
	m_iSelectIndex = -1;

	auto table = Sprite::create(SPRITE_HUDGAME_TABLE);
	auto tableSize = table->getContentSize();
	m_pNode->setContentSize(tableSize);
	m_pNode->addChild(table);

	m_pTargetItem = 0;
	return true;
}

void BarItems::setMenuState(bool isEnable)
{
	float value = (isEnable) ? SCALE_BARITEM_BUTTON_ICON_ENABLE : SCALE_BARITEM_BUTTON_ICON_DISABLE;
	for (auto button : m_pMenuItems->getChildren())
	{
		button->setScale(value);
	}
}

void BarItems::setPosition(Vec2 pos)
{
	m_pNode->setPosition(pos);
}

BarItems::BarItems()
{

}

void BarItems::openMenu()
{
	m_pNode->setScale(0.1f);
	m_pNode->setVisible(true);
	m_bEnable = true;
	auto func = CallFunc::create([this]()
	{
		m_pNode->setScale(1.0f);
		m_pNode->setVisible(true);
	});

	auto scale = EaseOut::create(ScaleTo::create(0.5f, 1.0f), 2.0f);
	m_pNode->runAction(Sequence::create(scale, func, NULL));
}

void BarItems::closeMenu()
{
	m_bEnable = false;
	m_pNode->setScale(1.0f);
	m_pNode->setVisible(true);

	auto scale = EaseIn::create(ScaleTo::create(0.5f, 0.1f), 2.0f);
	auto func = CallFunc::create([this]()
	{
		m_pNode->setScale(0.1f);
		m_pNode->setVisible(false);
	});

	m_pNode->runAction(Sequence::create(scale, func, NULL));
}