#ifndef __BARITEMS_H__
#define __BARITEMS_H__

#include "Config.h"
#include "LevelInfo.h"

class MapGame;
class GameEntity;

class BarItems
{
public:
	CC_SYNTHESIZE(MapGame*, _delegate, Delegate);

	static BarItems* getInstance(Layer* layer = 0);
	void setPosition(Vec2 pos);
	void setMenuState(bool isEnable);
	void setTypeMenu(int type) { m_iTypeMenu = type; }
	void loadButtonMenu();
	bool getState() { return m_bEnable; }
	void openMenu();
	void closeMenu();
	void setState(bool isEnable) { m_bEnable = isEnable; }
	void stopAllActions() { m_pNode->stopAllActions(); }
	bool initMenuTargetItem();
	void setTargetItem(GameEntity* target) { m_pTargetItem = target; }

private:
	virtual bool init();
	BarItems();

private:
	static BarItems* s_pInstance;
	Node* m_pNode;
	bool m_bEnable;
	Layer* m_pLayer;
	int m_iTypeMenu;
	int m_pZOrder;
	int m_iSelectIndex;
	Menu* m_pMenuItems;
	GameEntity* m_pTargetItem;
};

#endif
