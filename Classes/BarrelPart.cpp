#include "BarrelPart.h"

BarrelPart*	BarrelPart::create(Node* parent)
{
	auto cp = new BarrelPart();
	cp->m_pParent = parent;
	cp->m_pZOrder = LLG_ENTITY_GROUND;
	if (cp->init())
	{
		return cp;
	}
	CC_SAFE_DELETE(cp);
	return NULL;
}

bool	BarrelPart::init()
{
	if (!RootPart::init())
		return false;
	m_iType = EP_BARREL;
	/* Add sprite for body */
	auto barrel = Sprite::create(SPRITE_ENEMIES_COASTGUARD_BARRET);
	m_pNode->addChild(barrel, 1, "COASTGUARD_BARRET_1");
	return true;
}

void	BarrelPart::setTarget(GameEntity* obj)
{

}

void	BarrelPart::shoot()
{

}