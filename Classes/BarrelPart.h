#ifndef __BARRELPART_H__
#define __BARRELPART_H__

#include "Config.h"
#include "RootPart.h"

#define SPRITE_ENEMIES_COASTGUARD_BARRET	"Enemies/Ene_CoastGuard_Barret.png"

class GameEntity;

class BarrelPart	:	public RootPart
{
public:
	static	BarrelPart*	create(Node* parent);
	virtual	bool	init();
	virtual	void	setTarget(GameEntity* obj);
	void			shoot();

protected:
	
};

#endif