#include "BasePopUp.h"

bool BasePopUp::init()
{
	m_pNode = Node::create();
	m_pNode->setUserData(this);
	m_pNode->setTag(TAG_DEFAULT);

	m_pLayer->addChild(m_pNode, m_pZOrder, m_sKey);
	return true;
}

void BasePopUp::setNewLayer(Layer* newLayer)
{
	m_pLayer->removeChild(m_pNode, false);
	m_pLayer = new Layer;
	m_pLayer->addChild(m_pNode, m_pZOrder, m_sKey);
}

void BasePopUp::showPopUp()
{
	m_pNode->setScale(0.1f);
	m_pNode->setVisible(true);
	auto show = EaseBackOut::create(ScaleTo::create(0.5f, 1.0f));
	m_pNode->runAction(show);
}

void BasePopUp::closePopUp()
{
	auto show = EaseBackIn::create(ScaleTo::create(0.5f, 0.1f));
	auto func = CallFunc::create([this]()
	{
		m_pNode->setVisible(false);
		m_pNode->setScale(1.0f);
	});
	m_pNode->runAction(Sequence::create(show, func, NULL));
}