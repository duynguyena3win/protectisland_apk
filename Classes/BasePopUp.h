#ifndef __BASEPOPUP_H__
#define __BASEPOPUP_H__

#include "Config.h"
#include "cocostudio\CocoStudio.h"
#include "ui\UIButton.h"
#include "ui\UIText.h"

class BasePopUp
{
public:
	void			setNewLayer(Layer* newLayer);
	virtual	bool	init();
	virtual	bool	setData(std::map<string, string> inforPopUp = std::map<string, string>()) = 0;
	void			setPosition(Vec2 pos) { m_pNode->setPosition(pos); }
	Vec2			getPosition()	{ return m_pNode->getPosition(); }
	void			showPopUp();
	void			closePopUp();
	int	getType()	{ return m_pType; }
	virtual	void	OnEvent(cocos2d::Ref *ref) = 0;
	Layer*			getLayer() { return m_pLayer; }
protected:
	int		m_pType;

	Layer*	m_pLayer;
	Node*	m_pNode;
	int		m_pZOrder;
	string	m_sKey;
};

#endif // !
