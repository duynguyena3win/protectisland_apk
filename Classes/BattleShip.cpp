#include "BattleShip.h"
#include "BodyPart.h"
#include "BarrelPart.h"

#include "ItemState.h"
#include "Utils.h"

BattleShip*	BattleShip::create(Layer* layer)
{
	auto cp = new BattleShip();
	cp->m_pLayer = layer;
	cp->m_iZOrder = LLG_ENTITY_GROUND;
	if (cp->init())
	{
		return cp;
	}
	CC_SAFE_DELETE(cp);
	return NULL;
}

bool	BattleShip::init()
{
	if (!Entity::init())
		return false;
	m_iID = ID_COASTGUARD;
	m_pStateItem = new ItemState(Utils::getItemById(m_iID));

	m_pListBody.push_back(BodyPart::create(m_pNode));
	m_pListBarrel.push_back(BarrelPart::create(m_pNode));

	m_pListBarrel[0]->setPosition(Vec2(0, 10));
	return true;
}

void	BattleShip::setState(int state)
{
	if (m_iState == state)
		return;

	m_iState = state;

	switch (state)
	{
	case IDLE_ENTITY:
	{
		m_pNode->setPosition(Vec2(-2000, -2000));
		setActive(false);
		break;
	}
	case ACTIVE_ENTITY:
		setActive(true);
		break;

	case FINISHED_ENTITY:
	case DESTROY_ENTITY:
	{
		LOGI("Destroy Fishing Boat Object");
		m_pNode->setPosition(Vec2(-2000, -2000));
		setActive(false);
	}
	break;
	default:
		break;
	}
}

void	BattleShip::setTarget(Entity* obj)
{

}

Vec2	BattleShip::getPositionBarrel()
{
	auto outPut = m_pListBarrel[0]->getPosition();
	outPut.rotate(Vec2::ZERO, CC_DEGREES_TO_RADIANS(-m_pNode->getRotation()));
	return m_pNode->getPosition() + outPut;
}

void	BattleShip::setRotation(float degree)
{
	m_pNode->setRotation(degree);
}

void	BattleShip::update(float deltaTime)
{
	switch (m_iCurrentAction)
	{
	case AE_NONE:
		break;
	case AE_MOVING:
	{
		auto disCurToTar = m_vCurrentPositon.distance(m_vMovingPoint);
		if (disCurToTar > m_fOldDistance)
		{
			auto vecDirection = m_vMovingPoint - m_vCurrentPositon;
		}
	}
		break;
	case AE_SHOOTING:
		break;
	}
}