#ifndef __BATTLESHIP_H__
#define __BATTLESHIP_H__

#include "Config.h"
#include "Entity.h"


class BattleShip : public Entity
{
public:
	static	BattleShip*	create(Layer* layer);
	virtual	bool	init();
	void			setState(int state) override;
	void			setTarget(Entity* obj)	override;
	Vec2			getPositionBarrel() override;
	void			setRotation(float degree) override;
	void			update(float deltaTime)	override;

protected:

};

#endif