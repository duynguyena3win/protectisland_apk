#include "BodyPart.h"

BodyPart*	BodyPart::create(Node* parent)
{
	auto cp = new BodyPart();
	cp->m_pParent = parent;
	cp->m_pZOrder = LLG_ENTITY_GROUND;
	if (cp->init())
	{
		return cp;
	}
	CC_SAFE_DELETE(cp);
	return NULL;
}

bool	BodyPart::init()
{
	if (!RootPart::init())
		return false;
	m_iType = EP_BODY;

	auto body = Sprite::create(SPRITE_ENEMIES_FISHINGSHIP);
	m_pNode->addChild(body, 1);

	return true;
}