#ifndef __BODYPART_H__
#define __BODYPART_H__

#include "Config.h"
#include "RootPart.h"

class BodyPart	:	public RootPart
{
public:
	static	BodyPart*	create(Node* parent);
	virtual	bool	init();

protected:
};

#endif