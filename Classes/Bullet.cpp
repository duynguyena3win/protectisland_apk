#include "Bullet.h"
#include "EffectFactory.h"

Bullet* Bullet::create(Layer* layer)
{
	auto cp = new Bullet();
	cp->m_pLayer = layer;
	cp->m_pZOrder = LLG_ENTITY_UNDERGROUND;
	if (cp->init())
	{
		return cp;
	}
	CC_SAFE_DELETE(cp);
	return NULL;
}

bool Bullet::init()
{
	if (!GameEntity::init())
	{
		return false;
	}
	m_pStateItem = new ItemState();
	m_pStateItem->m_pDamge = 10;

	auto body = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(KEY_BULLET_TYPE(0)));
	body->setPosition(Vec2::ZERO);
	body->setAnchorPoint(Vec2(0.5,0.5));
	m_pNode->addChild(body, 1, "BULLET_BODY");
	m_pInfo = 0;
	m_pState = IDLE_ENTITY;
	m_iID = ID_BULLET;
	setSprite(ID_SMALLBULLET);
	setActive(false);
	return true;
}

void Bullet::setRotate(Vec2 point)
{
	auto pos = getPosition();
	auto vec_huyen = point - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));
	auto actionRotate = RotateTo::create(1.0f, goc);
	auto func = CallFunc::create([this]()
	{
		//m_pTargetEnemy->takeDamage(getDamage(), m_pTypeBullet);
	});
	log("Machine Gun set rotate");
	m_pNode->runAction(Sequence::create(actionRotate, func, NULL));
}

Action* Bullet::createAction(Vec2 target)
{
	m_pNode->setVisible(true);
	m_pTargetPoint = target;
	// calculate degree
	auto pos = m_pNode->getPosition();
	auto vec_huyen = m_pTargetPoint - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));
	m_pNode->setRotation(goc);
	//
	auto distance = m_pTargetPoint.distance(pos);
	auto moving = EaseIn::create(MoveTo::create(distance / m_pStateItem->m_pSpeedMove, m_pTargetPoint), 2.0f);
	auto func = CallFunc::create([this](){
		exploding();
	});
	auto action = Sequence::create(moving, func, NULL);
	return action;
}

void Bullet::setState(int state)
{
	if (m_pState == state)
		return;

	m_pState = state;

	switch (state)
	{
	case IDLE_ENTITY:
		setActive(false);
		break;

	case ACTIVE_ENTITY:
		setActive(true);
		break;

	case FINISHED_ENTITY:
	case DESTROY_ENTITY:
		stopAllActions();
		setActive(false);
		break;
	default:
		break;
	}
}

void Bullet::setSprite(int id)
{
	m_pTypeBullet = id;
	((Sprite*)m_pNode->getChildByName("BULLET_BODY"))->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(Utils::getSpriteNameBullet(m_pTypeBullet)));
}

void Bullet::exploding()
{
	EffectEntity* effectItem = 0;
	switch (m_pTypeBullet)
	{
	case ID_SMALLBULLET:
		effectItem = EffectFactory::getInstance()->getObject(ID_SMALLBULLET);
		break;
	case ID_FOGBULLET:
		effectItem = EffectFactory::getInstance()->getObject(ID_FOGBULLET);
		effectItem->setDelegate(m_pTargetEnemy);
		break;
	case ID_CANONBULLET:
		effectItem = EffectFactory::getInstance()->getObject(ID_CANONBULLET);
		break;
	case ID_ROCKET:
		break;
	}
	if (effectItem)
		effectItem->setPosition(m_pNode->getPosition());

	if (m_pTargetEnemy)
		m_pTargetEnemy->takeDamage(getDamage(), m_pTypeBullet);

	setState(FINISHED_ENTITY);
}

void Bullet::runAction(Action* action)
{
	setState(ACTIVE_ENTITY);
	GameEntity::runAction(action);
}

void Bullet::shooting()
{

}

void Bullet::destroy()
{

}


void Bullet::finish()
{

}