#ifndef __BULLET_H__
#define __BULLET_H__

#include "GameEntity.h"

class Bullet : public GameEntity
{
public:
	static Bullet* create(Layer* layer);
	virtual bool init();
	void setRotate(Vec2 point);
	Action* createAction(Vec2 target=NULL);
	virtual void setState(int state);
	void exploding();
	void runAction(Action* action);
	virtual void destroy();
	virtual void shooting();
	virtual void finish();
	bool isInsideArea(GameEntity* item) { return false; }
	void setSprite(int strSprite);

protected:
	int m_pTypeBullet;
};

#endif