#ifndef __CANON203MM_H__
#define __CANON203MM_H__

#include "GameEntity.h"

#define SPRITE_WEAPON_CANON203MM_BODY		"Weapons/Icon_Canon203mm_Body.png"
#define SPRITE_WEAPON_CANON203MM_BARRET		"Weapons/Icon_Canon203mm_Barret.png"


class Canon203mm : public GameEntity
{
public:
	static Canon203mm* create(Layer* layer);
	virtual bool init();
	void	setRotate(Vec2 point);
	Action* createAction(Vec2 target = Vec2(0,0));
	void	setState(int state);
	void	attack(bool isAttack);
	
	bool	takeDamage(float damage, int typeDamge);
	bool	isHaveTarget();
	void	setTarget(GameEntity* target) override;

	virtual void shooting();
	virtual void destroy();
	virtual void finish();
};

#endif