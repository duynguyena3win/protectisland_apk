#include "Canon37mm.h"
#include "SpineHelper.h"
#include "EntityFactory.h"
#include "Utils.h"

Canon37mm* Canon37mm::create(Layer* layer)
{
	auto cp = new Canon37mm();
	cp->m_pZOrder = LLG_ENTITY_GROUND;
	cp->m_pLayer = layer;
	if (cp->init())
	{
		return cp;
	}
	CC_SAFE_DELETE(cp);
	return NULL;
}

bool Canon37mm::init()
{
	if (!GameEntity::init())
	{
		return false;
	}
	// Load body
	m_pBodyEntity = Sprite::create(SPRITE_WEAPON_CANON37MM_BODY);
	m_pNode->addChild(m_pBodyEntity);

	// Node of Barret
	auto noteBarret = Node::create();

	// Load base
	auto base = Sprite::create(SPRITE_WEAPON_CANON37MM_BASEBARRET);
	noteBarret->addChild(base, 1);
	noteBarret->setContentSize(base->getContentSize());

	auto sizeBase = noteBarret->getContentSize();

	//Load barrel 1
	auto barrel1 = Sprite::create(SPRITE_WEAPON_CANON37MM_BARRET);
	barrel1->setAnchorPoint(Vec2(0.5, 0));
	barrel1->setPosition(1.2 * sizeBase.width / 5, 0);
	m_pBarrelEntities.pushBack(barrel1);
	noteBarret->addChild(m_pBarrelEntities.at(0), 0);

	//Load barrel 2
	auto barrel2 = Sprite::create(SPRITE_WEAPON_CANON37MM_BARRET);
	barrel2->setAnchorPoint(Vec2(0.5, 0));
	barrel2->setPosition(-1.2 * sizeBase.width / 5, 0);
	m_pBarrelEntities.pushBack(barrel2);
	noteBarret->addChild(m_pBarrelEntities.at(1), 0);

	m_pNode->addChild(noteBarret, 1, "CANON37MM_BASEBARRET");

	// Load effect be destroy
	SpineHelper::getInstance()->lazyInit(SPRITE_PREFIX_EFFECT, "Exploding");
	m_pDestroy = SkeletonAnimation::createWithFile(
		SpineHelper::getInstance()->_dataFile, SpineHelper::getInstance()->_atlasFile, SCALE_EFFECT_EXPLODING);
	m_pDestroy->setVisible(false);
	m_pNode->addChild(m_pDestroy, 5);

	// Infor of Game
	m_pInfo = EntityInfo::create();
	m_pLayer->addChild(m_pInfo, LLG_ENTITY_GROUND);

	m_iID = ID_CANON37MM;
	m_pStateItem = new ItemState(Utils::getItemById(m_iID));
	
	m_bIsAttack = false;
	m_pState = IDLE_ENTITY;
	setActive(false);

	m_pNode->setPosition(Vec2(-2000, -2000));
	return true;
}

void Canon37mm::setRotate(Vec2 point)
{
	auto pos = getPosition();
	auto vec_huyen = point - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));
	m_pRotBase = goc;
	auto actionRotate = RotateTo::create(1.0f, goc);

	m_pNode->runAction(actionRotate);
}

Action* Canon37mm::createAction(Vec2 target)
{
	return NULL;
}

void Canon37mm::setPosition(Vec2 pos, Size sizeItem)
{
	float posX = pos.x, posY = pos.y;
	if (sizeItem.height > sizeItem.width)
	{
		posY -= 35.0f / 2;
		m_pNode->setRotation(90);
		m_pRotBase = 90;
	}
	else
	{
		posX -= 35.0f / 2;
		m_pRotBase = 0;
	}

	GameEntity::setPosition(pos, sizeItem);
}

void Canon37mm::setState(int state)
{
	if (m_pState == state)
		return;

	m_pState = state;

	switch (state)
	{
	case IDLE_ENTITY:
		m_pTargetEnemy = 0;
		m_pInfo->reset();
		m_pStateItem->reset();
		setActive(false);
		break;

	case ACTIVE_ENTITY:
		setActive(true);
		break;

	case FINISHED_ENTITY:
	case DESTROY_ENTITY:
	{
						   m_pNode->setPosition(Vec2(2000, 2000));
						   m_pInfo->reset();
						   m_pStateItem->reset();
						   setActive(false);
						   m_pTargetEnemy = 0;
	}
	}
}

void Canon37mm::destroy()
{
	m_pDestroy->setVisible(true);
	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	m_pBarrelEntities.at(1)->stopAllActions();
	m_pBarrelEntities.at(1)->unscheduleAllCallbacks();
	//
	spTrackEntry* entry = m_pDestroy->setAnimation(0, ANIMATION_EXPLODING, false);
	m_pDestroy->setTrackEndListener(entry, [this](int trackIndex) {
		m_pDestroy->setVisible(false);
		m_pInfo->reset();
		stopAllActions();
		setState(DESTROY_ENTITY);
	});
}

void Canon37mm::finish()
{
	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	m_pBarrelEntities.at(1)->stopAllActions();
	m_pBarrelEntities.at(1)->unscheduleAllCallbacks();
	//

	setState(FINISHED_ENTITY);
}

bool Canon37mm::takeDamage(float damage, int typeDamge)
{
	if (!GameEntity::takeDamage(damage, typeDamge))
		return false;
	if (m_pStateItem->m_pHeath <= 0)
	{
		m_pStateItem->m_pHeath = 0;
		destroy();
	}

	return true;
}

void Canon37mm::attack(bool isAttack)
{
	if (!m_pTargetEnemy || !isAttack || !m_pTargetEnemy->isActive())
	{
		m_bIsAttack = false;
		m_pBarrelEntities.at(0)->unschedule("SHOT_KEY");
		m_pBarrelEntities.at(1)->unschedule("SHOT_KEY");
		m_pTargetEnemy = 0;
		return;
	}

	auto baseBerret = m_pNode->getChildByName("CANON37MM_BASEBARRET");

	auto pos = getPosition();
	auto vec_huyen = m_pTargetEnemy->getPosition() - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));

	auto currentGoc = m_pNode->getRotation();
	goc -= currentGoc;

	auto actionRotate = RotateTo::create(ROTATION_TIME_WEAPON_MACHINEGUN, goc);

	CallFunc* shot = NULL;
	if (isAttack && !m_bIsAttack)
	{
		m_bIsAttack = true;
		shot = CallFunc::create([this](){
			// First shoot 0
			m_pBarrelEntities.at(0)->scheduleOnce([this](float dt){
				if (m_pTargetEnemy->isActive())
				{
					m_bIsRightShoot = true;
					shooting();
				}
			}, 0.0f, "FIRST_SHOT_KEY");
			
			m_pBarrelEntities.at(0)->schedule([this](float dt){
				if (m_pTargetEnemy->isActive())
				{
					m_bIsRightShoot = true;
					shooting();
				}
			}, m_pStateItem->m_pSpeedShoot, "SHOT_KEY");

			// First shoot 1
			m_pBarrelEntities.at(1)->scheduleOnce([this](float dt){
				if (m_pTargetEnemy->isActive())
				{
					m_bIsRightShoot = false;
					shooting();
				}
			}, 0.0f, "FIRST_SHOT_KEY");

			m_pBarrelEntities.at(1)->schedule([this](float dt){
				if (m_pTargetEnemy->isActive())
				{
					m_bIsRightShoot = false;
					shooting();
				}
			}, m_pStateItem->m_pSpeedShoot, "SHOT_KEY");
		});
	}
	baseBerret->runAction(Sequence::create(actionRotate, DelayTime::create(ROTATION_TIME_DELAY), shot, NULL));
}

bool Canon37mm::isHaveTarget()
{
	if (m_pTargetEnemy)
	{
		if (m_pTargetEnemy->isActive())
			if (m_pTargetEnemy->getId() == ID_COASTGUARD)
				return true;
		return false;
	}
	return false;
}

void Canon37mm::setTarget(GameEntity* target)
{
	if (target->getId() == ID_COASTGUARD && target->isActive())
	{
		m_pBarrelEntities.at(0)->unschedule("SHOT_KEY");
		GameEntity::setTarget(target);
	}
	else if (target->getId() == ID_FISHINGBOAT && target->isActive())
	{
		m_pBarrelEntities.at(0)->unschedule("SHOT_KEY");
		GameEntity::setTarget(target);
	}
	else if (target->getId() == ID_DESTROYER956 && target->isActive())
	{
		m_pBarrelEntities.at(0)->unschedule("SHOT_KEY");
		GameEntity::setTarget(target);
	}
}

void Canon37mm::shooting()
{
	auto bul = EntityFactory::getInstance()->getObject(ID_BULLET);
	bul->setSprite(ID_SMALLBULLET);
	bul->setSpeedMove(m_pStateItem->m_pSpeedBullet);
	bul->setDamage(getDamage() / 2);

	Vec2 posBarret = Vec2::ZERO;

	if (m_bIsRightShoot)
		posBarret = m_pBarrelEntities.at(0)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(m_pNode->getRotation() + m_pRotBase));
	else
		posBarret = m_pBarrelEntities.at(1)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(m_pNode->getRotation() + m_pRotBase));

	bul->setPosition(m_pNode->getPosition() + posBarret);
	bul->setTarget(m_pTargetEnemy);
	auto targetPoint = m_pTargetEnemy->getPosition() + Vec2(0, 50).rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(-m_pTargetEnemy->getRotate()));
	bul->runAction(bul->createAction(targetPoint + posBarret));
}