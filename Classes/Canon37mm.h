#ifndef __CANON37MM_H__
#define __CANON37MM_H__

#include "GameEntity.h"

#define SPRITE_WEAPON_CANON37MM_BODY		"Weapons/Icon_Canon37mm_Body.png"
#define SPRITE_WEAPON_CANON37MM_BASEBARRET	"Weapons/Icon_Canon37mm_BaseBarret.png"
#define SPRITE_WEAPON_CANON37MM_BARRET		"Weapons/Icon_Canon37mm_Barret.png"

class Canon37mm : public GameEntity
{
public:
	static Canon37mm* create(Layer* layer);
	virtual bool init();
	void	setRotate(Vec2 point);
	virtual void attack(bool isAttack);
	Action* createAction(Vec2 target = Vec2(0,0));
	void	setState(int state);
	void	setPosition(Vec2 pos, Size sizeItem = Size(1, 1));
	void	setTarget(GameEntity* target) override;
	virtual void destroy();
	virtual void shooting();
	virtual void finish();
	bool takeDamage(float damage, int typeDamge);
	bool isHaveTarget();

private:
	bool	m_bIsRightShoot;
};

#endif