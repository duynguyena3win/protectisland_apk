#include "CanonBulletExploding.h"
#include "Utils.h"

CanonBulletExploding* CanonBulletExploding::create(Layer* layer)
{
	auto gem = new CanonBulletExploding();
	gem->m_pLayer = layer;
	if (gem->init())
	{
		return gem;
	}
	CC_SAFE_DELETE(gem);
	return NULL;
}

bool CanonBulletExploding::init()
{
	if (!EffectEntity::init())
	{
		return false;
	}
	
	//Load effect exploding
	ArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo(SPRITE_CANON_BULLET_EXPLODING, PLIST_CANON_BULLET_EXPLODING, EXPORTJSON_CANON_BULLET_EXPLODING);
	m_pArmExploding = Armature::create(NAME_CANON_BULLET_EXPLODING);
	m_pArmExploding->setPosition(Vec2::ZERO);
	m_pNode->addChild(m_pArmExploding);


	m_pArmExploding->getAnimation()->setMovementEventCallFunc([this](Armature* arma, MovementEventType eventType, string moventID) {
		if (eventType == MovementEventType::COMPLETE)
			setActive(false);
	});

	m_iID = ID_SMALLBULLET;
	setActive(false);
	return true;
}

void CanonBulletExploding::setPosition(Vec2 pos)
{
	EffectEntity::setPosition(pos);
	runAction();
}

Action* CanonBulletExploding::createAction()
{
	return NULL;
}

void CanonBulletExploding::runAction()
{
	vector<string> animation;
	animation.push_back("exploding");
	setActive(true);
	m_pArmExploding->getAnimation()->playWithNames(animation, -1, false);
}