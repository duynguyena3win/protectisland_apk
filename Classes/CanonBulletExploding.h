#ifndef __CANONBULLETEXPLODING_H__
#define __CANONBULLETEXPLODING_H__

#include "EffectEntity.h"

#define	NAME_CANON_BULLET_EXPLODING			"CanonBulletExploding"
#define SPRITE_CANON_BULLET_EXPLODING		"Effects/CanonBulletExploding/CanonBulletExploding0.png"
#define PLIST_CANON_BULLET_EXPLODING		"Effects/CanonBulletExploding/CanonBulletExploding0.plist"
#define EXPORTJSON_CANON_BULLET_EXPLODING	"Effects/CanonBulletExploding/CanonBulletExploding.ExportJson"

class CanonBulletExploding : public EffectEntity
{
public:
	static CanonBulletExploding* create(Layer* layer);
	bool init();
	void setPosition(Vec2 pos);
	void runAction();
	Action* createAction();
};

#endif