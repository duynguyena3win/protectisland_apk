#include "CashEffect.h"
#include"Utils.h"

CashEffect* CashEffect::create(Layer* layer)
{
	auto gem = new CashEffect();
	gem->m_pLayer = layer;
	if (gem->init())
	{
		return gem;
	}
	CC_SAFE_DELETE(gem);
	return NULL;
}

bool CashEffect::init()
{
	if (!EffectEntity::init())
	{
		return false;
	}
	auto image = Sprite::create(SPRITE_EFFECT_CASH_ITEM);
	image->setScale(SCALE_EFFECT_SPRITE);
	m_pNode->addChild(image);

	m_pLabelText = Label::createWithBMFont(FONT_GAME, StringUtils::format("000"));
	m_pLabelText->setScale(FONT_GAME_SCALE);
	m_pLabelText->setPosition(Vec2(38.0f, 0.0f));
	m_pNode->addChild(m_pLabelText);

	m_iID = ID_CASH;
	setActive(false);
	return true;
}

void CashEffect::setPosition(Vec2 pos)
{
	EffectEntity::setPosition(pos);
	EffectEntity::runAction(createAction());
}

Action* CashEffect::createAction()
{
	Vec2 pos = getPosition();
	m_pNode->setPosition(pos + Vec2(20, 5));
	setActive(true);
	m_pNode->setScale(0.6f);

	auto fly = MoveTo::create(1.5f, Vec2(pos.x + 30, pos.y + 15));
	auto scale = ScaleTo::create(1.0f, 1.0f);
	auto spawn = Spawn::createWithTwoActions(fly, scale);
	auto fadeout = FadeOut::create(0.25f);
	auto fadein = FadeIn::create(0.0f);
	auto callfunc = CallFunc::create([this](){
		stopAllActions();
	});
	return Sequence::create(fadein, spawn, fadeout, callfunc, NULL);
}