#ifndef __CASHEFFECT_H__
#define __CASHEFFECT_H__

#include "EffectEntity.h"

class CashEffect : public EffectEntity
{
public:
	static CashEffect* create(Layer* layer);
	bool init();
	void setPosition(Vec2 pos);
	void runAction() { }
	Action* createAction();
};

#endif