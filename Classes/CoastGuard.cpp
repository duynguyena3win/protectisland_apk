#include "CoastGuard.h"
#include "EntityFactory.h"
#include "VisibleRect.h"
#include "GameScene.h"
#include "SpineHelper.h"
CoastGuard* CoastGuard::create(Layer* layer)
{
	auto rk = new CoastGuard();
	rk->m_pZOrder = LLG_ENTITY_GROUND;
	rk->m_pLayer = layer;
	if (rk->init())
	{
		return rk;
	}
	CC_SAFE_DELETE(rk);
	return NULL;
}

bool CoastGuard::init()
{
	if (!GameEntity::init())
	{
		return false;
	}
	m_pBodyEntity = Sprite::create(SPRITE_ENEMIES_COASTGUARD);
	m_pNode->addChild(m_pBodyEntity);

	// Load Destroy effect
	SpineHelper::getInstance()->lazyInit(SPRITE_PREFIX_EFFECT, "Exploding");
	m_pDestroy = SkeletonAnimation::createWithFile(
		SpineHelper::getInstance()->_dataFile, SpineHelper::getInstance()->_atlasFile, SCALE_EFFECT_EXPLODING);
	m_pDestroy->setVisible(false);
	m_pNode->addChild(m_pDestroy, 5);

	//Load PhysicBody
	auto body = PhysicsBody::createBox(Size(50, 100), PHYSICSBODY_MATERIAL_DEFAULT);
	body->setCollisionBitmask(COLLISION_BITMASK_ENEMY);
	body->setContactTestBitmask(true);
	body->setGravityEnable(false);
	m_pNode->setPhysicsBody(body);

	// Barret of Coast
	auto barret = Sprite::create(SPRITE_ENEMIES_COASTGUARD_BARRET);
	m_pBarrelEntities.pushBack(barret);
	m_pBarrelEntities.at(0)->setPosition(Vec2(0, 10));
	m_pNode->addChild(barret, 1, "COASTGUARD_BARRET_1");

	m_pInfo = EntityInfo::create();
	m_pLayer->addChild(m_pInfo, LLG_ENTITY_GROUND);

	m_iID = ID_COASTGUARD;
	m_bIsAttack = false;
	m_pTargetEnemy = 0;
	m_pStateItem = new ItemState(Utils::getItemById(m_iID));
	m_pState = IDLE_ENTITY;
	setActive(false);
	m_pNode->setPosition(Vec2(-2000, -2000));
	return true;
}

void CoastGuard::setPosition(Vec2 pos, Size sizeItem)
{
	GameEntity::setPosition(pos);
	auto moving = createAction(m_pTargetPoint);

	runAction(moving);
	setState(ACTIVE_ENTITY);
}

void CoastGuard::setRotate(Vec2 point)
{

}

void CoastGuard::setState(int state)
{
	if (m_pState == state)
		return;

	m_pState = state;

	switch (state)
	{
	case IDLE_ENTITY:
	{
						m_pTargetEnemy = 0;
						m_pInfo->reset();
						m_pStateItem->reset();
						setActive(false);
	}
		break;

	case ACTIVE_ENTITY:
		setActive(true);
		break;

	case FINISHED_ENTITY:
	case DESTROY_ENTITY:
		m_pNode->setPosition(Vec2(-2000, -2000));
		m_pInfo->reset();
		m_pStateItem->reset();
		setActive(false);
		m_pTargetEnemy = 0;
		break;

	default:
		break;
	}
}

void CoastGuard::setBarretRot()
{
	
}

void CoastGuard::attack(bool isAttack)
{
	if (!m_pTargetEnemy || !isAttack ||!m_pTargetEnemy->isActive())
	{
		m_pNode->resume();
		m_pInfo->resume();
		m_bIsAttack = false;
		m_pBarrelEntities.at(0)->unschedule("SHOT_KEY");
		m_pTargetEnemy = 0;
		return;
	}

	m_pNode->pause();
	m_pInfo->pause();

	auto pos = getPosition() + m_pBarrelEntities.at(0)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(m_pNode->getRotation()));
	
	auto vec_huyen = m_pTargetEnemy->getPosition() - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));
	auto currentGoc = m_pNode->getRotation();
	
	goc -= currentGoc;
	auto actionRotate = RotateTo::create(ROTATION_TIME_WEAPON_MACHINEGUN, goc);

	CallFunc* shot = NULL;
	if (isAttack && !m_bIsAttack)
	{
		m_bIsAttack = true;
		shot = CallFunc::create([this](){
			m_pBarrelEntities.at(0)->schedule([this](float dt){
				if (m_pTargetEnemy && m_pTargetEnemy->isActive())
				{
					auto bul = EntityFactory::getInstance()->getObject(ID_BULLET);
					bul->setSprite(ID_SMALLBULLET);
					bul->setSpeedMove(m_pStateItem->m_pSpeedBullet);
					bul->setDamage(getDamage());
					auto posBarrel = m_pBarrelEntities.at(0)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(-m_pNode->getRotation()));
					bul->setPosition(m_pNode->getPosition() + posBarrel);
					bul->setTarget(m_pTargetEnemy);
					bul->runAction(bul->createAction(m_pTargetEnemy->getPosition()));
				}
			}, m_pStateItem->m_pSpeedShoot, "SHOT_KEY");
		});
	}
	m_pBarrelEntities.at(0)->runAction(Sequence::create(actionRotate, DelayTime::create(ROTATION_TIME_DELAY), shot, NULL));
}

void CoastGuard::destroy()
{
	m_pDestroy->setVisible(true);

	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	//
	spTrackEntry* entry = m_pDestroy->setAnimation(0, ANIMATION_EXPLODING, false);
	this->getDelegate()->enemyBeKillCallback(m_pStateItem->m_pValue, getPosition());
	m_pDestroy->setTrackEndListener(entry, [this](int trackIndex) {
		m_pDestroy->setVisible(false);
		setState(DESTROY_ENTITY);
	});
}

Action* CoastGuard::createAction(Vec2 target)
{
	m_pTargetPoint = target;
	// calculate degree
	auto pos = getPosition();
	auto vec_huyen = m_pTargetPoint - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));
	m_pNode->setRotation(goc);
	m_pRotBase = goc;
	//
	m_pState = IDLE_ENTITY;
	setActive(false);
	//
	auto distance = m_pTargetPoint.distance(pos);
	auto moving = MoveTo::create(distance / m_pStateItem->m_pSpeedMove, m_pTargetPoint);
	return moving;
}

void CoastGuard::finish()
{
	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	//
	this->getDelegate()->enemyFinishCallback(m_pStateItem->m_pDamge);
	setState(FINISHED_ENTITY);
}

void CoastGuard::runAction(Action* action)
{
	auto subAction = createActionBarInfo();
	GameEntity::runAction(action);
	m_pInfo->runAction(subAction);
}

bool CoastGuard::takeDamage(float damage, int typeDamge)
{
	if (!GameEntity::takeDamage(damage, typeDamge))
		return false;

	if (m_pStateItem->m_pHeath <= 0)
	{
		m_pStateItem->m_pHeath = 0;
		destroy();
	}
	return true;
}

void CoastGuard::shooting()
{

}