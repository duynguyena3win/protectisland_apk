#ifndef __COASTGUARD_H__
#define __COASTGUARD_H__

#include "GameEntity.h"

#define SPRITE_ENEMIES_COASTGUARD			"Enemies/Ene_CoastGuard.png"
#define SPRITE_ENEMIES_COASTGUARD_BARRET	"Enemies/Ene_CoastGuard_Barret.png"

class CoastGuard : public GameEntity
{
public:
	static CoastGuard* create(Layer* layer);
	virtual bool init();
	void setPosition(Vec2 pos, Size sizeItem = Size(1, 1));
	void setRotate(Vec2 point);
	Action* createAction(Vec2 target = Vec2(0,0));
	void setState(int state);
	virtual void destroy();
	virtual void shooting();
	virtual void finish();
	void attack(bool isAttack);
	void setBarretRot();
	void runAction(Action* action);
	bool takeDamage(float damage, int typeDamge);

};

#endif