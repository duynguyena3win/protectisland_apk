#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "cocos2d.h"
USING_NS_CC;
using namespace std;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <android/log.h>
#   define  LOGI(...)               __android_log_print(ANDROID_LOG_INFO, "DN_COCOS", __VA_ARGS__)
#else
#   define  LOGI(...)               log(__VA_ARGS__)
#endif

// Config game
#define DESIGN_RESOLUTION_WIDTH 1280
#define DESIGN_RESOLUTION_HEIGHT 720

// Enum of Entity
enum ENTITY_STATE
{
	IDLE_ENTITY,
	ACTIVE_ENTITY,
	ATTACK_ENTITY,
	FINISHED_ENTITY,
	DESTROY_ENTITY
};

enum ID_POPUP
{
	PU_NOTICE = 1,
	PU_ITEMINFORMATION = 2,
	PU_OPTION = 3,
	PU_REWARD = 4,
	PU_RESULT = 5,
	PU_RESOURCE = 6,	// Not enough resource
	PU_TRADE = 7,
	PU_SOCIAL = 8,
};

enum ID_BUTTON
{
	ID_YES,
	ID_NO,
	ID_OK,
	ID_PURCHASE,
	ID_WATCH,
	ID_CLOSE,
};

enum BUTTON_TARGETMENU
{
	BTG_SELL,
	BTG_UPGRADE
};

enum LAYER_LEVEL
{
	LLG_BASE,
	LLG_MAP,
	LLG_ENTITY_UNDERGROUND,
	LLG_ENTITY_GROUND,
	LLG_EFFECT,
	LLG_HUBMENU,
	LLG_HUDITEM,
	LLG_FREEMIUMBAR,
	LLG_POPUP,
};

enum MENU_ITEMS
{
	MI_1X1,
	MI_1X2,
	MI_2X1,
	MI_2X2,
	MI_ITEM,
};

enum ID_SPECIALITEM
{
	IS_BOOMER,
	IS_HELP,
	IS_SURRENDER,
};

enum ID_GAMEENTITY
{
	// Enemies
	ID_FISHINGBOAT = 0,
	ID_COASTGUARD = 1,
	ID_DESTROYER956 = 2,
	ID_AIRCRAFTCARRIERSHIP = 3,
	
	// SubItems
	ID_JETPLANE = 4,
	ID_BULLET = 5,
	
	// Weapons
	ID_CANON37MM = 6,
	ID_CANON203MM = 7,
	ID_MACHINEGUN = 8,
	ID_FOGGUN = 9,
	ID_ROCKETS300 = 10,

	// Effect Items,
	ID_GEM = 11,
	ID_CASH = 12,
	ID_HEART = 13,
	ID_EXP = 14,
	ID_WARNING = 15,
	ID_SMALLBULLET = 16,
	ID_FOGBULLET = 17,
	ID_CANONBULLET = 18,
	ID_ROCKET = 19
};

#define TAG_MOVING	50

// KEY DEFINE OF GAMES -- New UI
#define KEY_SCENE_UI_SHOP			"ShopScene"
#define	KEY_SCENE_UI_LEVEL			"LevelSceneBar"
#define	KEY_SCENE_UI_MENU			"MenuScene"

#define	KEY_GAMEUI_FREEMIUMBAR		"FreemiumBar"
#define	KEY_GAMEUI_LEVELITEM		"LevelItem"

#define	KEY_GAMEUI_PANEL_CONTAIN		"ContainPanel"
#define KEY_GAMEUI_PANEL_USER			"UserPanel"
#define KEY_GAMEUI_PANEL_TOP			"TopPanel"
#define KEY_GAMEUI_PANEL_MIDDLE			"MiddlePanel"
#define KEY_GAMEUI_PANEL_BOTTOM			"BottomPanel"

#define KEY_GAMEUI_PANEL_ITEM			"ItemPanel"
#define KEY_GAMEUI_PANEL_INFORMATION	"InforPanel"
#define KEY_GAMEUI_PANEL_HEATH			"HeathPanel"
#define KEY_GAMEUI_PANEL_DAMAGE			"DamagePanel"
#define KEY_GAMEUI_PANEL_SHEILD			"ShieldPanel"
#define KEY_GAMEUI_PANEL_RANGE			"RangePanel"
#define KEY_GAMEUI_PANEL_RELOAD			"ReloadPanel"
#define KEY_GAMEUI_PANEL_SPEEDBULLET	"SpeedBulletPanel"

#define KEY_GAMEUI_USER_AVATAR			"UserAvatar"

#define KEY_GAMEUI_TITLE_TEXT		"TitleText"
#define KEY_GAMEUI_BODY_TEXT		"BodyText"
#define KEY_GAMEUI_USER_NAME_PANEL	"NamePanel"
#define KEY_GAMEUI_USER_NAME_TEXT	"NameText"

#define KEY_GAMEUI_USER_LEVEL_PANEL	"LevelPanel"
#define KEY_GAMEUI_USER_LEVEL_TEXT	"LevelText"

#define KEY_GAMEUI_USER_HEATH_BAR	"HeathBar"
#define KEY_GAMEUI_USER_HEATH_TEXT	"HeathText"

#define KEY_GAMEUI_RESOURCE_PANEL	"ResourcePanel"
#define KEY_GAMEUI_RESOURCE_CASH	"CASH"
#define KEY_GAMEUI_RESOURCE_EXP		"EXP"
#define KEY_GAMEUI_RESOURCE_GEM		"GEM"
#define KEY_GAMEUI_RESOURCE_STAR	"STAR"

#define KEY_GAMEUI_RESOURCE_TEXT	"Text"
#define	KEY_GAMEUI_BUTTON_SHOP		"ShopButton"
#define	KEY_GAMEUI_BUTTON_UPGRADE	"UpgradeButton"
#define	KEY_GAMEUI_BUTTON_SOCIAL	"SocialButton"
#define	KEY_GAMEUI_BUTTON_BACK		"BackButton"
#define KEY_GAMEUI_BUTTON_CAMPAIGN	"CampaignButton"
#define KEY_GAMEUI_BUTTON_EVENTS	"EventButton"

#define KEY_GAMEUI_NODE_LEVEL(x)	"Level_" + StringUtils::toString(x)

#define KEY_POUPUI_BODY_TEXT		"BodyText"

#define KEY_GAMEUI_PANEL_LISTVIEW_GEM		"GemPanel"
#define KEY_GAMEUI_PANEL_LISTVIEW_EXP		"EXPPanel"
#define KEY_GAMEUI_LISTVIEW					"ListView"

// Define Key of PopUp
#define KEY_POPUP_REWARD				"POPUP_REWARD"
#define KEY_POPUP_SOCIAL				"POPUP_FACEBOOK"
#define KEY_POPUP_NOTENOUGHRESOURCE		"POPUP_NER"
#define KEY_POPUP_BUYSELL				"POPUP_BUYSELL"
#define KEY_POPUP_ITEMINFORMATION		"POPUP_INFORMATIONITEM"
#define KEY_POPUP_NOTICE				"POPUP_NOTICE"
#define KEY_POPUP_RESOURCE				"POPUP_RESOURCE"
#define KEY_POPUP_RESULT				"POPUP_RESULT"
// =============================
#define KEY_PANEL_CONTAIN	"panelContain"
#define KEY_SUB_TABLE		"panelSTable"
#define KEY_TEXT_GEM		"labelGem"
#define KEY_TEXT_CASH		"labelCash"
#define KEY_TEXT_EXP		"labelEXP"
#define KEY_TEXT_WEAPON		"labelWeapon"
#define KEY_TEXT_TITLE		"labelTitle"
#define KEY_TEXT_NAME		"labelName"
#define KEY_TEXT_VALUE		"labelValue"
#define KEY_TEXT_CONTAIN	"labelText"
#define KEY_LISTVIEW_GEM	"listGemItems"
#define KEY_LISTVIEW_EXP	"listEXPItems"

// Button Key
#define KEY_BUTTON_OK			"OKButton"
#define KEY_BUTTON_MORE			"MoreButton"
#define KEY_BUTTON_CONNECT		"ConnectButton"
#define KEY_BUTTON_DISCONNECT	"DisconnectButton"
#define KEY_BUTTON_CANCEL		"CancelButton"
#define KEY_BUTTON_SHARE		"ShareButton"
#define KEY_BUTTON_REPLAY		"ReplayButton"
#define KEY_BUTTON_REPLAY		"ReplayButton"

#define KEY_BUTTON_WATCH		"btnWatch"
#define KEY_BUTTON_SHOP			"btnShop"
#define KEY_BUTTON_YES			"YesButton"
#define KEY_BUTTON_NO			"NoButton"
#define KEY_BUTTON_BUYNOW		"btnBuyNow"

// Menu Button
#define KEY_STAR(x)				"Star_" + StringUtils::toString(x)
#define KEY_BUTTON_CAMPAIN		"btnCampain"
#define KEY_BUTTON_SURVIVAL		"btnSurvival"

//================== GAME INFORMATION ==================
#define FONT_GAME			"GameFonts/font2F.fnt"
#define FONT_GAME_SCALE		1.0f
#define GAME_HEATH_POINT	5000
#define TAG_DEFAULT			999
#define TAG_GAMEENTITY		998
#define TAG_ACTIVE			997

//================== MAP DESIGN ==================
#define MAP_CELL_WIDTH		25
#define MAP_CELL_HEIGHT		25
#define MAP_WITDH		45
#define MAP_HEIGHT		26
#define MAPGAME_FILES(level)	StringUtils::format("LevelMaps/Level%d.tmx", level)

//================== BACKGROUND SPRITE SENCE ==================
#define SPRITE_MENUSCENE_BACKGROUND "MenuScene/bg_menu.png"
#define SPRITE_GAMESCENE_BACKGROUND "GameScene/bg_game.png"
#define SPRITE_LEVELSCENE_BACKGROUND "LevelScene/bg_level.png"
#define SPRITE_LOGOSCENE_BACKGROUND "LogoScene/bg_logo.png"
#define SPRITE_UPGRADESCENE_BACKGROUND "UpgradeScene/bg_upgrade.png"

// Position 
#define POSITION_ENTITYINFO_DEFAULT	Vec2(0, 50)

// Rotation Time for Entity
#define ROTATION_TIME_WEAPON_MACHINEGUN	0.5f

#define ROTATION_TIME_DELAY	0.2f

//================== GAME WEAPONS ==================
#define NUMBER_WEAPONS	5

#define SPRITE_ICON_WEAPON_MACHINEGUN	"Weapons/Icon_MachineGun.png"
#define SPRITE_ICON_WEAPON_CANON30MM	"Weapons/Icon_Canon30mm.png"
#define SPRITE_ICON_WEAPON_CANON203MM	"Weapons/Icon_Canon203mm.png"
#define SPRITE_ICON_WEAPON_ROCKET300	"Weapons/Icon_Rocket300.png"
#define SPRITE_ICON_WEAPON_FOGGUN		"Weapons/Icon_FogGun.png"
#define SPRITE_ICON_WEAPON_JETPLANE		"Weapons/Icon_JetPlane.png"

static const char* LIST_ICON_NORMAL[NUMBER_WEAPONS] = {
	SPRITE_ICON_WEAPON_MACHINEGUN,
	SPRITE_ICON_WEAPON_CANON30MM,
	SPRITE_ICON_WEAPON_CANON203MM,
	SPRITE_ICON_WEAPON_ROCKET300,
	SPRITE_ICON_WEAPON_FOGGUN
};

#define SPRITE_WEAPON_MACHINEGUN_BODY	"Weapons/MachineBody.png"
#define SPRITE_WEAPON_MACHINEGUN_BARREL	"Weapons/MachineBarrel.png"

#define NUMBER_BULLETS_TYPE	2

// UI Resources
#define		PATH_GAMEUI_SHOPSCENE_CSB		"GameUI/ShopScene.csb"
#define		PATH_GAMEUI_MENUSCENE_CSB		"GameUI/MenuScene.csb"
#define		PATH_GAMEUI_LEVELSCENE_CSB		"GameUI/LevelScene.csb"

#define		PATH_POPUPUI_FACEBOOK			"PopUpUI/PU_Facebook.csb"
#define		PATH_POPUPUI_NOTICE				"PopUpUI/PU_Notice.csb"
#define		PATH_POPUPUI_RESOURCE			"PopUpUI/PU_Resource.csb"
#define		PATH_POPUPUI_RESULT				"PopUpUI/PU_Result.csb"
#define		PATH_POPUPUI_REWARD				"PopUpUI/PU_Reward.csb"
#define		PATH_POPUPUI_TRADE				"PopUpUI/PU_Trade.csb"
#define		PATH_POPUPUI_ITEMINFORMATION	"PopUpUI/PU_ItemInformation.csb"

#define		PATH_ITEMIAP_CSB				"UICBS/ItemIAP.csb"

//================== GAME SUDITEM ==================
#define SPRITE_SUBITEMS_BULLET_PREFIX		"SubItems/BulletType"
#define KEY_BULLET_TYPE(x)		"BulletType" + StringUtils::toString(x)

#define SPRITE_SUBITEMS_GAMEBAR_BACKGROUND	"SubItems/Default_BG.png"
#define SPRITE_SUBITEMS_GAMEBAR_PROGRESS	"SubItems/Default_Progress.png"
#define SPRITE_SUBITEMS_ENTITY_BACKGROUND	"SubItems/Default_GameInfoBack.png"
#define SPRITE_SUBITEMS_ENTITY_PROGRESS		"SubItems/Default_GameInfoProgress.png"

//================== GAME ENEMIES ==================
#define SPRITE_ENEMIES_FISHINGSHIP	"Enemies/Ene_FishingBoat.png"
#define SPRITE_ENEMIES_AIRCRAFTCARRIERSHIP	"Enemies/Ene_AircraffCarrier.png"
#define SPRITE_ENEMIES_AIRCRAFT	"Enemies/Aircraft.png"

//================== BUTTONS ==================
// Button Debug
#define SPRITE_BUTTON_ENABLE	"Buttons/ButtonEna.png"
#define SPRITE_BUTTON_DISABLE	"Buttons/ButtonDis.png"

// Button Start
#define SPRITE_BUTTON_START_ENABLE	"Buttons/BtnStart_Ena.png"
#define SPRITE_BUTTON_START_DISABLE	"Buttons/BtnStart_Dis.png"

// Button Pause
#define SPRITE_BUTTON_PAUSE_ENABLE	"Buttons/BtnPause_Ena.png"
#define SPRITE_BUTTON_PAUSE_DISABLE	"Buttons/BtnPause_Dis.png"

// Button OK
#define SPRITE_BUTTON_OK_ENABLE		"Buttons/ButtonOK_Ena.png"
#define SPRITE_BUTTON_OK_DISABLE	"Buttons/ButtonOK_Dis.png"

// Button Menu Campaign
#define SPRITE_MENUSCENE_BUTTON_CAMPAIGN_ENABLE		"Buttons/BtnCampaign_Ena.png"
#define SPRITE_MENUSCENE_BUTTON_CAMPAIGN_DISABLE	"Buttons/BtnCampaign_Dis.png"

// Button Menu Shop
#define SPRITE_MENUSCENE_BUTTON_SHOP_ENABLE		"Buttons/BtnShop_Ena.png"
#define SPRITE_MENUSCENE_BUTTON_SHOP_DISABLE	"Buttons/BtnShop_Dis.png"

// Button Menu Survival
#define SPRITE_MENUSCENE_BUTTON_SURVIVAL_ENABLE		"Buttons/BtnSurvival_Ena.png"
#define SPRITE_MENUSCENE_BUTTON_SURVIVAL_DISABLE	"Buttons/BtnSurvival_Dis.png"

// Button Upgrade in LevelScene
#define SPRITE_LEVELSCEN_BUTTON_UPGRADE_ENABLE		"Buttons/BtnUpgrade_Ena.png"
#define SPRITE_LEVELSCEN_BUTTON_UPGRADE_DISABLE		"Buttons/BtnUpgrade_Dis.png"

// Button Upgrade in LevelScene
#define SPRITE_BUTTON_BACK_ENABLE		"Buttons/BtnBack_Ena.png"
#define SPRITE_BUTTON_BACK_DISABLE		"Buttons/BtnBack_Dis.png"

// Button OK
#define SPRITE_BUTTON_OK_ENABLE		"Buttons/ButtonOK_Ena.png"
#define SPRITE_BUTTON_OK_DISABLE	"Buttons/ButtonOK_Dis.png"

// Button Build
#define SPRITE_BUTTON_BUILD_ENABLE	"Buttons/ButtonBuild_Ena.png"
#define SPRITE_BUTTON_BUILD_DISABLE	"Buttons/ButtonBuild_Dis.png"

// Button Sell
#define SPRITE_BUTTON_SELL_ENABLE		"Buttons/ButtonSell_Ena.png"
#define SPRITE_BUTTON_SELL_DISABLE		"Buttons/ButtonSell_Dis.png"

// Button Watch
#define SPRITE_BUTTON_WATCH_ENABLE		"Buttons/BtnWatch_Ena.png"
#define SPRITE_BUTTON_WATCH_DISABLE		"Buttons/BtnWatch_Dis.png"

// Button Purchase
#define SPRITE_BUTTON_PURCHASE_ENABLE		"Buttons/ButtonPurchase_Ena.png"
#define SPRITE_BUTTON_PURCHASE_DISABLE		"Buttons/ButtonPurchase_Dis.png"

// Button Close
#define SPRITE_BUTTON_CLOSE_ENABLE		"Buttons/BtnClose_Ena.png"
#define SPRITE_BUTTON_CLOSE_DISABLE		"Buttons/BtnClose_Dis.png"

// Button Upgrade in MainGame
#define SPRITE_BUTTON_UPGRADE_ENABLE	"Buttons/ButtonUpgrade_Ena.png"
#define SPRITE_BUTTON_UPGRADE_DISABLE	"Buttons/ButtonUpgrade_Dis.png"

// Button Connect
#define SPRITE_BUTTON_CONNECT_ENABLE	"Buttons/BtnConnect_Ena.png"
#define SPRITE_BUTTON_CONNECT_DISABLE	"Buttons/BtnConnect_Dis.png"

// Button Cancel
#define SPRITE_BUTTON_CANCEL_ENABLE		"Buttons/BtnCancel_Ena.png"
#define SPRITE_BUTTON_CANCEL_DISABLE	"Buttons/BtnCancel_Dis.png"

// Key for Animaiton
#define ANIMATION_EXPLODING	"animation"

//================== EFFECT ITEM ==================
// Effect for scene
#define SPRITE_PREFIX_EFFECT	"Effects"

// Scale Effect
#define SPRITE_EFFECT_CASH_ITEM	"Effects/CashSymbol.png"
#define SPRITE_EFFECT_GEM_ITEM	"Effects/GemSymbol.png"
#define SPRITE_EFFECT_EXP_ITEM	"Effects/EXPSymbol.png"
#define SPRITE_EFFECT_WEAPON_ITEM	"Resource/WeaponIcon.png"
#define SPRITE_EFFECT_STAR_ENA	"Effects/StarResult.png"
#define SPRITE_EFFECT_STAR_DIS	"Effects/StarResult_Dis.png"
#define SPRITE_EFFECT_HEART_ITEM	"Effects/HeartSymbol.png"

// Scale Effect Game
#define SCALE_EFFECT_SPRITE	0.7f
#define SCALE_EFFECT_EXPLODING	0.07f

//================== HUDGAME ==================
#define SPRITE_HUDGAME_TABLE	"Images/HudImages/Table.png"
#define SPRITE_HUDGAME_TOPTABLE	"HudGame/TopTable.png"

// Sprite for Heath Bar
#define SPRITE_HUDGAME_HEATHBAR_BACKGROUND	"ProgressBar/HeathBarBackground.png"
#define SPRITE_HUDGAME_HEATHBAR_PROGESS0	"ProgressBar/HeathBarProgess0.png"
#define SPRITE_HUDGAME_HEATHBAR_PROGESS1	"ProgressBar/HeathBarProgess1.png"

// Sprite for Enemy Bar
#define SPRITE_HUDGAME_ENEMYBAR_BACKGROUND	"ProgressBar/EnemyBarBG.png"
#define SPRITE_HUDGAME_ENEMYBAR_PROGRESS	"ProgressBar/EnemyBarPG.png"
#define SPRITE_HUDGAME_ENEMYBAR_BORDER		"ProgressBar/EnemyBarBD.png"

// Scale value of BarItems
#define SCALE_BARITEM_BUTTON_ICON_DISABLE	0.4f
#define SCALE_BARITEM_BUTTON_ICON_ENABLE	1.0f

// Icon Game
#define SPRITE_ICON_PRIRATE		"Icons/IconPrirate.png"
#define SPRITE_ICON_FLAG	"Icons/IconFlag.png"


#define COLLISION_BITMASK_ENEMY		0x00001
#define COLLISION_BITMASK_ISLAND	0x00002


#endif