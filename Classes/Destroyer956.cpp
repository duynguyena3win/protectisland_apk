#include "Destroyer956.h"
#include "GameScene.h"
#include "EntityFactory.h"
#include "SpineHelper.h"

Destroyer956* Destroyer956::create(Layer* layer)
{
	auto rk = new Destroyer956();
	rk->m_pLayer = layer;
	rk->m_pZOrder = LLG_ENTITY_GROUND;
	if (rk->init())
	{
		return rk;
	}
	CC_SAFE_DELETE(rk);
	return NULL;
}

bool Destroyer956::init()
{
	if (!GameEntity::init())
	{
		return false;
	}

	// Add Body for Node
	m_pBodyEntity = Sprite::create(SPRITE_ENEMIES_DESTROYER956_BODY);
	m_pNode->addChild(m_pBodyEntity, 1);

	// Add Barrel for Node
	auto barrelMain = Sprite::create(SPRITE_ENEMIES_DESTROYER956_MAINBARRET);
	m_pBarrelEntities.pushBack(barrelMain);
	m_pBarrelEntities.at(0)->setPosition(0, 10);
	m_pNode->addChild(m_pBarrelEntities.at(0), 2, "DESTROYER956_MAIN_BARRET");

	// Add Sub Barrel 1 for Node
	auto barrelSub1 = Sprite::create(SPRITE_ENEMIES_DESTROYER956_SUBBARRET);
	m_pBarrelEntities.pushBack(barrelSub1);
	m_pBarrelEntities.at(1)->setRotation(90);
	m_pBarrelEntities.at(1)->setPosition(0, -15);
	m_pNode->addChild(m_pBarrelEntities.at(1), 2, "DESTROYER956_SUB_BARRET_1");

	// Add Sub Barrel 2 for Node
	auto barrelSub2 = Sprite::create(SPRITE_ENEMIES_DESTROYER956_SUBBARRET);
	m_pBarrelEntities.pushBack(barrelSub2);
	m_pBarrelEntities.at(2)->setRotation(90);
	m_pBarrelEntities.at(2)->setPosition(0, -25);
	m_pNode->addChild(m_pBarrelEntities.at(2), 2, "DESTROYER956_SUB_BARRET_2");

	// Load Destroy effect
	SpineHelper::getInstance()->lazyInit(SPRITE_PREFIX_EFFECT, "Exploding");
	m_pDestroy = SkeletonAnimation::createWithFile(
		SpineHelper::getInstance()->_dataFile, SpineHelper::getInstance()->_atlasFile, SCALE_EFFECT_EXPLODING);
	m_pDestroy->setVisible(false);
	m_pNode->addChild(m_pDestroy, 3);

	//Load PhysicBody
	auto body = PhysicsBody::createBox(Size(50, 100), PHYSICSBODY_MATERIAL_DEFAULT);
	body->setCollisionBitmask(COLLISION_BITMASK_ENEMY);
	body->setContactTestBitmask(true);
	body->setGravityEnable(false);
	m_pNode->setPhysicsBody(body);
	
	m_pTrailWater = ParticleSun::create();
	m_pTrailWater->setEndColor(Color4F::BLUE);
	m_pTrailWater->setStartColor(Color4F::BLUE);
	m_pTrailWater->setPosition(Vec2::ZERO);
	m_pTrailWater->setPosVar(Vec2(1, 1));
	m_pTrailWater->setPositionType(tPositionType::RELATIVE);

	m_pNode->addChild(m_pTrailWater, 0);

	m_iID = ID_DESTROYER956;
	m_pInfo = EntityInfo::create();
	m_pLayer->addChild(m_pInfo, LLG_ENTITY_GROUND);

	m_bIsAttack = false;
	m_pTargetEnemy = 0;
	m_pStateItem = new ItemState(Utils::getItemById(m_iID));
	m_pState = IDLE_ENTITY;
	setActive(false);
	m_pNode->setPosition(Vec2(-2000, -2000));
	return true;
}

void Destroyer956::setRotate(Vec2 point)
{

}

void Destroyer956::attack(bool isAttack)
{
	if (!m_pTargetEnemy || !isAttack || !m_pTargetEnemy->isActive())
	{
		m_pNode->resume();
		m_pInfo->resume();
		m_bIsAttack = false;
		m_pBarrelEntities.at(0)->unschedule("SHOT_KEY");
		m_pBarrelEntities.at(1)->unschedule("SHOT_KEY");
		m_pBarrelEntities.at(2)->unschedule("SHOT_KEY");
		m_pTargetEnemy = 0;
		return;
	}

	m_pNode->pause();
	m_pInfo->pause();

	auto posMain = getPosition() + m_pBarrelEntities.at(0)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(m_pNode->getRotation()));
	auto gocMain = Utils::angleTwoVector(posMain, m_pTargetEnemy->getPosition());
	auto actionRotate = RotateTo::create(ROTATION_TIME_WEAPON_MACHINEGUN, gocMain - m_pNode->getRotation());

	auto posSubBarrel1 = getPosition() + m_pBarrelEntities.at(1)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(m_pNode->getRotation()));
	auto gocSub1 = Utils::angleTwoVector(posSubBarrel1, m_pTargetEnemy->getPosition());
	auto actionRotate1 = RotateTo::create(ROTATION_TIME_WEAPON_MACHINEGUN, gocSub1 - m_pNode->getRotation());

	auto posSubBarrel2 = getPosition() + m_pBarrelEntities.at(2)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(m_pNode->getRotation()));
	auto gocSub2 = Utils::angleTwoVector(posSubBarrel2, m_pTargetEnemy->getPosition());
	auto actionRotate2 = RotateTo::create(ROTATION_TIME_WEAPON_MACHINEGUN, gocSub2 - m_pNode->getRotation());

	CallFunc* shot = NULL;
	CallFunc* shot1 = NULL;
	CallFunc* shot2 = NULL;

	if (isAttack && !m_bIsAttack)
	{
		m_bIsAttack = true;
		shot = CallFunc::create([this](){
			m_pBarrelEntities.at(0)->schedule([this](float dt){
				if (m_pTargetEnemy && m_pTargetEnemy->isActive())
				{
					auto bul = EntityFactory::getInstance()->getObject(ID_BULLET);
					bul->setSprite(ID_SMALLBULLET);
					bul->setSpeedMove(m_pStateItem->m_pSpeedBullet);
					bul->setDamage(getDamage() / 2);
					auto posBarrel = m_pBarrelEntities.at(0)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(-m_pNode->getRotation()));
					bul->setPosition(m_pNode->getPosition() + posBarrel);
					bul->setTarget(m_pTargetEnemy);
					bul->runAction(bul->createAction(m_pTargetEnemy->getPosition()));
				}
			}, m_pStateItem->m_pSpeedShoot, "SHOT_KEY");
		});

		shot1 = CallFunc::create([this](){
			m_pBarrelEntities.at(1)->schedule([this](float dt){
				if (m_pTargetEnemy && m_pTargetEnemy->isActive())
				{
					auto bul = EntityFactory::getInstance()->getObject(ID_BULLET);
					bul->setSprite(ID_SMALLBULLET);
					bul->setSpeedMove(m_pStateItem->m_pSpeedBullet / 1.5);
					bul->setDamage(getDamage() / 4);
					auto posBarrel = m_pBarrelEntities.at(1)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(-m_pNode->getRotation()));
					bul->setPosition(m_pNode->getPosition() + posBarrel);
					bul->setTarget(m_pTargetEnemy);
					bul->runAction(bul->createAction(m_pTargetEnemy->getPosition()));
				}
			}, m_pStateItem->m_pSpeedShoot, "SHOT_KEY");
		});

		shot2 = CallFunc::create([this](){
			m_pBarrelEntities.at(2)->schedule([this](float dt){
				if (m_pTargetEnemy && m_pTargetEnemy->isActive())
				{
					auto bul = EntityFactory::getInstance()->getObject(ID_BULLET);
					bul->setSprite(ID_SMALLBULLET);
					bul->setSpeedMove(m_pStateItem->m_pSpeedBullet / 1.5);
					bul->setDamage(getDamage() / 4);
					auto posBarrel = m_pBarrelEntities.at(2)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(-m_pNode->getRotation()));
					bul->setPosition(m_pNode->getPosition() + posBarrel);
					bul->setTarget(m_pTargetEnemy);
					bul->runAction(bul->createAction(m_pTargetEnemy->getPosition()));
				}
			}, m_pStateItem->m_pSpeedShoot, "SHOT_KEY");
		});
	}
	m_pBarrelEntities.at(0)->runAction(Sequence::create(actionRotate, DelayTime::create(ROTATION_TIME_DELAY), shot, NULL));
	m_pBarrelEntities.at(1)->runAction(Sequence::create(actionRotate1, DelayTime::create(ROTATION_TIME_DELAY), shot1, NULL));
	m_pBarrelEntities.at(2)->runAction(Sequence::create(actionRotate2, DelayTime::create(ROTATION_TIME_DELAY), shot2, NULL));
}

Action*		Destroyer956::createActionMoving()
{
	m_pTargetPoint = m_vPathMoving[1];
	// calculate degree
	auto pos = getPosition();
	auto vec_huyen = m_pTargetPoint - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));
	m_pNode->setRotation(goc);
	m_pRotBase = goc;
	m_pTrailWater->setGravity(Vec2(0, -80));

	auto distance = m_pTargetPoint.distance(pos);
	auto timeRunning = distance / m_pStateItem->m_pSpeedMove;
	m_pMovingAction = MoveTo::create(timeRunning, m_pTargetPoint);
	return m_pMovingAction;
}

Action* Destroyer956::createBarInfoMoving()
{
	auto startPoint = m_pInfo->getPosition();
	auto targetPoint = m_vPathMoving[1] + POSITION_ENTITYINFO_DEFAULT - m_pInfo->getCenter();
	auto distance = targetPoint.distance(startPoint);

	auto timeRunning = distance / m_pStateItem->m_pSpeedMove;
	m_pMovingBarInfoAction = MoveTo::create(timeRunning, targetPoint);
	return m_pMovingBarInfoAction;
}

Action* Destroyer956::createAction(Vec2 target)
{
	m_pTargetPoint = target;
	// calculate degree
	auto pos = getPosition();
	auto vec_huyen = m_pTargetPoint - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));
	m_pNode->setRotation(goc);
	m_pRotBase = goc;
	//
	m_pState = IDLE_ENTITY;
	setActive(false);
	//
	auto distance = m_pTargetPoint.distance(pos);
	auto moving = MoveTo::create(distance / m_pStateItem->m_pSpeedMove, m_pTargetPoint);
	return moving;
}

void Destroyer956::setPosition(Vec2 pos, Size sizeItem)
{
	GameEntity::setPosition(pos);
	setState(ACTIVE_ENTITY);
}

void Destroyer956::setState(int state)
{
	if (m_pState == state)
		return;

	m_pState = state;

	switch (state)
	{
	case IDLE_ENTITY:
	{
						
						setActive(false);
	}
		break;

	case ACTIVE_ENTITY:
		setActive(true);
		break;

	case FINISHED_ENTITY:
	case DESTROY_ENTITY:
	{
						   m_pNode->setPosition(Vec2(-2000, -2000));
						   m_pInfo->reset();
						   m_pStateItem->reset();
						   setActive(false);
						   m_pTargetEnemy = 0;
	}

		break;
	default:
		break;
	}
}

void Destroyer956::destroy()
{
	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	//
	spTrackEntry* entry = m_pDestroy->setAnimation(0, ANIMATION_EXPLODING, false);
	this->getDelegate()->enemyBeKillCallback(m_pStateItem->m_pValue, getPosition());
	m_pDestroy->setTrackEndListener(entry, [this](int trackIndex) {
		m_pDestroy->setVisible(false);
		setState(DESTROY_ENTITY);
	});
}


void Destroyer956::finish()
{
	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	//
	this->getDelegate()->enemyFinishCallback(m_pStateItem->m_pDamge);
	setState(FINISHED_ENTITY);
}

void Destroyer956::runAction(Action* action)
{
	auto subAction = createActionBarInfo();
	GameEntity::runAction(action);
	m_pInfo->runAction(subAction);
}

bool Destroyer956::takeDamage(float damage, int typeDamge)
{
	switch (typeDamge)
	{
	case ID_FOGBULLET:
		m_pStateItem->m_pSpeedMove -= damage;
		createActionMoving();
		createBarInfoMoving();
		runMovingAction();
		break;
	default:
		if (!GameEntity::takeDamage(damage, typeDamge))
			return false;
		break;
	}

	LOGI("My heath = %d", m_pStateItem->m_pHeath);
	if (m_pStateItem->m_pHeath <= 0)
	{
		m_pStateItem->m_pHeath = 0;
		destroy();
	}

	if (!GameEntity::takeDamage(damage, typeDamge))
		return false;
	return true;
}

void Destroyer956::runMovingAction()
{
	if (m_pMovingAction && m_pMovingBarInfoAction)
	{
		GameEntity::runAction(m_pMovingAction);
		m_pInfo->stopAllActions();
		m_pInfo->runAction(m_pMovingBarInfoAction);
	}
}

void Destroyer956::shooting()
{

}