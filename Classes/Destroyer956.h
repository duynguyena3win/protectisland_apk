#ifndef __DESTROYER956_H__
#define __DESTROYER956_H__

#include "GameEntity.h"

#define SPRITE_ENEMIES_DESTROYER956_BODY	"Enemies/Ene_Destroyer956.png"
#define SPRITE_ENEMIES_DESTROYER956_MAINBARRET	"Enemies/Ene_CoastGuard_Barret.png"
#define SPRITE_ENEMIES_DESTROYER956_SUBBARRET	"Enemies/Ene_Destroyer956_SubBarret.png"

class Destroyer956 : public GameEntity
{
public:
	static Destroyer956* create(Layer* layer);
	virtual bool init();
	void setRotate(Vec2 point);
	void setPosition(Vec2 pos, Size sizeItem = Size(1, 1));
	Action* createAction(Vec2 target = Vec2(0,0));
	void setState(int state);
	virtual void destroy();
	virtual void shooting();
	virtual void finish();
	void attack(bool isAttack);
	void runAction(Action* action);
	bool takeDamage(float damage, int typeDamge);

	Action*		createActionMoving()	override;
	Action*		createBarInfoMoving()	override;
	void		runMovingAction()		override;
private:
	bool m_bIsSubAttack;
};

#endif