#include "EffectEntity.h"
#include"Utils.h"

bool EffectEntity::init()
{
	m_pNode = Node::create();
	m_pNode->setUserData(this);
	m_pNode->setTag(TAG_DEFAULT);
	m_pZOrder = LLG_EFFECT;
	m_pLayer->addChild(m_pNode, m_pZOrder);
	return true;
}

void EffectEntity::setActive(bool isActive)
{
	m_bIsActive = isActive;
	m_pNode->setVisible(isActive);
}

void EffectEntity::setPosition(Vec2 pos)
{
	m_pNode->setPosition(pos);
}

void EffectEntity::runAction(Action* action)
{
	m_pNode->stopAllActions();
	m_pNode->runAction(action);
}

void EffectEntity::stopAllActions()
{
	setActive(false);
	m_pNode->stopAllActions();
}

void EffectEntity::setText(int value)
{
	m_pLabelText->setString(StringUtils::format("%d", abs(value)));
	if (value > 0)
	{
		m_pLabelText->setColor(Color3B::ORANGE);
	}
	else if (value < 0)
	{
		m_pLabelText->setColor(Color3B::RED);
	}
}