#ifndef __EFFECTENTITY_H__
#define __EFFECTENTITY_H__

#include "Config.h"
#include "cocostudio\CocoStudio.h"
using namespace cocostudio;

class GameEntity;

class EffectEntity
{
public:
	Layer* m_pLayer;
	Node* m_pNode;
	int m_pZOrder;

public:
	CC_SYNTHESIZE(GameEntity*, _delEffect, Delegate);

	virtual bool init();
	virtual void setPosition(Vec2 pos);
	Vec2 getPosition() { return m_pNode->getPosition(); }
	virtual Action* createAction() = 0;
	virtual void runAction(Action* action);
	virtual void runAction() = 0;
	virtual Vec2 getCenter() { return m_pNode->getPosition(); };
	virtual bool isActive() { return m_bIsActive; }
	virtual void setActive(bool isActive);
	int getId() { return m_iID; }
	virtual void stopAllActions();
	virtual void setText(int value);
	virtual void setZOder(int zoder) { m_pNode->setZOrder(zoder); }
protected:
	int m_iID;
	Label* m_pLabelText;
	bool m_bIsActive;
	Armature* m_pArmExploding;
};

#endif