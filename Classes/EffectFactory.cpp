#include "EffectFactory.h"
#include "GemEffect.h"
#include "CashEffect.h"
#include "SmallBulletExploding.h"
#include "CanonBulletExploding.h"
#include "FogBulletExploding.h"
#include "HeathEffect.h"

EffectFactory* EffectFactory::s_pInstance = 0;

EffectFactory::EffectFactory()
{
	m_pPools.clear();
}

EffectFactory* EffectFactory::getInstance(Layer* layer)
{
	if (!s_pInstance)
	{
		s_pInstance = new EffectFactory();
		s_pInstance->m_pLayer = layer;
	}
	return s_pInstance;
}

EffectEntity* EffectFactory::getObject(int id)
{
	EffectEntity* object = 0;
	for (auto item : m_pPools)
	{
		if (!item->isActive() && item->getId() == id)
		{
			object = item;
			return object;
		}
	}
	return createObject(id);
}

void EffectFactory::reset()
{
	for (auto item : m_pPools)
	{
		item->setActive(false);
	}
}

void EffectFactory::release()
{
	for (auto item : m_pPools)
	{
		CC_SAFE_DELETE(item);
	}
	m_pPools.clear();
}


EffectEntity* EffectFactory::createObject(int id)
{
	EffectEntity* object = 0;
	switch (id)
	{
	case ID_GEM:
		object = GemEffect::create(m_pLayer);
		break;
	
	case ID_CASH:
		object = CashEffect::create(m_pLayer);
		break;
	case ID_HEART:
		object = HeathEffect::create(m_pLayer);
		break;
	case ID_EXP:
		//object = CoastGuard::create(m_pLayer);
		break;
	case ID_SMALLBULLET:
		object = SmallBulletExploding::create(m_pLayer);
		break;
	case ID_FOGBULLET:
		object = FogBulletExploding::create(m_pLayer);
		break;
	case ID_CANONBULLET:
		object = CanonBulletExploding::create(m_pLayer);
		break;
	default:
		break;
	}
	m_pPools.push_back(object);
	return object;
}