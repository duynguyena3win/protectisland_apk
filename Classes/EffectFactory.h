#ifndef __EFFECTFACTORY_H__
#define __EFFECTFACTORY_H__

#include "Config.h"
#include "EffectEntity.h"

class EffectFactory
{
protected:
	static EffectFactory* s_pInstance;
	Layer* m_pLayer;
	std::vector<EffectEntity*> m_pPools;

public:
	EffectFactory();
	static EffectFactory* getInstance(Layer* m_pLayer = 0);
	EffectEntity* getObject(int id);
	EffectEntity* createObject(int id);
	void reset();
	void release();
};

#endif