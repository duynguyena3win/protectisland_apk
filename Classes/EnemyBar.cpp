#include "EnemyBar.h"
#include"LevelInfo.h"

Vec2 EnemyBar::BEGIN_POINT = Vec2::ZERO;
float EnemyBar::LENGTH_BAR = 0.0f;
EnemyBar* EnemyBar::create()
{
	auto ebar = new EnemyBar();

	if (ebar->init())
	{
		return ebar;
	}
	CC_SAFE_DELETE(ebar);
	return ebar;
}

bool EnemyBar::init()
{
	if (!GameBar::init())
		return false;
	auto bg = Sprite::create(SPRITE_HUDGAME_ENEMYBAR_BACKGROUND);
	m_pSize = bg->getContentSize();
	bg->setAnchorPoint(Vec2(0, 0.5));
	bg->setPosition(Vec2::ZERO);
	this->addChild(bg, LLGB_BACKGROUND);
	this->setContentSize(bg->getContentSize());

	auto border = Sprite::create(SPRITE_HUDGAME_ENEMYBAR_BORDER);
	border->setAnchorPoint(Vec2(0, 0.5));
	this->addChild(border, LLGB_BORDER);
	
	m_pProgess = ProgressTimer::create(Sprite::create(SPRITE_HUDGAME_ENEMYBAR_PROGRESS));
	m_pProgess->setType(ProgressTimer::Type::BAR);
	m_pProgess->setMidpoint(Vec2(0, 1));
	m_pProgess->setBarChangeRate(Vec2(1, 0));
	m_pProgess->setPercentage(0);
	m_pProgess->setAnchorPoint(Vec2(0, 0.5));
	this->addChild(m_pProgess, LLGB_PROGESS);
	this->setScaleX(0.9f);
	this->setScaleY(0.6f);

	m_pIconBar = Sprite::create(SPRITE_ICON_PRIRATE);
	m_pIconBar->setScale(0.3f);
	this->addChild(m_pIconBar, LLGB_ICON2);
	m_pLength = abs(this->getContentSize().width);

	LENGTH_BAR = m_pLength;
	BEGIN_POINT = Vec2(0, 0);

	m_pIconBar->setPosition(Vec2::ZERO);
	m_pTime = 10;
	m_pCapity = m_pNumWave = 0;
	return true;
}

void EnemyBar::setLevel(LevelInfo* level)
{
	m_pTime = level->getEndTime();
	m_pNumWave = level->m_NumWaves;
	initFlagWave(level->m_pWaves);
}

void EnemyBar::runAction(bool reset)
{
	m_pIconBar->stopAllActions();
	if (reset)
	{
		m_pIconBar->setPosition(Vec2::ZERO);
	}
	auto pointMove = Vec2(m_pLength, 0);
	auto moving = MoveBy::create(m_pTime, pointMove);
	this->schedule([this](float dt){
		auto percent = (m_pIconBar->getPosition() - BEGIN_POINT).length() * 100 / LENGTH_BAR;
		log(percent);
		m_pProgess->setPercentage(percent);
	}, 0.01f, StringUtils::format("%dasdjkh%d", RandomHelper::random_int(0, 1234), RandomHelper::random_int(0, 1234)));

	m_pIconBar->runAction(moving);
	runFlagAction();
}

void EnemyBar::setTimeWorking(int time)
{
	m_pTime = time;
}

void EnemyBar::initFlagWave(vector<float> times)
{ 
	if (!m_pListWave.empty())
	{
		m_pListWave.clear();
		m_pListActionWave.clear();
	}

	for (int i = 0; i < m_pNumWave; i++)
	{
		auto wave = Sprite::create(SPRITE_ICON_FLAG);
		auto val = times[i] * LENGTH_BAR / m_pTime;
		this->addChild(wave, LLGB_ICON1);
		wave->setPosition(Vec2(val, 0));
		// create aciton
		auto up = MoveBy::create(0.5f, Vec2(0, 20));
		m_pListActionWave.pushBack(Sequence::create(DelayTime::create(times[i]), up, NULL));
		//
		m_pListWave.pushBack(wave);
		wave->setVisible(true);
	}
}

void EnemyBar::runFlagAction()
{
	stopAllFlagAction();
	for (int i = 0; i < m_pListWave.size(); i++)
		m_pListWave.at(i)->runAction(m_pListActionWave.at(i));
}

void EnemyBar::stopAllFlagAction()
{
	for (int i = 0; i < m_pListWave.size(); i++)
	{
		m_pListWave.at(i)->stopAllActions();
		m_pListWave.at(i)->setPositionY(BEGIN_POINT.y);
	}
}

void EnemyBar::resetListWave()
{
	for (int i = 0; i < m_pCapity; i++)
	{
		m_pListWave.at(i)->setVisible(false);
	}
	m_pNumWave = 0;
}

void EnemyBar::releaseListWave()
{
	for (int i = 0; i < m_pCapity; i++)
	{
		m_pListWave.at(i)->release();
	}
	m_pNumWave = 0;
	m_pCapity = 0;
}