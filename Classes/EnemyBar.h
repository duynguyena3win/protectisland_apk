#ifndef __ENEMYBAR_H__
#define __ENEMYBAR_H__

#include "GameBar.h"

class LevelInfo;

class EnemyBar : public GameBar
{
public:
	static EnemyBar* create();
	bool init();
	void runAction(bool reset = true);
	
	void setTimeWorking(int time);
	void initFlagWave(vector<float> times);
	void resetListWave();
	void releaseListWave();
	static Vec2 BEGIN_POINT;
	static float LENGTH_BAR;
	void setLevel(LevelInfo* level);
	void stopAllFlagAction();

protected:
	void runFlagAction();

private:
	Sprite* m_pIconBar;
	cocos2d::Vector<Sprite*> m_pListWave;
	cocos2d::Vector<Action*> m_pListActionWave;
	int m_pCapity;
	int m_pLength;
	float m_pTime;
	int m_pNumWave;
};

#endif