#include "Entity.h"
#include "BodyPart.h"
#include "BarrelPart.h"

bool	Entity::init()
{
	m_pNode = Node::create();
	m_pNode->setUserData(this);
	m_pNode->setTag(TAG_DEFAULT);

	m_pLayer->addChild(m_pNode, m_iZOrder);
	return true;
}

void	Entity::setRotation(float degree)
{
	m_pNode->setRotation(degree);
	m_pListBarrel[0]->setRotation(degree);
}

void	Entity::setActive(bool isActive)
{
	m_bIsActive = isActive;
	m_pNode->setVisible(isActive);
}

float	Entity::getBarrelRotation(int indBarrel, bool isWorldView)
{
	if (m_pListBarrel.size() <= indBarrel)
		return -1.0f;

	auto rotationNode = isWorldView ? m_pNode->getRotation() : 0;
	auto rotationBarrel = m_pListBarrel[indBarrel]->getRotation();
	return rotationNode + rotationBarrel;
}

void	Entity::setBarrelRotation(float rotation, int indBarrel)
{
	if (m_pListBarrel.size() <= indBarrel)
		return;
	m_pListBarrel[indBarrel]->setRotation(rotation);
}