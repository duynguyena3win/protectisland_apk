#ifndef __ENTITY_H__
#define __ENTITY_H__

#include "Config.h"

class RootPart;
class ItemState;

enum ACTION_ENTITY
{
	AE_NONE,
	AE_MOVING,
	AE_SHOOTING
};

class Entity
{
public:
	virtual	bool	init();
	virtual	void	setState(int state) = 0;
	virtual	void	setTarget(Entity* obj) = 0;
	virtual	Vec2	getPositionBarrel() = 0;
	virtual	float	getRotation() { return m_pNode->getRotation(); }
	//virtual	void	setRotation(float degree) = 0;
	virtual	void	setRotation(float degree);
	virtual	void	setPosition(Vec2 pos)	{ m_pNode->setPosition(pos); }
	virtual	void	setScale(float scale)	{ m_pNode->setScale(scale); }
	virtual	void	setActive(bool isActive);
	virtual	float	getBarrelRotation(int indBarrel = 0, bool isWorldView = false);
	virtual	void	setBarrelRotation(float rotation, int indBarrel = 0);
	virtual	void	update(float deltaTime) = 0;
	virtual	void	setMovingTo();

protected:
	Node*		m_pNode;
	Layer*		m_pLayer;
	int			m_iZOrder;
	int			m_iState;
	int			m_iID;
	bool		m_bIsActive;
	int			m_iCurrentAction;

protected:		// Information for 
	ItemState*	m_pStateItem;
	Vec2		m_vMovingPoint;
	Vec2		m_vCurrentPositon;
	float		m_fOldDistance;

protected:
	vector<RootPart*>		m_pListBody;
	vector<RootPart*>		m_pListBarrel;
};

#endif