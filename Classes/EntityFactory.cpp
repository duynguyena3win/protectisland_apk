#include "EntityFactory.h"
#include "Bullet.h"
#include "AircraftCarrierShip.h"
#include "CoastGuard.h"
#include "Destroyer956.h"
#include "FishingBoat.h"
#include "Canon37mm.h"
#include "Canon203mm.h"
#include "MachineGun.h"
#include "RocketS300.h"
#include "FogGun.h"

EntityFactory* EntityFactory::s_pInstance = 0;
int EntityFactory::COUNTENTITY = 0;

EntityFactory::EntityFactory()
{
	m_pPools.clear();
}

EntityFactory* EntityFactory::getInstance(Layer* layer)
{
	if (!s_pInstance)
	{
		s_pInstance = new EntityFactory();
		s_pInstance->m_pLayer = layer;
	}
	return s_pInstance;
}

GameEntity* EntityFactory::getObject(int id)
{
	GameEntity* object = 0;
	for (auto item : m_pPools)
	{
		if (!item->isActive() && item->getId() == id)
		{
			object = item;
			object->setState(IDLE_ENTITY);
			return object;
		}
	}
	return createObject(id);
}

void EntityFactory::reset()
{
	for (auto item : m_pPools)
	{
		item->setState(IDLE_ENTITY);
	}
}

void EntityFactory::release()
{
	for (auto item : m_pPools)
	{
		CC_SAFE_DELETE(item);
	}
	m_pPools.clear();
}


GameEntity* EntityFactory::createObject(int id)
{
	GameEntity* object = 0;
	switch (id)
	{
	case ID_BULLET:
		object = Bullet::create(m_pLayer);
		break;

	case ID_AIRCRAFTCARRIERSHIP:
		object = AircraftCarrierShip::create(m_pLayer);
		break;

	case ID_COASTGUARD:
		object = CoastGuard::create(m_pLayer);
		break;

	case ID_DESTROYER956:
		object = Destroyer956::create(m_pLayer);
		break;

	case ID_FISHINGBOAT:
		object = FishingBoat::create(m_pLayer);
		break;
		
	case ID_CANON37MM:
		object = Canon37mm::create(m_pLayer);
		break;

	case ID_CANON203MM:
		object = Canon203mm::create(m_pLayer);
		break;

	case ID_FOGGUN:
		object = FogGun::create(m_pLayer);
		break;

	case ID_MACHINEGUN:
		object = MachineGun::create(m_pLayer);
		break;

	case ID_ROCKETS300:
		object = RocketS300::create(m_pLayer);
		break;
	default:
		break;
	}
	m_pPools.push_back(object);
	object->setTag(COUNTENTITY++);
	return object;
}