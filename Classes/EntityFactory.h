#ifndef __ENTITYFACTORY_H__
#define __ENTITYFACTORY_H__

#include "GameEntity.h"

class EntityFactory
{
protected:
	static EntityFactory* s_pInstance;
	Layer* m_pLayer;
	std::vector<GameEntity*> m_pPools;
	
public:
	EntityFactory();
	static EntityFactory* getInstance(Layer* m_pLayer = 0);
	GameEntity* getObject(int id);
	GameEntity* createObject(int id);
	void reset();
	void release();

	static int COUNTENTITY;
};

#endif