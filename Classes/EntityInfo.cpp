#include "EntityInfo.h"

EntityInfo* EntityInfo::create()
{
	auto gbar = new EntityInfo();
	if (gbar->init())
	{
		return gbar;
	}
	CC_SAFE_DELETE(gbar);
	return gbar;
}

bool EntityInfo::init()
{
	if (!GameBar::init())
		return false;

	auto back = Sprite::create(SPRITE_SUBITEMS_ENTITY_BACKGROUND);
	m_pSize = back->getContentSize();
	back->setAnchorPoint(Vec2(0, 0.5));
	back->setPosition(Vec2::ZERO);
	this->addChild(back, 0);

	m_pProgess = ProgressTimer::create(Sprite::create(SPRITE_SUBITEMS_ENTITY_PROGRESS));
	m_pProgess->setType(ProgressTimer::Type::BAR);
	m_pProgess->setBarChangeRate(Vec2(1, 0));
	m_pProgess->setPercentage(100);
	m_pProgess->setMidpoint(Vec2(0, 1));
	m_pProgess->setAnchorPoint(Vec2(0, 0.5));
	m_pProgess->setPosition(Vec2::ZERO);

	this->addChild(m_pProgess, 1);
	this->setScale(0.5f);
	
	return true;
}