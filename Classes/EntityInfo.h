#ifndef __ENTITYINFO_H__
#define __ENTITYINFO_H__

#include "GameBar.h"

class EntityInfo : public GameBar
{
public:
	static EntityInfo* create();
	bool init();

protected:
};

#endif