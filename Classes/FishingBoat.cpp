#include "FishingBoat.h"
#include "VisibleRect.h"
#include "SpineHelper.h"
#include "GameScene.h"

FishingBoat* FishingBoat::create(Layer* layer)
{
	auto rk = new FishingBoat();
	rk->m_pLayer = layer;
	rk->m_pZOrder = LLG_ENTITY_GROUND;
	if (rk->init())
	{
		return rk;
	}
	CC_SAFE_DELETE(rk);
	return NULL;
}

bool FishingBoat::init()
{
	if (!GameEntity::init())
	{
		return false;
	}

	LOGI("Init Fishing Boat Object");
	// Load body
	m_pBodyEntity = Sprite::create(SPRITE_ENEMIES_FISHINGSHIP);
	m_pNode->addChild(m_pBodyEntity, 1);

	// Load Destroy effect
	SpineHelper::getInstance()->lazyInit(SPRITE_PREFIX_EFFECT, "Exploding");
	m_pDestroy = SkeletonAnimation::createWithFile(
		SpineHelper::getInstance()->_dataFile, SpineHelper::getInstance()->_atlasFile, SCALE_EFFECT_EXPLODING);
	m_pDestroy->setVisible(false);
	m_pNode->addChild(m_pDestroy, 5);
	
	//Load PhysicBody
	auto body = PhysicsBody::createBox(Size(50, 100), PHYSICSBODY_MATERIAL_DEFAULT);
	body->setCollisionBitmask(COLLISION_BITMASK_ENEMY);
	body->setContactTestBitmask(true);
	body->setGravityEnable(false);
	m_pNode->setPhysicsBody(body);

	m_pTrailWater = ParticleSun::create();
	m_pTrailWater->setEndColor(Color4F::BLUE);
	m_pTrailWater->setStartColor(Color4F::BLUE);
	m_pTrailWater->setPosition(Vec2::ZERO);
	m_pTrailWater->setPosVar(Vec2(1, 1));
	m_pTrailWater->setPositionType(tPositionType::RELATIVE);

	m_pNode->addChild(m_pTrailWater, 0);

	// Load effect be destroy
	m_pInfo = EntityInfo::create();
	m_pLayer->addChild(m_pInfo, LLG_EFFECT);

	m_iID = ID_FISHINGBOAT;
	m_pMovingAction = m_pMovingBarInfoAction = 0;
	m_pStateItem = new ItemState(Utils::getItemById(m_iID));
	m_pState = IDLE_ENTITY;
	setActive(false);
	m_pNode->setPosition(Vec2(-2000, -2000));
	return true;
}

Action* FishingBoat::createActionMoving()
{
	m_pTargetPoint = m_vPathMoving[1];
	// calculate degree
	auto pos = getPosition();
	auto vec_huyen = m_pTargetPoint - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));
	m_pNode->setRotation(goc);
	m_pRotBase = goc;
	m_pTrailWater->setGravity(Vec2(0, -80));// .rotateByAngle(Vec2::ZERO, -m_pRotBase));

	auto distance = m_pTargetPoint.distance(pos);
	m_pMovingAction = MoveTo::create(distance / m_pStateItem->m_pSpeedMove, m_pTargetPoint);
	return m_pMovingAction;
}

Action* FishingBoat::createBarInfoMoving()
{
	auto startPoint = m_pInfo->getPosition();
	auto targetPoint = m_vPathMoving[1] + POSITION_ENTITYINFO_DEFAULT - m_pInfo->getCenter();
	auto distance = targetPoint.distance(startPoint);

	auto timeRunning = distance / m_pStateItem->m_pSpeedMove;
	m_pMovingBarInfoAction = MoveTo::create(timeRunning, targetPoint);
	return m_pMovingBarInfoAction;
}

void FishingBoat::setState(int state)
{
	if (m_pState == state)
		return;

	m_pState = state;

	switch (state)
	{
	case IDLE_ENTITY:
	{
						m_pNode->setPosition(Vec2(-2000, -2000));
						m_pTargetEnemy = 0;
						m_pInfo->reset();
						m_pStateItem->reset();
						setActive(false);
						break;
	}
	case ACTIVE_ENTITY:
		setActive(true);
		break;

	case FINISHED_ENTITY:
	case DESTROY_ENTITY:
	{
		LOGI("Destroy Fishing Boat Object");
						   m_pNode->setPosition(Vec2(-2000, -2000));
						   m_pInfo->reset();
						   m_pStateItem->reset();
						   m_pTargetEnemy = 0;
						   setActive(false);
	}
		break;
	default:
		break;
	}
}

bool FishingBoat::takeDamage(float damage, int typeDamge)
{
	switch (typeDamge)
	{
	case ID_FOGBULLET:
		m_pStateItem->m_pSpeedMove -= damage;
		createActionMoving();
		createBarInfoMoving();
		runMovingAction();
		break;
	default:
		if (!GameEntity::takeDamage(damage, typeDamge))
			return false;
		break;
	}

	if (m_pStateItem->m_pHeath <= 0)
	{
		m_pStateItem->m_pHeath = 0;
		destroy();
	}
	return true;
}

void FishingBoat::destroy()
{
	m_pDestroy->setVisible(true);
	
	stopAllActions();
	spTrackEntry* entry = m_pDestroy->setAnimation(0, ANIMATION_EXPLODING, false);
	this->getDelegate()->enemyBeKillCallback(m_pStateItem->m_pValue, getPosition());
	m_pDestroy->setTrackEndListener(entry, [this](int trackIndex) {
		m_pDestroy->setVisible(false);
		m_pInfo->reset();
		setState(DESTROY_ENTITY);
	});
}

void FishingBoat::setPosition(Vec2 pos, Size sizeItem)
{
	GameEntity::setPosition(pos, sizeItem);
	setState(ACTIVE_ENTITY);
}

void FishingBoat::runMovingAction()
{
	if (m_pMovingAction && m_pMovingBarInfoAction)
	{
		GameEntity::runAction(m_pMovingAction);
		m_pInfo->stopAllActions();
		m_pInfo->runAction(m_pMovingBarInfoAction);
	}
}

void FishingBoat::finish()
{
	stopAllActions();
	this->getDelegate()->enemyFinishCallback(m_pStateItem->m_pDamge);
	setState(FINISHED_ENTITY);
}

void FishingBoat::shooting()
{

}