#ifndef __FISHINGBOAT_H__
#define __FISHINGBOAT_H__

#include "GameEntity.h"

class FishingBoat : public GameEntity
{

public:
	static FishingBoat* create(Layer* layer);
	virtual bool init();
	void setRotate(Vec2 point) { }
	void setPosition(Vec2 pos, Size sizeItem = Size(1, 1));

	void setState(int state);
	bool takeDamage(float damage, int typeDamge);
	void destroy();

	Action* createActionMoving() override;
	Action* createBarInfoMoving() override;
	void runMovingAction() override;
	
	virtual void finish();
	virtual void shooting();
};

#endif