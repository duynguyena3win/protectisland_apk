#include "FogBulletExploding.h"
#include "Utils.h"
#include "GameEntity.h"

FogBulletExploding* FogBulletExploding::create(Layer* layer)
{
	auto gem = new FogBulletExploding();
	gem->m_pLayer = layer;
	if (gem->init())
	{
		return gem;
	}
	CC_SAFE_DELETE(gem);
	return NULL;
}

bool FogBulletExploding::init()
{
	if (!EffectEntity::init())
	{
		return false;
	}
	
	m_pSmoke = ParticleSmoke::create();
	m_pSmoke->setAngle(180.0f);
	m_pSmoke->setEmissionRate(300);
	m_pSmoke->setLife(3);
	m_pSmoke->setSpeed(20);
	m_pSmoke->setEmissionRate(60);
	m_pSmoke->setPosVar(Vec2(80, 80));
	m_pSmoke->setGravity(Vec2::ZERO);
	m_pSmoke->setDuration(TIME_DURATION_FOGBULLET);
	m_pSmoke->setOpacity(50);
	m_pSmoke->setPosition(Vec2::ZERO);
	m_pSmoke->setPositionType(tPositionType::RELATIVE);
	m_pSmoke->setStartSize(60);
	m_pSmoke->setEndSize(8);
	m_pNode->addChild(m_pSmoke);
	

	m_iID = ID_FOGBULLET;
	setActive(false);
	return true;
}

void FogBulletExploding::setPosition(Vec2 pos)
{
	EffectEntity::setPosition(pos);
	setActive(true);
	runAction();
}

Action* FogBulletExploding::createAction()
{
	return NULL;
}

void FogBulletExploding::runAction()
{
	m_pSmoke->onEnter();
	m_pSmoke->scheduleOnce([this](float dt){
			_delEffect->effectFinishCallback(ID_FOGBULLET, 20);
	}, TIME_DURATION_FOGBULLET + 0.1f, "FINISH_FOG_EFFECT");
}