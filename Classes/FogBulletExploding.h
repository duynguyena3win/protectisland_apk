#ifndef __FOGBULLETEXPLODING_H__
#define __FOGBULLETEXPLODING_H__

#include "EffectEntity.h"

#define TIME_DURATION_FOGBULLET		5.0f
class FogBulletExploding : public EffectEntity
{
public:
	static FogBulletExploding* create(Layer* layer);
	bool init();
	void setPosition(Vec2 pos);
	void runAction();
	ParticleSmoke* m_pSmoke;
	Action* createAction();
};

#endif