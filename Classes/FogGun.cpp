#include "FogGun.h"
#include "Utils.h"
#include "EntityFactory.h"
#include "SpineHelper.h"

FogGun* FogGun::create(Layer* layer)
{
	auto cp = new FogGun();
	cp->m_pLayer = layer;
	cp->m_pZOrder = LLG_ENTITY_GROUND;
	if (cp->init())
	{
		return cp;
	}
	CC_SAFE_DELETE(cp);
	return NULL;
}

bool FogGun::init()
{
	if (!GameEntity::init())
	{
		return false;
	}
	// Load body
	m_pBodyEntity = Sprite::create(SPRITE_WEAPON_FOGGUN_BODY);
	m_pNode->addChild(m_pBodyEntity);
	
	//Load barrel
	auto barrel = Sprite::create(SPRITE_WEAPON_FOGGUN_BARREL);
	m_pBarrelEntities.pushBack(barrel);
	m_pNode->addChild(barrel);

	// Load effect be destroy
	SpineHelper::getInstance()->lazyInit(SPRITE_PREFIX_EFFECT, "Exploding");
	m_pDestroy = SkeletonAnimation::createWithFile(
		SpineHelper::getInstance()->_dataFile, SpineHelper::getInstance()->_atlasFile, SCALE_EFFECT_EXPLODING);
	m_pDestroy->setVisible(false);
	m_pNode->addChild(m_pDestroy, 5);

	// Infor of Game
	m_pInfo = EntityInfo::create();
	m_pLayer->addChild(m_pInfo, LLG_ENTITY_GROUND);

	m_iID = ID_FOGGUN;
	m_pStateItem = new ItemState(Utils::getItemById(m_iID));
	m_bIsAttack = false;
	m_pState = IDLE_ENTITY;

	setActive(false);
	m_pNode->setPosition(Vec2(2000, 2000));
	return true;
}

void FogGun::setRotate(Vec2 point)
{
	auto pos = getPosition();
	auto vec_huyen = point - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));
	auto actionRotate = RotateTo::create(1.0f, goc);
	
	m_pNode->runAction(actionRotate);
}

Action* FogGun::createAction(Vec2 target)
{
	return NULL;
}

void FogGun::attack(bool isAttack)
{
	if (!m_pTargetEnemy || !isAttack || !m_pTargetEnemy->isActive())
	{
		m_bIsAttack = false;
		m_pBarrelEntities.at(0)->unschedule("SHOT_KEY");
		m_pTargetEnemy = 0;
		return;
	}

	auto pos = getPosition();
	auto vec_huyen = m_pTargetEnemy->getPosition() - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));

	auto actionRotate = RotateTo::create(ROTATION_TIME_WEAPON_MACHINEGUN, goc);


	CallFunc* shot = NULL;
	if (isAttack && !m_bIsAttack)
	{
		m_bIsAttack = true;
		shot = CallFunc::create([this](){
			m_pBarrelEntities.at(0)->schedule([this](float dt){
				if (m_pTargetEnemy && m_pTargetEnemy->isActive())
				{
					shooting();
					m_pBarrelEntities.at(0)->unschedule("FIRST_SHOT_KEY");
				}
			}, 0.0f, "FIRST_SHOT_KEY");
			
			m_pBarrelEntities.at(0)->schedule([this](float dt){
				if (m_pTargetEnemy && m_pTargetEnemy->isActive())
				{
					shooting();
				}
			}, m_pStateItem->m_pSpeedShoot, "SHOT_KEY");
		});
		
	}
	m_pBarrelEntities.at(0)->runAction(Sequence::create(actionRotate, DelayTime::create(ROTATION_TIME_DELAY), shot, NULL));
}

void FogGun::shooting()
{
	auto bul = EntityFactory::getInstance()->getObject(ID_BULLET);
	bul->setSprite(ID_FOGBULLET);
	bul->setSpeedMove(m_pStateItem->m_pSpeedBullet);
	bul->setDamage(getDamage());
	auto posBarret = m_pBarrelEntities.at(0)->getPosition().rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(m_pNode->getRotation()));
	bul->setPosition(m_pNode->getPosition() + posBarret);
	bul->setTarget(m_pTargetEnemy);
	bul->runAction(bul->createAction(m_pTargetEnemy->getPosition()));
}

void FogGun::destroy()
{
	m_pDestroy->setVisible(true);

	stopAllActions();

	m_pNode->unscheduleAllCallbacks();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();

	spTrackEntry* entry = m_pDestroy->setAnimation(0, ANIMATION_EXPLODING, false);

	m_pDestroy->setTrackEndListener(entry, [this](int trackIndex) {
		m_pDestroy->setVisible(false);
		m_pInfo->reset();
		stopAllActions();
		setState(DESTROY_ENTITY);
	});
}

void FogGun::setState(int state)
{
	if (m_pState == state)
		return;

	m_pState = state;

	switch (state)
	{
	case IDLE_ENTITY:
		m_pTargetEnemy = 0;
		m_pInfo->reset();
		m_pStateItem->reset();
		setActive(false);
		break;

	case ACTIVE_ENTITY:
		setActive(true);
		break;

	case FINISHED_ENTITY:
	case DESTROY_ENTITY:
	{
						   setActive(false);
						   m_pTargetEnemy = 0;
						   m_pNode->setPosition(Vec2(2000, 2000));
						   m_pInfo->reset();
						   m_pStateItem->reset();
	}
		break;
	}
}

bool FogGun::takeDamage(float damage, int typeDamge)
{
	if (!GameEntity::takeDamage(damage, typeDamge))
		return false;
	if (m_pStateItem->m_pHeath <= 0)
	{
		m_pStateItem->m_pHeath = 0;
		destroy();
	}

	return true;
}

void FogGun::finish()
{
	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	//
	setState(FINISHED_ENTITY);
}

bool FogGun::isHaveTarget()
{
	if (m_pTargetEnemy)
	{
		if (m_pTargetEnemy->isActive())
			if (m_pTargetEnemy->getId() == ID_FISHINGBOAT)
				return true;
		return false;
	}
	return false;
}