#ifndef __FOGGUN_H__
#define __FOGGUN_H__

#include "GameEntity.h"

#define SPRITE_WEAPON_FOGGUN_BODY	"Weapons/FogGun_Body.png"
#define SPRITE_WEAPON_FOGGUN_BARREL	"Weapons/FogGun_Barret.png"

class FogGun : public GameEntity
{
public:
	static FogGun* create(Layer* layer);
	virtual bool init();
	void setRotate(Vec2 point);
	Action* createAction(Vec2 target = Vec2(0,0));
	virtual void attack(bool isAttack);
	virtual void destroy();
	virtual void shooting();
	virtual void finish();
	void setState(int state);
	bool takeDamage(float damage, int typeDamge);
	bool isHaveTarget();
};

#endif