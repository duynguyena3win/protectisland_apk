#include "FreemiumBar.h"
#include "VisibleRect.h"
#include "UserProfile.h"
#include "GameProgress.h"
#include "EnemyBar.h"

FreemiumBar* FreemiumBar::s_pInstance = 0;
Size FreemiumBar::SIZE_SUB_FREEMIUMBAR = Size(0, 0);
Size FreemiumBar::SIZE_FREEMIUMBAR = Size(0, 0);
FreemiumBar* FreemiumBar::GetInstance(Layer* layer, bool isInit)
{ 
	if (!s_pInstance || isInit)
	{
		if (!layer)
		{
			s_pInstance = 0;
		}
		else
		{
			if (s_pInstance)
				CC_SAFE_DELETE(s_pInstance);
			s_pInstance = new FreemiumBar();
			s_pInstance->m_pLayer = layer;
			s_pInstance->m_iZOrder = LLG_FREEMIUMBAR;

			if (!s_pInstance->init())
			{
				CC_SAFE_DELETE(s_pInstance);
				return NULL;
			}
		}
	}

	return s_pInstance;
}

void FreemiumBar::BeginLevel(Layer* layer)
{
	FreemiumBar::setNewLayer(layer);
	FREEMIUM_BAR->setPosition(VisibleRect::leftTop());
}

bool FreemiumBar::init()
{
	// init layer
	m_pNode = CSLoader::createNode(PATH_FREEMIUMBAR_CSB);
	m_pNode->setUserData(this);
	m_pLayer->addChild(m_pNode, m_iZOrder, KEY_GAMEUI_FREEMIUMBAR);
	m_pNode->setPosition(VisibleRect::center());
	auto userProfile = UserProfile::Get();
	m_iGem = userProfile->m_pGem;
	m_iEXP = userProfile->m_pEXP;
	m_iWeapon = userProfile->m_iNumWeapons;
	m_iCash = 0;

	auto containPanel = m_pNode->getChildByName(KEY_GAMEUI_PANEL_CONTAIN);
	m_pUserTable = containPanel->getChildByName(KEY_GAMEUI_PANEL_USER);
	m_pTopTable =  containPanel->getChildByName(KEY_GAMEUI_PANEL_TOP);

	// Load User Panel
	m_pAvatar = static_cast<Sprite*>(m_pUserTable->getChildByName(KEY_GAMEUI_USER_AVATAR));

	m_pNameText = static_cast<ui::Text*>(m_pUserTable->getChildByName(KEY_GAMEUI_USER_NAME_PANEL)->getChildByName(KEY_GAMEUI_USER_NAME_TEXT));
	m_pNameText->setText("Captain ZERO");

	m_pLevelText = static_cast<ui::Text*>(m_pUserTable->getChildByName(KEY_GAMEUI_USER_LEVEL_PANEL)->getChildByName(KEY_GAMEUI_USER_LEVEL_TEXT));
	m_pLevelText->setText("Lv. 3");

	m_pHeathProgress = static_cast<ui::LoadingBar*>(m_pUserTable->getChildByName(KEY_GAMEUI_USER_HEATH_BAR));
	m_pHeathProgress->setPercent(100);

	m_pHeathText = static_cast<ui::Text*>(m_pUserTable->getChildByName(KEY_GAMEUI_USER_HEATH_TEXT));
	m_pHeathText->setText("100 %");
	// ===============
	
	// Load Top Panel
	m_pGemText = static_cast<ui::Text*>(m_pTopTable->getChildByName(KEY_GAMEUI_RESOURCE_PANEL)->getChildByName(KEY_GAMEUI_RESOURCE_GEM)->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pGemText->setText(StringUtils::toString(m_iGem));

	m_pCashText = static_cast<ui::Text*>(m_pTopTable->getChildByName(KEY_GAMEUI_RESOURCE_PANEL)->getChildByName(KEY_GAMEUI_RESOURCE_CASH)->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pCashText->setText(StringUtils::toString(m_iCash));

	m_pEXPText = static_cast<ui::Text*>(m_pTopTable->getChildByName(KEY_GAMEUI_RESOURCE_PANEL)->getChildByName(KEY_GAMEUI_RESOURCE_EXP)->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pEXPText->setText(StringUtils::toString(m_iEXP));
	// ===============
	if (!initEnemyBar())
		return false;

	return true;
}

bool FreemiumBar::initEnemyBar()
{
	// Init Heath Bar
	vector<string> listProgess;
	listProgess.push_back(SPRITE_HUDGAME_HEATHBAR_PROGESS0);
	listProgess.push_back(SPRITE_HUDGAME_HEATHBAR_PROGESS1);


	auto nodeEne = m_pTopTable->getChildByName(KEY_NODE_ENEMYBAR);
	
	m_pEnemyBar = EnemyBar::create();
	m_pEnemyBar->setAnchorPoint(Vec2(0, 0));
	m_pEnemyBar->setPosition(Vec2::ZERO);

	nodeEne->addChild(m_pEnemyBar);

	return true;
}

void FreemiumBar::setTitle(string text)
{
	if (!m_pTopTable)
		return;
	m_pTitle->setString(text);
}
void FreemiumBar::setNewLayer(Layer* newlayer)
{
	if (!s_pInstance)
		FreemiumBar::GetInstance(newlayer);
	else
		FreemiumBar::GetInstance(newlayer, true);
}

bool FreemiumBar::loadFreemiumBar()
{
	return true;
}

int FreemiumBar::getValue(int typeItem)
{
	switch (typeItem)
	{
	case FI_GEM:
		return m_iGem;
	case FI_EXP:
		return m_iEXP;
		break;
	case FI_CASH:
		return m_iCash;
	case FI_WEAPONS:
		return m_iWeapon;
	}
	return -1;
}

void FreemiumBar::setValue(int typeItem, int newValue)
{
	switch (typeItem)
	{
	case FI_GEM:
	{
					if (!m_pGemText)
						return;
					m_iGem = newValue;
					m_pGemText->setText(StringUtils::toString(newValue));
	}
		break;
	case FI_EXP:
	{
					if (!m_pEXPText)
						return;
					m_iEXP = newValue;
					m_pEXPText->setText(StringUtils::toString(newValue));
	}
		break;
	case FI_CASH:
	{
					if (!m_pCashText)
						return;
					m_iCash = newValue;
					m_pCashText->setText(StringUtils::toString(newValue));
	}
		break;
	case FI_WEAPONS:
	{
					if (!m_pNumWeapons)
						return;
					m_iWeapon = newValue;
					m_pNumWeapons->setText(StringUtils::toString(newValue));
	}
		break;
	}
}

void FreemiumBar::setAvatar(string strNewAvatar)
{
	
}

void FreemiumBar::setVisible(int typeItem, bool state)
{
	switch (typeItem)
	{
	case FI_AVATAR:
		m_pAvatar->setVisible(state);
		m_pNameText->setVisible(state);
		break;
	case FI_GEM:
		m_pGemText->setVisible(state);
		break;
	case FI_EXP:
		m_pEXPText->setVisible(state);
		//m_pEXP->setVisible(state);

		m_pCashText->setVisible(!state);
		//m_pCash->setVisible(!state);
		break;
	case FI_CASH:
		m_pCashText->setVisible(state);
		//m_pCash->setVisible(state);

		m_pEXPText->setVisible(!state);
		//m_pEXP->setVisible(!state);
		break;
	case FI_WEAPONS:
		//m_pNumWeapons->setVisible(state);
		break;
	case FI_SUBTABLE:
		m_pTopTable->setVisible(state);
		break;
	case FI_HEATHBAR:
		m_pHeathProgress->setVisible(state);
		m_pHeathText->setVisible(state);
		break;
	case FI_ENEMYBAR:
		m_pEnemyBar->setVisible(state);
		break;
	case FI_TABLE:
	default:
		m_pNode->setVisible(state);
		break;
	}
}

bool FreemiumBar::subHeath(int subHeath)
{
	m_iHeath -= subHeath;

	bool isLose = false;
	if (m_iHeath <= 0)
	{
		isLose = true;
		m_iHeath = 0;
	}
	auto percent = m_iHeath * 100 / GAME_HEATH_POINT;
	
	m_pHeathProgress->setPercent(percent);
	m_pHeathText->setText(StringUtils::toString(percent) + " %");
	return isLose;
}

void FreemiumBar::resetHeathBar()
{
	m_iHeath = GAME_HEATH_POINT;
	subHeath(0);
}

void FreemiumBar::setInfoEnemyBar(LevelInfo* m_pCurrentLevelInfo)
{
	m_pEnemyBar->setLevel(m_pCurrentLevelInfo);
}

void FreemiumBar::runEnemyBar()
{
	m_pEnemyBar->runAction();
}

float FreemiumBar::getHeight()
{
	auto sizeTable = m_pUserTable->getContentSize();
	return sizeTable.height * m_pUserTable->getScale();
}
