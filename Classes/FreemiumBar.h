#ifndef __FREEMIUMBAR_H__
#define __FREEMIUMBAR_H__

#include "cocostudio\CocoStudio.h"
#include "ui\UIText.h"
#include "ui\UILoadingBar.h"

#include "Config.h"
#include "GameScene.h"

class GameProgress;
class EnemyBar;

#define PATH_FREEMIUMBAR_CSB					"GameUI/FreemiumBar.csb"

#define KEY_SPRITE_AVATAR	"spriteAvatar"
#define KEY_SPRITE_EXP		"spriteEXP"
#define KEY_SPRITE_CASH		"spriteCash"
#define KEY_SPRITE_GEM		"spriteGem"
#define KEY_SPRITE_WEAPON	"spriteWeapon"
#define KEY_NODE_HEATH		"nodeHeath"
#define KEY_NODE_ENEMYBAR	"EnemiesBarNode"

#define KEY_FOR_GEM			"FB_TEXTGEM"
#define KEY_FOR_CASH		"FB_TEXTCASH"
#define KEY_FOR_EXP			"FB_TEXTEXP"
#define KEY_FOR_WEAPON		"FB_TEXTWEAPON"
#define KEY_FOR_TITLE		"FB_TITLE"

enum FREEMIUM_ITEM
{
	FI_TABLE,
	FI_SUBTABLE,
	FI_AVATAR,
	FI_GEM,
	FI_CASH,
	FI_EXP,
	FI_WEAPONS,
	FI_HEATHBAR,
	FI_ENEMYBAR
};

class FreemiumBar
{
public:
	static FreemiumBar* GetInstance(Layer* layer = NULL, bool isInit = false);
	static void setNewLayer(Layer* newlayer);
	static void BeginLevel(Layer* layer);

	bool loadFreemiumBar();
	
	void	setValue(int typeItem, int newValue);
	int		getValue(int typeItem);

	void	setAvatar(string strNewAvatar);
	void	setVisible(int typeItem, bool state);
	void	setTitle(string text);
	void	setPosition(Vec2 pos) { m_pNode->setPosition(pos); }
	void	runEnemyBar();
	bool	subHeath(int value);
	void	resetHeathBar();
	void	setInfoEnemyBar(LevelInfo* m_pCurrentLevelInfo);
	void	setHeath(int heath) { m_iHeath = heath; }
	float	getHeight();

	static Size		SIZE_SUB_FREEMIUMBAR;
	static Size		SIZE_FREEMIUMBAR;
private:
	virtual bool init();
	virtual bool initEnemyBar();
	
	static FreemiumBar* s_pInstance;
	int m_iEXP;
	int m_iCash;
	int m_iGem;
	int m_iWeapon;

protected:
	Node* m_pNode;
	Layer* m_pLayer;
	int m_iZOrder;

	int m_iHeath;
	Node* m_pUserTable;
	Node* m_pTopTable;

	
	Sprite* m_pGem;
	Sprite* m_pCash;
	Sprite* m_pEXP;
	Sprite* m_pWeapons;
	EnemyBar* m_pEnemyBar;
	
// User Panel
	ui::Text*	m_pNameText;
	Sprite*		m_pAvatar;
	ui::Text*	m_pLevelText;
	ui::LoadingBar* m_pHeathProgress;

	ui::Text* m_pHeathText;
	ui::Text* m_pEXPText;
	ui::Text* m_pCashText;
	ui::Text* m_pGemText;
	ui::Text* m_pNumWeapons;
	ui::Text* m_pTitle;
};

#define FREEMIUM_BAR	FreemiumBar::GetInstance()
#endif
