#include "GameBar.h"

bool GameBar::init()
{
	if (!Node::init())
		return false;

	this->setTag(TAG_DEFAULT);
	return true;
}

void GameBar::setActive(bool isActive)
{
	m_bIsActive = isActive;
	this->setVisible(isActive);
}

void GameBar::reset()
{
	setActive(false);
	m_pProgess->setPercentage(100);
}