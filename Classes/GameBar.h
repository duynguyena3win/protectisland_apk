#ifndef __GAMEBAR_H__
#define __GAMEBAR_H__

#include "Config.h"

enum LEVEL_LAYER_GAMEBAR
{
	LLGB_BACKGROUND,
	LLGB_PROGESS,
	LLGB_BORDER,
	LLGB_ICON1,
	LLGB_ICON2,
	LLGB_ICON3,
};

class GameBar : public Node
{
public:
	bool init();
	virtual void setPercent(float percent) { m_pProgess->setPercentage(percent); }
	virtual int getPercent() { return m_pProgess->getPercentage(); }
	virtual void setActive(bool isActive);
	Node* getNode() { return this; }
	virtual void reset();
	Vec2 getCenter() { return Vec2(m_pSize.width * getScale() / 2, 0); }

protected:
	ProgressTimer* m_pProgess;
	bool m_bIsActive;
	Size m_pSize;
};

#endif