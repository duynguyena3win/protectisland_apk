#include "GameEntity.h"
#include"Utils.h"
#include "VisibleRect.h"
#include "EffectEntity.h"

bool GameEntity::init()
{
	m_pNode = Node::create();
	m_pNode->setUserData(this);
	m_pNode->setTag(TAG_DEFAULT);
	
	m_pLayer->addChild(m_pNode, m_pZOrder);
	m_pTargetEnemy = 0;
	return true;
}

void GameEntity::setActive(bool isActive)
{
	m_bIsActive = isActive;
	m_pNode->setVisible(isActive);
	if (m_iID != ID_BULLET)
		m_pInfo->setActive(isActive);
}

void GameEntity::setPosition(Vec2 pos, Size sizeItem)
{
	auto posNew = pos;
	m_pNode->setPosition(posNew);
	if (m_iID != ID_BULLET)
		m_pInfo->setPosition(posNew + POSITION_ENTITYINFO_DEFAULT - m_pInfo->getCenter());
}

void GameEntity::runMovingAction()
{
	m_pNode->stopAllActions();
	m_pNode->runAction(m_pMovingAction);
}

void GameEntity::runAction(Action* action)
{
	m_pNode->stopAllActions();
	m_pNode->runAction(action);
}

bool GameEntity::isInsideArea(GameEntity* item)
{
	auto distance = getPosition().distance(item->getPosition());
	if (distance < m_pStateItem->m_pRange)
		return true;
	return false;
}

bool GameEntity::takeDamage(float damage, int typeDamge)
{
	if (isDead())
		return false;

	m_pStateItem->m_pHeath -= Utils::calculateDamage(m_pStateItem->m_pArmor, damage);
	if (m_iID != ID_BULLET)
	{
		float percent = m_pStateItem->m_pHeath / m_pStateItem->m_pMaxHeath * 100;
		if (m_pInfo)
			m_pInfo->setPercent(percent);
	}
	return true;
}

void GameEntity::effectFinishCallback(int idEffect, int damage)
{
	switch (idEffect)
	{
	case ID_FOGBULLET:
		m_pStateItem->m_pSpeedMove += damage;
		setPosition(getPosition());
		break;
	}
}

bool GameEntity::isHaveTarget()
{
	if (m_pTargetEnemy == 0)
		return false;
	else if (m_pTargetEnemy->isActive())
		return true;
	return false;
}

void GameEntity::stopAllActions()
{
	m_pNode->stopAllActions();
	m_pNode->unscheduleAllCallbacks();
	if (m_pInfo)
		m_pInfo->stopAllActions();
}

bool GameEntity::isDead()
{
	return m_pStateItem->isDead();
}

void GameEntity::pause()
{
	m_pNode->pause();
	m_pBodyEntity->pause();
	for (auto barrel : m_pBarrelEntities)
		barrel->pause();
	m_pInfo->pause();
}

void GameEntity::resume()
{
	m_pNode->resume();
	m_pBodyEntity->resume();
	for (auto barrel : m_pBarrelEntities)
		barrel->resume();
	m_pInfo->resume();
}

void GameEntity::reset()
{
	m_pInfo->reset();
	m_pStateItem->reset();
}

void GameEntity::setPathMoving(vector<Vec2> path)
{
	if (path.size() != 0)
	{
		m_vPathMoving.clear();
		m_vPathMoving = path;
	}
}