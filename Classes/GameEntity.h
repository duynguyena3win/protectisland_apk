#ifndef __GAMEENTITY_H__
#define __GAMEENTITY_H__

#include "Config.h"
#include "ItemState.h"
#include "EntityInfo.h"
#include <spine/spine-cocos2dx.h>
using namespace spine;

class GameScene;
class EffectEntity;

class GameEntity
{
public:
	ItemState* m_pStateItem;
	Layer* m_pLayer;
	Node* m_pNode;
	int m_pZOrder;

public:
	CC_SYNTHESIZE(GameScene*, _delegate, Delegate);

	virtual bool init();
	virtual void setPosition(Vec2 pos, Size sizeItem = Size(1,1));
	Vec2 getPosition() { return m_pNode->getPosition(); }
	virtual void setRotate(Vec2 point) = 0;
	virtual float getRotate() { return m_pRotBase; }
	
	virtual void runAction(Action* action);
	
	virtual Node* getNode() { return m_pNode; }
	virtual void attack(bool isAttack) {};
	virtual void shooting() = 0;
	virtual void destroy() = 0;	// dead
	virtual void finish() = 0;	// make damge
	virtual bool isInsideArea(GameEntity* item);
	virtual bool isAttack() { return m_bIsAttack; }
	virtual Vec2 getCenter() { return m_pNode->getPosition(); };
	virtual void setState(int state) {};
	virtual void setSpeedMove(float speed) { m_pStateItem->m_pSpeedMove = speed; }
	virtual void setSprite(int strSprite) { }
	virtual int getState() { return m_pState; }
	virtual bool isActive() { return m_bIsActive; }
	virtual void setActive(bool isActive);
	int getId() { return m_iID; }
	virtual float getDamage() { return m_pStateItem->m_pDamge; }
	virtual void setDamage(float newDamage) { m_pStateItem->m_pDamge = newDamage; }
	virtual bool takeDamage(float damage, int typeDamge);
	virtual void setTarget(GameEntity* target) { m_pTargetEnemy = target; }
	virtual bool isHaveTarget();
	virtual void stopAllActions();
	virtual bool isDead();
	virtual void setTargetPoint(Vec2 target) { m_pTargetPoint = target; }
	virtual int getTag() { return m_pNode->getTag(); }
	virtual void setTag(int tag) { m_pNode->setTag(tag); }
	
	GameEntity* getTarget() { return m_pTargetEnemy; }
	virtual void pause();
	virtual void resume();
	void effectFinishCallback(int idEffect, int damage);
	void reset();

	// Action for Entity
	virtual Action*		createAction(Vec2 target = Vec2(0, 0)) { return 0; }
	virtual	Action*		createActionMoving() { return NULL; }
	
	Action*				createActionBarInfo() { return 0; }
	virtual Action*		createBarInfoMoving() { return 0; }
	virtual	void		runMovingAction();
	void setPathMoving(vector<Vec2> path);

protected:
	virtual void setStateEntity() {};
	Sprite* m_pBodyEntity;
	EntityInfo* m_pInfo;

	GameEntity* m_pTargetEnemy;

	cocos2d::Vector<Sprite*> m_pBarrelEntities;
	int m_pState;
	int m_iID;
	bool m_bIsActive;
	bool m_bIsAttack;
	Vec2 m_pTargetPoint;
	float m_pRotBase;
	std::set<EffectEntity*> m_pListEffect;
	Action* m_pMovingAction;
	Action* m_pMovingBarInfoAction;
	vector<Vec2> m_vPathMoving;

protected:	// Make effect
	SkeletonAnimation* m_pDestroy;
	ParticleSun* m_pTrailWater;		// Only for ship
};

#endif