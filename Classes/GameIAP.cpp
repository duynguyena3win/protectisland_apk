#include "GameIAP.h"

GameIAP* GameIAP::s_pInstance = 0;


ItemIAP::ItemIAP()
{
	m_eTypeItem = TI_UNKNOW;
	m_sIdItem = "";
	m_sNameItem = "";
	m_iValue = 0;
	m_fRealMoney = 0.0f;
}

ItemIAP::ItemIAP(std::string id, std::string name, int type, int value, double realmoney)
{
	m_eTypeItem = type;
	m_sIdItem = id;
	m_sNameItem = name;
	m_iValue = value;
	m_fRealMoney = realmoney;
}

ItemIAP::~ItemIAP()
{
	m_eTypeItem = TI_UNKNOW;
	m_sIdItem.clear();
	m_sNameItem.clear();
	m_iValue = 0;
	m_fRealMoney = 0.0f;
}

GameIAP* GameIAP::GetInstance()
{
	if(!s_pInstance)
		s_pInstance = new GameIAP();
	return s_pInstance;
}

GameIAP::GameIAP()
{
	initGEMPackageData();
	initEXPPackageData();
}

bool GameIAP::initGEMPackageData()
{
	m_pListGEM.push_back(new ItemIAP("com.duynguyenit.protectisland.gem100", "Pile of Gems", TI_GEM, 100, 10000));
	m_pListGEM.push_back(new ItemIAP("com.duynguyenit.protectisland.gem220", "Bag of Gems", TI_GEM, 220, 20000));
	m_pListGEM.push_back(new ItemIAP("com.duynguyenit.protectisland.gem650", "Sack of Gems", TI_GEM, 650, 50000));

	return true;
}

bool GameIAP::initEXPPackageData()
{
	m_pListEXP.push_back(new ItemIAP("com.duynguyenit.protectisland.exp200", "Pile of EXPs", TI_EXP, 200, 15000));
	m_pListEXP.push_back(new ItemIAP("com.duynguyenit.protectisland.exp450", "Bag of EXPs", TI_EXP, 450, 30000));
	m_pListEXP.push_back(new ItemIAP("com.duynguyenit.protectisland.exp900", "Sack of EXPs", TI_EXP, 900, 50000));

	return true;
}