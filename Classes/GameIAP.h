#ifndef __GAMEIAP_H__
#define __GAMEIAP_H__

#include "Config.h"

enum TYPE_IAP
{
	TI_UNKNOW,
	TI_GEM,
	TI_EXP
};

class ItemIAP
{
public:
	std::string m_sIdItem;
	std::string m_sNameItem;
	int			m_eTypeItem;
	int			m_iValue;
	double		m_fRealMoney;

public:
	ItemIAP();
	ItemIAP(std::string id, std::string name, int type, int value, double m_fRealMoney);
	~ItemIAP();
};

class GameIAP
{
public:
	static GameIAP* GetInstance();
	vector<ItemIAP*> m_pListGEM;
	vector<ItemIAP*> m_pListEXP;

private:
	GameIAP();
	bool initGEMPackageData();
	bool initEXPPackageData();

private:
	static GameIAP* s_pInstance;
};

#endif
