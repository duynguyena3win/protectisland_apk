#include "GameProgress.h"

GameProgress* GameProgress::create(string str_bg, string str_progess)
{
	vector<string> list;
	list.push_back(str_progess);
	return create(str_bg, list);
}

GameProgress* GameProgress::create(string str_bg, vector<string> str_progess)
{
	auto gbar = new GameProgress();
	if (str_bg.empty())
		str_bg = SPRITE_SUBITEMS_GAMEBAR_BACKGROUND;

	if (str_progess.empty())
		str_progess.push_back(SPRITE_SUBITEMS_GAMEBAR_PROGRESS);
	
	if (gbar->init(str_bg, str_progess))
	{
		return gbar;
	}
	CC_SAFE_DELETE(gbar);
	return gbar;
}

bool GameProgress::init(string background, vector<string> progess)
{
	if (!GameBar::init())
		return false;

	auto back = Sprite::create(background);
	m_pSize = back->getContentSize();
	back->setAnchorPoint(Vec2(0, 0.5));
	back->setPosition(Vec2::ZERO);
	this->addChild(back, 1);

	// load image
	char str[100] = { 0 };
	char name[100] = { 0 };
	for (int i = 0; i < progess.size(); i++)
	{
		auto frame = SpriteFrame::create(progess[i], Rect(0, 0, 279, 24)); //we assume that the sprites' dimentions are 40*40 rectangles.
		sprintf(name, "HPProgess%d", i);
		SpriteFrameCache::getInstance()->addSpriteFrame(frame, name);
	}
	
	auto progessBar = Sprite::createWithSpriteFrameName("HPProgess1");
	m_pProgess = ProgressTimer::create(progessBar);
	m_pProgess->setType(ProgressTimer::Type::BAR);
	m_pProgess->setMidpoint(Vec2(0, 1));
	m_pProgess->setBarChangeRate(Vec2(1, 0));
	m_pProgess->setPercentage(100);
	m_pProgess->setAnchorPoint(Vec2(1, 0.5));
	m_pProgess->setPosition(Vec2(m_pSize.width, 0));
	this->addChild(m_pProgess, 0);
	m_iChange = 0;

	

	this->schedule([this](float dt){
		if (m_iChange == 0)
		{
			m_pProgess->getSprite()->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("HPProgess0"));
			m_iChange = 1;
		}
		else
		{
			m_pProgess->getSprite()->setSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("HPProgess1"));
			m_iChange = 0;
		}
	}, 0.3f, StringUtils::format("%dasdjkh%d", RandomHelper::random_int(0, 1234), RandomHelper::random_int(0, 1234)));

	return true;
}
