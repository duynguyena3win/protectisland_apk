#ifndef __GAMEPROGRESS_H__
#define __GAMEPROGRESS_H__

#include "GameBar.h"

class GameProgress : public GameBar
{
public:
	static GameProgress* create(string str_bg, string str_progess);
	static GameProgress* create(string str_bg, vector<string> str_progess);
	bool init(string background, vector<string> progess);

protected:
	int m_iChange;
};

#endif