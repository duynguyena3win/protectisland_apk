#include "GameScene.h"
#include "VisibleRect.h"
#include "Utils.h"
#include "ActionHelper.h"
#include "EntityFactory.h"
#include "EffectFactory.h"
#include "HudGame.h"
#include "MapGame.h"
#include "FreemiumBar.h"
#include "PopUpManager.h"

GameScene*	GameScene::s_pInstance = 0;

cocos2d::Scene* GameScene::createScene(int level)
{
	//auto scene = Scene::create();
	auto scene = Scene::createWithPhysics();
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	
	auto gamelayer = GameScene::create(level);
	gamelayer->setPhysicsWorld(scene->getPhysicsWorld());
	scene->addChild(gamelayer, LLG_MAP, "ID_GAMESCENE");

	// set Level
	auto mapGame = MapGame::create(gamelayer);
	scene->addChild(mapGame, LLG_BASE);
	gamelayer->setMap(mapGame);

	// set HudGame
	auto hud = HudGame::create(gamelayer);
	scene->addChild(hud, LLG_HUBMENU);
	gamelayer->setHUD(hud);
	
	s_pInstance = gamelayer;
	return scene;
}

bool GameScene::init(int level)
{
	if (!Layer::init())
		return false;
	LOGI("Init GameScene Start!");
	FreemiumBar::BeginLevel(this);
	FREEMIUM_BAR->setVisible(FI_SUBTABLE, true);
	FREEMIUM_BAR->setVisible(FI_CASH, true);
	FREEMIUM_BAR->setVisible(FI_HEATHBAR, true);
	FREEMIUM_BAR->setVisible(FI_ENEMYBAR, true);
	//FREEMIUM_BAR->setTitle("Mission1 - Level 1");
	FREEMIUM_BAR->setHeath(GAME_HEATH_POINT);

	Utils::loadSpriteCache();
	initTouch();
	initGameInformation(level);
	this->scheduleUpdate();
	PopUpManager::GetInstance(this);
	LOGI("Init GameScene Finish!");
	return true;
}


void GameScene::initGameInformation(int level)
{
	m_iCurrentLevel = level;
	m_pCurrentLevelInfo = 0;
	m_iCashEarn = 2000;
	m_iGemEarn = 100;
	FreemiumBar::GetInstance(this)->setValue(FI_CASH, m_iCashEarn);
	FreemiumBar::GetInstance(this)->setValue(FI_GEM, m_iGemEarn);
	FreemiumBar::GetInstance(this)->setValue(FI_EXP, 0);
	m_pGameState = GS_RUNNING;
}

void GameScene::update(float delta)
{
	switch (m_pGameState)
	{
	case GAMESTATE::GS_RUNNING:
	{
								  if (!m_pHud->isFinishLoading())
									  return;
								  if (!m_pMapGame->isFinishLoading())
									  return;

								  m_pMapGame->update(delta);
								  updateCheckMap();
	}
		break;
	default:
		break;
	}
}

void GameScene::loadLevel(int level)
{
	m_iCurrentLevel = level;
}

void GameScene::initTouch()
{
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	auto listener = EventListenerTouchOneByOne::create();

	listener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(GameScene::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(GameScene::onTouchEnded, this);

	dispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	auto pKeybackListener = EventListenerKeyboard::create();
	pKeybackListener->onKeyReleased = CC_CALLBACK_2(GameScene::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(pKeybackListener, this);
}

GameScene * GameScene::create(int level)
{
	LOGI("Create GameScene here!");
	auto scene = new GameScene();
	if (scene->init(level))
	{
		scene->autorelease();
		return scene;
	}
	CC_SAFE_DELETE(scene);

	return NULL;
}

void GameScene::onTouchMoved(Touch *touch, Event *event)
{
	if (m_pGameState == GS_RUNNING)
	{
		m_pMapGame->onTouchMove(touch, event);
	}
}

bool GameScene::onTouchBegan(Touch *touch, Event *event)
{
	if (m_pGameState == GS_RUNNING)
	{
		m_pMapGame->onTouchBegin(touch, event, 0);
	}
	return true;
}

void GameScene::updateCheckMap()
{
	auto point1 = m_pMapGame->getPosition();
	auto size = m_pMapGame->getSize();
	auto point2 = m_pMapGame->getPosition() + size;
	Vec2 newPos = Vec2(0, 0);

	auto b1 = point1.x > 0 || point1.y > 0;
	auto b2 = point2.x < DESIGN_RESOLUTION_WIDTH || point2.y < DESIGN_RESOLUTION_HEIGHT;

	if (point1.x > 0)
	{
		newPos.x = -1;
		newPos.y = m_pMapGame->getPosition().y;
	}
	if (point1.y > 0)
	{
		newPos.y = -1;
		newPos.x = m_pMapGame->getPosition().x;
	}
	if (point2.x < DESIGN_RESOLUTION_WIDTH)
	{
		newPos.x = -(m_pMapGame->getSize().x - DESIGN_RESOLUTION_WIDTH) + 10;
		newPos.y = m_pMapGame->getPosition().y;
	}
	if (point2.y < DESIGN_RESOLUTION_HEIGHT)
	{
		newPos.x = m_pMapGame->getPosition().x;
		newPos.y = -(m_pMapGame->getSize().y - DESIGN_RESOLUTION_HEIGHT) + 1;
	}
	
	if (b1 || b2)
	{
		log("update MAP");
		m_pMapGame->stopAllActions();
		m_pMapGame->setPosition(newPos);
	}
}

void GameScene::onTouchEnded(cocos2d::Touch *touch, Event *event)
{
	if (m_pGameState == GAMESTATE::GS_RUNNING)
	{
		m_pMapGame->onTouchEnd(touch, event);
	}
}

void GameScene::setHUD(HudGame* hud)
{
	m_pHud = hud;
	FreemiumBar::GetInstance()->setInfoEnemyBar(m_pCurrentLevelInfo);
}

void GameScene::setMap(MapGame* map)
{
	m_pMapGame = map;
	m_pMapGame->setPosition(Point::ZERO);
	EntityFactory::getInstance(map);
	EffectFactory::getInstance(map);
	loadLevel(m_iCurrentLevel);
	m_pMapGame->InitGame("LevelMaps/Level" + StringUtils::toString(m_iCurrentLevel) + ".tmx");
}

void GameScene::setRadar(RadarGame* rad)
{
	m_pRadar = rad;
}

void GameScene::enemyFinishCallback(int damge)
{
	m_pMapGame->getMap()->stopAllActions();
	m_pMapGame->getMap()->setPosition(Vec2::ZERO);
	m_pMapGame->getMap()->runAction(ActionHelper::Shaking(0.3f, 2.0f));
	if (FreemiumBar::GetInstance()->subHeath(damge))
	{
		std::map<string, string> infors;
		infors.insert(std::pair<string, string>(KEY_GAMEUI_TITLE_TEXT, "Mission Fail"));

		string keyGemText = KEY_GAMEUI_RESOURCE_GEM;	keyGemText += KEY_GAMEUI_RESOURCE_TEXT;
		infors.insert(std::pair<string, string>(keyGemText, StringUtils::toString(m_iGemEarn)));

		string keyExpText = KEY_GAMEUI_RESOURCE_EXP;	keyExpText += KEY_GAMEUI_RESOURCE_TEXT;
		infors.insert(std::pair<string, string>(keyExpText, StringUtils::toString(FreemiumBar::GetInstance()->getValue(FI_EXP))));

		infors.insert(std::pair<string, string>(KEY_GAMEUI_RESOURCE_STAR, StringUtils::toString(0)));

		auto popUp = PopUpManager::GetInstance(this)->getPopUp(PU_RESULT);
		popUp->setData(infors);
		PopUpManager::GetInstance()->show(popUp);
	}
}

void GameScene::enemyBeKillCallback(int gold, Vec2 pos)
{
	//
	int rand = RandomHelper::random_int(0, 1);
	if (rand == 1)
	{
		m_iCashEarn += gold;
		FreemiumBar::GetInstance(this)->setValue(FI_CASH, m_iCashEarn);
		auto cash = EffectFactory::getInstance()->getObject(ID_CASH);
		cash->setText(gold);
		cash->setPosition(pos);
		
	}
	else
	{
		m_iGemEarn += 10;
		FreemiumBar::GetInstance(this)->setValue(FI_GEM, m_iGemEarn);
		auto cash = EffectFactory::getInstance()->getObject(ID_GEM);
		cash->setText(10);
		cash->setPosition(pos);
	}
	//
}

void GameScene::startGame()
{
	m_pMapGame->startGame();
	FreemiumBar::GetInstance()->runEnemyBar();
}

void GameScene::showPopUp(int type)
{
	setStateGame(GAMESTATE::GS_PAUSE);

	std::map<string, string> infors;
	infors.insert(std::pair<string, string>(KEY_GAMEUI_BODY_TEXT, "Game is Pause!"));
	auto Popup = PopUpManager::GetInstance(this)->getPopUp(PU_NOTICE, infors);
	PopUpManager::GetInstance()->show(Popup);
}

void GameScene::showNotice(string title, string text)
{
	std::map<string, string> infors;
	infors.insert(std::pair<string, string>(KEY_GAMEUI_TITLE_TEXT, title));
	infors.insert(std::pair<string, string>(KEY_GAMEUI_BODY_TEXT, text));
	auto Popup = PopUpManager::GetInstance(this)->getPopUp(PU_NOTICE, infors);
	PopUpManager::GetInstance()->show(Popup);
}

void GameScene::popUpclose()
{
	setStateGame(GAMESTATE::GS_RUNNING);
}

void GameScene::setStateGame(int state)
{
	if (m_pGameState == state)
		return;

	m_pGameState = state;
	switch (state)
	{
	case GAMESTATE::GS_PAUSE:
		pauseGame();
		break;
	
	case GAMESTATE::GS_RUNNING:
		resumeGame();
		break;
	}
}

void GameScene::pauseGame()
{
	this->pause();
	m_pMapGame->pauseGame();
	m_pHud->pauseGame();
	for (auto n : this->getChildren())
	{
		if (n->getTag() == TAG_DEFAULT)
		{
			n->pause();
		}
	}
}

void GameScene::resumeGame()
{
	m_pMapGame->resumeGame();
	m_pHud->resumeGame();
	for (auto n : this->getChildren())
	{
		if (n->getTag() == TAG_DEFAULT)
		{
			n->resume();
		}
	}
	this->resume();
}

void GameScene::callBackPopUpEvent(int idEvent, int typePopUp)
{
	switch (typePopUp)
	{
	case PU_TRADE:
		if (idEvent == ID_YES)
		{
			m_iCashEarn = FreemiumBar::GetInstance()->getValue(FI_CASH);
			m_pMapGame->sellTargetItem(m_iCashEarn);
			FreemiumBar::GetInstance(this)->setValue(FI_CASH, m_iCashEarn);
		}
		break;
	default:
		break;
	}
}