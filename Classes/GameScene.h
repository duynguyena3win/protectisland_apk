#ifndef __GAMESCENE_H__
#define __GAMESCENE_H__

#include "Config.h"
#include "GameEntity.h"
#include "LevelHelper.h"
#include "cocostudio\CocoStudio.h"

using namespace cocostudio;

class HudGame;
class MapGame;
class RadarGame;

enum GAMESTATE
{
	GS_PAUSE,
	GS_RUNNING,
};

class GameScene : public cocos2d::Layer
{
public:
	int m_pGameState;

public:
	static GameScene*	GetInstance() { return s_pInstance; }
	static cocos2d::Scene* createScene(int level = -1);
	virtual bool init(int level = -1);
	virtual void initTouch();
	virtual void loadLevel(int level);
	void update(float delta);
	static GameScene* create(int level = -1);
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchMoved(cocos2d::Touch *touch, Event *event);
	void onTouchEnded(cocos2d::Touch *touch, Event *event);
	void enemyFinishCallback(int damge);
	void enemyBeKillCallback(int gold, Vec2 pos);
	int m_iCurrentLevel;
	LevelInfo* m_pCurrentLevelInfo;
	void startGame();
	void tick(float dt);
	void showPopUp(int type);
	void showNotice(string title, string text);
	void popUpclose();
	void setStateGame(int state);
	void pauseGame();
	void resumeGame();
	HudGame* getHUD() { return m_pHud; }
	void callBackPopUpEvent(int idEvent, int typePopUp);
	void onActionOK(cocostudio::Armature *pArmature, cocostudio::MovementEventType eventType, const char *animationID);
	MapGame* getMap() { return m_pMapGame; }

protected:
	void initGameInformation(int level);
	void setHUD(HudGame* hud);
	void setMap(MapGame* map);
	void setRadar(RadarGame* rad);

	// Game Function
	HudGame* m_pHud;
	MapGame* m_pMapGame;
	void updateCheckMap();
	RadarGame* m_pRadar;

private:
	static GameScene* s_pInstance;
	cocos2d::PhysicsWorld* sceneWorld;
	void setPhysicsWorld(cocos2d::PhysicsWorld *world) { sceneWorld = world; }
	int m_iGemEarn;
	int m_iCashEarn;
};

#endif // !__GAMESCENE_H__
