#ifndef __GEMEFFECT_H__
#define __GEMEFFECT_H__

#include "EffectEntity.h"

class GemEffect : public EffectEntity
{
public:
	static GemEffect* create(Layer* layer);
	bool init();
	void setPosition(Vec2 pos);
	Action* createAction();
	void runAction() { }
};

#endif