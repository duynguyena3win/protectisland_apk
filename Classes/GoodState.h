#ifndef __GOODSTATE_H__
#define __GOODSTATE_H__

#include "Config.h"
#include "ItemState.h"

class GoodState : public ItemState
{
public:
	GoodState();
	virtual ~GoodState();
};

#endif