#include "HeathEffect.h"
#include "Utils.h"

HeathEffect* HeathEffect::create(Layer* layer)
{
	auto heart = new HeathEffect();
	heart->m_pLayer = layer;
	if (heart->init())
	{
		return heart;
	}
	CC_SAFE_DELETE(heart);
	return NULL;
}

bool HeathEffect::init()
{
	if (!EffectEntity::init())
	{
		return false;
	}
	auto image = Sprite::create(SPRITE_EFFECT_HEART_ITEM);
	image->setScale(SCALE_EFFECT_SPRITE);
	m_pNode->addChild(image);

	m_iID = ID_CASH;
	setActive(false);
	return true;
}

void HeathEffect::setPosition(Vec2 pos)
{
	EffectEntity::setPosition(pos);
	EffectEntity::runAction(createAction());
}

Action* HeathEffect::createAction()
{
	Vec2 pos = getPosition();
	m_pNode->setPosition(pos + Vec2(20, 5));
	setActive(true);
	m_pNode->setScale(0.6f);

	auto fly = MoveTo::create(1.5f, Vec2(pos.x + 30, pos.y + 15));
	auto scale = ScaleTo::create(1.0f, 1.0f);
	auto spawn = Spawn::createWithTwoActions(fly, scale);
	auto fadeout = FadeOut::create(0.25f);
	auto fadein = FadeIn::create(0.0f);
	auto callfunc = CallFunc::create([this](){
		stopAllActions();
	});
	return Sequence::create(fadein, spawn, fadeout, callfunc, NULL);
}