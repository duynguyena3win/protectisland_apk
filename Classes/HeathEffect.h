#ifndef __HEATHEFFECT_H__
#define __HEATHEFFECT_H__

#include "EffectEntity.h"

class HeathEffect : public EffectEntity
{
public:
	static HeathEffect* create(Layer* layer);
	bool init();
	void setPosition(Vec2 pos);
	void runAction() { }
	Action* createAction();
};

#endif