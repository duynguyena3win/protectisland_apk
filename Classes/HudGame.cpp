#include "HudGame.h"
#include "Utils.h"
#include "VisibleRect.h"
#include "MenuSpecialItem.h"
#include "MapGame.h"
#include "FreemiumBar.h"

HudGame* HudGame::create(GameScene* gamelayer)
{
	auto hud = new HudGame();
	hud->m_pGameLayer = gamelayer;

	if (hud && hud->init())
	{
		hud->autorelease();
		return hud;
	}
	return hud;
}

bool HudGame::init()
{
	if (!Layer::init())
	{
		return false;
	}
	m_bFinishLoading = false;

	initTopBar();
	initHudButton();
	initSpecialButton();

	return true;
}

void HudGame::initHudButton()
{
	// Start button
	auto btnStart = Utils::addButton(SPRITE_BUTTON_START_ENABLE, SPRITE_BUTTON_START_DISABLE, SPRITE_BUTTON_START_DISABLE, [this](Ref* ref)
	{
		MenuItem* btn = (MenuItem*) ref;
		//btn->setVisible(false);
		m_pGameLayer->startGame();
	});
	btnStart->setPosition(VisibleRect::bottom() + Vec2(0, 80));

	//// Pause button
	//auto btnPause = Utils::addButton(SPRITE_BUTTON_PAUSE_ENABLE, SPRITE_BUTTON_PAUSE_DISABLE, SPRITE_BUTTON_PAUSE_DISABLE, [this](Ref* ref)
	//{
	//	m_pGameLayer->showPopUp(PU_OPTION);
	//});
	//btnPause->setScale(0.8f);
	//btnPause->setPosition(VisibleRect::top() + Vec2(0, -50));

	auto menu = Menu::create(btnStart, NULL);
	menu->setPosition(Point::ZERO);
	menu->setTag(TAG_DEFAULT);
	addChild(menu, 80);
	//
}

void HudGame::initSpecialButton()
{
	MenuSpecialItem::getInstance(this)->openMenu();
}

bool HudGame::initTopBar()
{
	auto sizeTable = FreemiumBar::SIZE_FREEMIUMBAR;
	auto sizeSubTable = FreemiumBar::SIZE_SUB_FREEMIUMBAR;
	vector<string> listProgess;
	listProgess.push_back(SPRITE_HUDGAME_HEATHBAR_PROGESS0);
	listProgess.push_back(SPRITE_HUDGAME_HEATHBAR_PROGESS1);

	m_bFinishLoading = true;
	return true;
}

void HudGame::resumeGame()
{
	for (auto n : this->getChildren())
	{
		if (n->getTag() == TAG_DEFAULT)
		{
			n->resume();
		}
	}
	this->resume();
}

void HudGame::callSpecialItem(int idSI)
{
	switch (idSI)
	{
	case IS_BOOMER:
		m_pGameLayer->getMap()->callSpecialBoom();
		break;
	case IS_HELP:
		m_pGameLayer->getMap()->callSpecialHeathHelp();
		break;
	}
	
}

void HudGame::pauseGame()
{
	for (auto n : this->getChildren())
	{
		if (n->getTag() == TAG_DEFAULT)
		{
			n->pause();
		}
	}
	this->pause();
}