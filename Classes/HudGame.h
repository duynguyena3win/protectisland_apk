#ifndef __HUDGAME_H__
#define __HUDGAME_H__

#include "Config.h"
#include "GameScene.h"

class LevelInfo;
class RadarGame;

class HudGame : public cocos2d::Layer
{
public:
	static HudGame* create(GameScene* gamelayer);
	virtual bool init();
	bool isFinishLoading() { return m_bFinishLoading; }
	void resumeGame();
	void pauseGame();
	void callSpecialItem(int idSI);

private:
	bool initTopBar();
	void initHudButton();
	void initSpecialButton();

protected:
	GameScene* m_pGameLayer;
	bool m_bFinishLoading;
};

#endif
