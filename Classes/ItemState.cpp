#include "ItemState.h"

ItemState::ItemState()
{
	m_sName = "Unknow";
	m_pValue = 100;
	m_pRange = 100;
	m_pSpeedShoot = 100;
	m_pSpeedMove = 200;
	m_pSpeedBullet = 300;
	m_pDamge = 10;
	m_pArmor = 3;
	m_pMaxHeath = m_pHeath = 100;
}

ItemState::ItemState(ItemData* data)
{
	m_sName = data->Name;
	m_pValue = data->Value;
	m_pData = data;
	m_pRange = m_pData->Range;
	m_pSpeedShoot = m_pData->SpeedShoot;
	m_pSpeedMove = m_pData->SpeedMove;
	m_pSpeedBullet = m_pData->SpeedBullet;
	m_pDamge = m_pData->Damage;
	m_pArmor = m_pData->Armor;
	m_pMaxHeath = m_pHeath = m_pData->MaxHeath;
}

void ItemState::reset()
{
	m_pValue = m_pData->Value;
	m_pRange = m_pData->Range;
	m_pSpeedShoot = m_pData->SpeedShoot;
	m_pSpeedMove = m_pData->SpeedMove;
	m_pSpeedBullet = m_pData->SpeedBullet;
	m_pDamge = m_pData->Damage;
	m_pArmor = m_pData->Armor;
	m_pMaxHeath = m_pHeath = m_pData->MaxHeath;
}


bool ItemState::isDead()
{
	return (m_pHeath <= 0);
}

ItemState::~ItemState()
{

}