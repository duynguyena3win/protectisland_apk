#ifndef __ITEMSTATE_H__
#define __ITEMSTATE_H__

#include "Utils.h"

class ItemState
{
public:
	string m_sName;
	int m_pValue;
	int m_pRange;
	int m_pSpeedShoot;
	int m_pSpeedMove;
	int m_pSpeedBullet;
	float m_pDamge;
	float m_pMaxHeath;
	float m_pHeath;
	float m_pArmor;
	ItemData* m_pData;

public:
	ItemState();
	ItemState(ItemData* data);
	virtual void reset();
	bool isDead();
	virtual ~ItemState();
};

#endif