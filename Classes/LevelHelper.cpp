#include "LevelHelper.h"


LevelHelper* LevelHelper::s_pInstance = 0;

LevelHelper* LevelHelper::getInstance()
{
	if (!s_pInstance)
		s_pInstance = new LevelHelper();
	return s_pInstance;
}

LevelInfo* LevelHelper::getData(int level)
{
	// default test with level = 1;
	LevelInfo* outPut = new LevelInfo();
	// Read file level.pList
	auto data = FileUtils::getInstance()->getValueMapFromFile(getStringLevel(level));

	outPut->m_pName = data.at("Name").asString();
	outPut->m_pLevel = data.at("Level").asInt();
	outPut->m_NumEnemies = data.at("NumEnemies").asInt();
	outPut->m_NumWaves = data.at("NumWaves").asInt();

	auto waves = data.at("WaveTimes").asString();
	phareWaves(outPut, waves);

	auto enemies = data.at("Enemies").asString();
	phareIdEnemies(outPut, enemies);

	auto times = data.at("Times").asString();
	phareTimes(outPut, times);
	
	auto position = data.at("Positions").asString();
	pharePositions(outPut, position);

	outPut->m_pEXP = data.at("EXPLevel").asInt();
	outPut->m_pGold = data.at("GoldLevel").asInt();
	outPut->m_pGoldBonus = data.at("GoldBonus").asInt();
	outPut->m_pGemBonus = data.at("GemBonus").asInt();
	
	return outPut;
}

void LevelHelper::phareIdEnemies(LevelInfo* info, std::string value)
{
	int pos = value.find("{");
	int numPos;
	value = value.substr(pos + 1);
	std::string number;
	do
	{
		numPos = value.find(",", pos);
		if (numPos == -1)
			numPos = value.find("}");
		number = value.substr(pos, numPos - pos);
		pos = numPos + 1;
		info->m_pIdEnemies.push_back(atoi(number.c_str()));
	} while (pos < value.length());
}

void LevelHelper::phareTimes(LevelInfo* info, std::string value)
{
	int pos = value.find("{");
	int numPos;
	value = value.substr(pos + 1);
	std::string number;
	do
	{
		numPos = value.find(",", pos);
		if (numPos == -1)
			numPos = value.find("}");
		number = value.substr(pos, numPos - pos);
		pos = numPos + 1;
		info->m_pTimes.push_back(atof(number.c_str()));
	} while (pos < value.length());
}

void LevelHelper::phareWaves(LevelInfo* info, std::string value)
{
	int pos = value.find("{");
	int numPos;
	value = value.substr(pos + 1);
	std::string number;
	do
	{
		numPos = value.find(",", pos);
		if (numPos == -1)
			numPos = value.find("}");
		number = value.substr(pos, numPos - pos);
		pos = numPos + 1;
		info->m_pWaves.push_back(atof(number.c_str()));
	} while (pos < value.length());
}

void LevelHelper::pharePositions(LevelInfo* info, std::string value)
{
	int pos = value.find("{");
	int numPos;
	value = value.substr(pos + 1);
	std::string number;
	do
	{
		numPos = value.find(",", pos);
		if (numPos == -1)
			numPos = value.find("}");
		number = value.substr(pos, numPos - pos);
		pos = numPos + 1;
		info->m_pPositions.push_back(atof(number.c_str()));
	} while (pos < value.length());
}

std::string LevelHelper::getStringLevel(int level)
{
	std::string str = StringUtils::format("DefaultLevels/Level_%d.plist", level);
	return str;
}