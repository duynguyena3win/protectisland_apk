#ifndef __LEVELHELPER_H__
#define __LEVELHELPER_H__

#include "LevelInfo.h"

class LevelHelper
{
public:
	static LevelHelper* getInstance();
	LevelInfo* getData(int level);
private:
	static LevelHelper* s_pInstance;

	std::string getStringLevel(int level);
	LevelInfo genEnemyData(ValueMap enemy);
	void phareIdEnemies(LevelInfo* info, std::string value);
	void phareTimes(LevelInfo* info, std::string value);
	void phareWaves(LevelInfo* info, std::string value);
	void pharePositions(LevelInfo* info, std::string value);
};

#endif // !__LEVEL_HELPER_H__
