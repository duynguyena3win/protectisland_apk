#include "LevelInfo.h"

LevelInfo::LevelInfo()
{
	m_pName = "";
	m_pLevel = 0;
	m_NumEnemies = 0;
	m_pIdEnemies.clear();
	m_pTimes.clear();
	m_pPositions.clear();
	m_pEXP = m_pGoldBonus = 0;
	m_pGold = m_pGemBonus = 0;
}

float LevelInfo::getEndTime()
{ 
	if (m_pTimes.empty())
		return 0;
	return m_pTimes[m_pTimes.size() - 1];
}