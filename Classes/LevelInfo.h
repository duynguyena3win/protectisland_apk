#ifndef __LEVELINFO_H__
#define __LEVELINFO_H__

#include "Config.h"

class LevelInfo
{
public:
	std::string m_pName;
	int m_pLevel;
	int m_NumEnemies;
	std::vector<int> m_pIdEnemies;
	std::vector<float> m_pTimes;
	std::vector<float> m_pPositions;
	std::vector<vector<Vec2>> m_pMovingLines;

	int m_NumWaves;
	std::vector<float> m_pWaves;
	int m_pEXP;
	int m_pGold;
	int m_pGoldBonus;
	int m_pGemBonus;
	
	LevelInfo();
	float getEndTime();
};

#endif