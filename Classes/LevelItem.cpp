#include "LevelItem.h"
#include "GameScene.h"

LevelItem* LevelItem::create(Layer* layer)
{
	auto li = new LevelItem();
	li->m_pLayer = layer;
	li->m_iZOrder = LLG_ENTITY_UNDERGROUND;
	if (li->init())
	{
		return li;
	}
	CC_SAFE_DELETE(li);
	return NULL;
}

bool LevelItem::init()
{
	m_pNode = CSLoader::createNode(PATH_LEVELITEM_CSB);
	m_pNode->setUserData(this);

	m_pLayer->addChild(m_pNode, m_iZOrder, KEY_GAMEUI_LEVELITEM);

	auto containPanel = m_pNode->getChildByName(KEY_GAMEUI_PANEL_CONTAIN);

	m_pButton = static_cast<ui::Button*>(containPanel->getChildByName(KEY_LEVELITEM_BUTTON));

	m_pPanelDisable = static_cast<Node*>(containPanel->getChildByName(KEY_LEVELITEM_PANELDISABLE));
	m_pPanelEnable = static_cast<Node*>(containPanel->getChildByName(KEY_LEVELITEM_PANELENABLE));
	
	setListenerForButton();
	return true;
}

void LevelItem::setListenerForButton()
{
	m_pButton->addClickEventListener([this](Ref* sender) {
		auto scaleCur = m_pButton->getScale();
		auto scaleIn = ScaleTo::create(0.2, scaleCur - 0.2);
		auto scaleOut = ScaleTo::create(0.2, scaleCur);
		
		auto funcCall = CallFunc::create([this]() {
			auto scene = GameScene::createScene(5);
			auto trans = TransitionFadeDown::create(0.5f, scene);
			Director::getInstance()->pushScene(trans);
		});

		m_pButton->runAction(Sequence::create(scaleIn, scaleOut, DelayTime::create(0.2f), funcCall, NULL));

	});
}

void LevelItem::setEnable(int numStart /*= 0*/)
{
	m_pPanelDisable->setVisible(false);
	m_pPanelEnable->setVisible(true);

	for (int i = 0; i < MAX_LEVELITEM_STARS; i++)
	{
		auto star = m_pPanelEnable->getChildByName(KEY_LEVELITEM_STAR(i));
		if (i < numStart)
			star->setVisible(true);
		else
			star->setVisible(false);
	}
}

void LevelItem::setDisable()
{
	m_pPanelDisable->setVisible(true);
	m_pPanelEnable->setVisible(false);

	m_pButton->setEnabled(false);
}

void LevelItem::setIndex(int ind)
{
	m_iIndexItem = ind;
}
