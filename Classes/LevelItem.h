#ifndef __LEVELITEM_H__
#define __LEVELITEM_H__

#include "Config.h"

#define		PATH_LEVELITEM_CSB			"GameItem/LevelItem.csb"
#define		KEY_LEVELITEM_BUTTON		"Button"
#define		KEY_LEVELITEM_PANELDISABLE	"DisablePanel"
#define		KEY_LEVELITEM_PANELENABLE	"EnablePanel"

#define		KEY_LEVELITEM_STAR(x)			"Star" + StringUtils::toString(x)
#define		MAX_LEVELITEM_STARS			3
#include	"cocostudio\CocoStudio.h"
#include	"ui\UIButton.h"

class LevelItem
{
public:
	static	LevelItem* create(Layer* layer);
	void	setEnable(int numStart = 0);
	void	setDisable();
	void	setPosition(Vec2 pos)	{ m_pNode->setPosition(pos); }
	void	setIndex(int ind);

private:
	bool	init();
	void	setListenerForButton();
	void	clickEvent(cocos2d::Ref *ref);
	
	
public:
	Layer*			m_pLayer;
	Node*			m_pNode;
	int				m_iZOrder;
	int				m_iIndexItem;

private:
	ui::Button*		m_pButton;
	Node*			m_pPanelDisable;
	Node*			m_pPanelEnable;

};

#endif