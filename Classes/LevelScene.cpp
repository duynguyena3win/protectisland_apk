#include "LevelScene.h"
#include "VisibleRect.h"
#include "Utils.h"
#include "GameScene.h"
#include "UserProfile.h"
#include "UpgradeScene.h"
#include "MenuScene.h"
#include "FreemiumBar.h"
#include "LevelItem.h"
#include "ShopScene.h"
#include "UpgradeScene.h"
#include "PopUpManager.h"

cocos2d::Scene* LevelScene::createScene()
{
	auto sence = Scene::create();
	auto layer = LevelScene::create();
	sence->addChild(layer);
	return sence;
}

bool LevelScene::init()
{
	if (!Layer::init())
		return false;
	setPosition(Vec2::ZERO);

	auto layerNode = CSLoader::createNode(PATH_GAMEUI_LEVELSCENE_CSB);
	layerNode->setUserData(this);
	addChild(layerNode, 0, KEY_SCENE_UI_LEVEL);
	
	auto containPanel = layerNode->getChildByName(KEY_GAMEUI_PANEL_CONTAIN);

	// Init Top Panel
	m_pTopPanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_TOP);

	m_pTitleText = static_cast<ui::Text*>(m_pTopPanel->getChildByName(KEY_GAMEUI_TITLE_TEXT));
	m_pTitleText->setText("LEVEL SCENE");

	auto userPanel = m_pTopPanel->getChildByName(KEY_GAMEUI_PANEL_USER);
	m_pAvatar = static_cast<Sprite*>(userPanel->getChildByName(KEY_GAMEUI_USER_AVATAR));
	m_pNameText = static_cast<ui::Text*>(userPanel->getChildByName(KEY_GAMEUI_USER_NAME_PANEL)->getChildByName(KEY_GAMEUI_USER_NAME_TEXT));
	m_pNameText->setText("Caption ZERO");
	m_pLevelText = static_cast<ui::Text*>(userPanel->getChildByName(KEY_GAMEUI_USER_LEVEL_PANEL)->getChildByName(KEY_GAMEUI_USER_LEVEL_TEXT));
	m_pLevelText->setText("Lv. 40");

	// Init Bottom Panel
	m_pBottomPanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_BOTTOM);

	m_pShopButton = static_cast<ui::Button*>(m_pBottomPanel->getChildByName(KEY_GAMEUI_BUTTON_SHOP));
	m_pShopButton->addClickEventListener([this](Ref* sender) {
		auto scene = ShopScene::createScene();
		auto trans = TransitionFadeDown::create(0.5f, scene);
		Director::getInstance()->pushScene(trans);
	});

	m_pUpgradeButton = static_cast<ui::Button*>(m_pBottomPanel->getChildByName(KEY_GAMEUI_BUTTON_UPGRADE));
	m_pUpgradeButton->addClickEventListener([this](Ref* sender) {
		auto scene = UpgradeScene::createScene();
		auto trans = TransitionFadeDown::create(0.5f, scene);
		Director::getInstance()->pushScene(trans);
	});
	
	m_pSocialButton = static_cast<ui::Button*>(m_pBottomPanel->getChildByName(KEY_GAMEUI_BUTTON_SOCIAL));
	m_pSocialButton->addClickEventListener([this](Ref* sender) {
		auto socialPU = PopUpManager::GetInstance(this)->getPopUp(PU_SOCIAL);
		PopUpManager::GetInstance()->show(socialPU);
	});

	// Middle Panel
	m_pMiddlePanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_MIDDLE);
	auto posMiddlePanel = m_pMiddlePanel->getPosition();

	for (int i = 0; i < MAX_GAME_LEVEL; i++)
	{
		auto item = LevelItem::create(this);
		item->setIndex(i + 1);
		if (i < UserProfile::Get()->m_pCurrentMission * MAX_GAME_LEVEL + UserProfile::Get()->m_pCurrntLevel)
			item->setEnable(UserProfile::Get()->m_pLevelStars[i]);
		else
			item->setDisable();
		auto posItem = m_pMiddlePanel->getChildByName(KEY_GAMEUI_NODE_LEVEL(i))->getPosition();
		item->setPosition(posMiddlePanel + posItem);
	}
	PopUpManager::GetInstance(this);
	return true;
}

void LevelScene::initLevels()
{
	cocos2d::Vector<MenuItem*> listItems;

	for (int i = 0; i < 4; i++)
	{
		auto item = Utils::addButton(SPRITE_LEVELSCENE_ISLAND_ENA, SPRITE_LEVELSCENE_ISLAND_DIS, SPRITE_LEVELSCENE_LOCK, [this](Ref* ref){
			auto obj = (MenuItem*)ref;
			auto scene = GameScene::createScene(2);
			auto trans = TransitionFadeDown::create(0.5f, scene);
			Director::getInstance()->pushScene(trans);
		});

		item->setTag(i + 1);

		if (i >= 2)
		{
			item->setEnabled(false);
		}

		auto sizeItem = item->getContentSize();
		item->setPositionX(sizeItem.width * i + 200 + i * 100);
		item->setPositionY(VisibleRect::center().y);

		listItems.pushBack(item);
	}

	auto menu = Menu::createWithArray(listItems);
	menu->setPosition(Vec2::ZERO);
	addChild(menu);
}

LevelScene * LevelScene::create()
{
	auto scene = new LevelScene();
	if (scene->init())
	{
		scene->autorelease();
		return scene;
	}
	CC_SAFE_DELETE(scene);

	return NULL;
}