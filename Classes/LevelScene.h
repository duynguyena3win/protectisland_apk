#ifndef __LEVELSCENE_H__
#define __LEVELSCENE_H__

#include	"Config.h"
#include	"cocostudio\CocoStudio.h"
#include	"ui\UIText.h"
#include	"ui\UIButton.h"
#include	"LevelItem.h"

#define SPRITE_LEVELSCENE_ISLAND_ENA	"Icons/IconIsland_Ena.png"
#define SPRITE_LEVELSCENE_ISLAND_DIS	"Icons/IconIsland_Dis.png"
#define SPRITE_LEVELSCENE_LOCK		"Icons/IconLock.png"

class LevelScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	static LevelScene* create();
protected:
	void initLevels();
	
	Node*		m_pTopPanel;
	Node*		m_pMiddlePanel;
	Node*		m_pBottomPanel;

	// Top Panel
	ui::Text*	m_pTitleText;
	ui::Text*	m_pNameText;
	ui::Text*	m_pLevelText;
	Sprite*		m_pAvatar;

	// Middle Panel
	vector<LevelItem*> m_pListLevels;

	// Bottom Panel
	ui::Button*	m_pShopButton;
	ui::Button*	m_pUpgradeButton;
	ui::Button*	m_pSocialButton;
	Node*		m_pEnegryBar;
};

#endif // !__LEVELSCENE_H__
