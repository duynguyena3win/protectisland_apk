#include "LogoScene.h"
#include "VisibleRect.h"
#include "GameScene.h"
#include "PopUpManager.h"
#include "BattleShip.h"

cocos2d::Scene* LogoScene::createScene()
{
	auto sence = Scene::create();
	auto layer = LogoScene::create();
	sence->addChild(layer);

	
	return sence;
}

bool LogoScene::init()
{
	if (!Layer::init())
		return false;

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto sp = Sprite::create(SPRITE_LOGOSCENE_BACKGROUND);
	addChild(sp);
	sp->setPosition(Point(visibleSize / 2));
	initKeyboardDebug();
	// Test new item:
	m_pTestItem = BattleShip::create(this);
	m_pTestItem->setPosition(VisibleRect::center());
	m_pTestItem->setScale(1.2);
	m_pTestItem->setRotation(45);
	m_pTestItem->setBarrelRotation(25, 0);
	auto pos = m_pTestItem->getPositionBarrel();
	auto degree = m_pTestItem->getBarrelRotation(0);
	PopUpManager::GetInstance(this);
	return true;
}

LogoScene * LogoScene::create()
{
	auto scene = new LogoScene();
	if (scene->init())
	{
		scene->autorelease();
		return scene;
	}
	CC_SAFE_DELETE(scene);

	return NULL;
}

void	LogoScene::update(float deltaTime)
{

}

void	LogoScene::initKeyboardDebug()
{
	auto eventListener = EventListenerKeyboard::create();

	eventListener->onKeyPressed = CC_CALLBACK_2(LogoScene::OnKeyboard, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(eventListener, this);
}

void	LogoScene::OnKeyboard(EventKeyboard::KeyCode keyCode, Event* event)
{
	Vec2 loc = event->getCurrentTarget()->getPosition();
	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
		m_pTestItem->setBarrelRotation(m_pTestItem->getBarrelRotation(0) - 1.0f, 0);
		break;
	case EventKeyboard::KeyCode::KEY_A:
		m_pTestItem->setRotation(m_pTestItem->getRotation() - 1.0f);
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
		m_pTestItem->setBarrelRotation(m_pTestItem->getBarrelRotation(0) + 1.0f, 0);
		break;
	case EventKeyboard::KeyCode::KEY_D:
		m_pTestItem->setRotation(m_pTestItem->getRotation() + 1.0f);
		break;
	case EventKeyboard::KeyCode::KEY_UP_ARROW:
	case EventKeyboard::KeyCode::KEY_W:
		break;
	case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
	case EventKeyboard::KeyCode::KEY_S:
		break;
	}
}