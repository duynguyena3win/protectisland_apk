#ifndef __LOGOSCENE_H__
#define __LOGOSCENE_H__

#include "Config.h"

class BattleShip;

class LogoScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	static LogoScene* create();

	void update(float) override;

	void	initKeyboardDebug();

	void	OnKeyboard(EventKeyboard::KeyCode keyCode, Event* event);

protected:
	BattleShip*		m_pTestItem;
};

#endif // !__LOGOSCENE_H__
