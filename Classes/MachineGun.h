#ifndef __MACHINEGUN_H__
#define __MACHINEGUN_H__

#include "GameEntity.h"

class MachineGun : public GameEntity
{
public:
	static MachineGun* create(Layer* layer);
	virtual bool init();
	void	setRotate(Vec2 point);
	Action* createAction(Vec2 target = Vec2(0,0));
	void	setTarget(GameEntity* target) override;
	virtual void attack(bool isAttack);
	void	setState(int state);
	bool	takeDamage(float damage, int typeDamge);
	bool	isHaveTarget();

	virtual void shooting();
	virtual void destroy();
	virtual void finish();
};

#endif