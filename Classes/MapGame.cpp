#include "MapGame.h"
#include "VisibleRect.h"
#include "EntityFactory.h"
#include "HudGame.h"
#include "Utils.h"
#include "BarItems.h"
#include "PopUpManager.h"
#include "EffectFactory.h"
#include "FreemiumBar.h"

MapGame* MapGame::create(GameScene* gamelayer)
{
	auto rk = new MapGame();
	rk->m_pGameLayer = gamelayer;
	rk->m_pZOrder = LLG_MAP;
	if (rk->init())
	{
		return rk;
	}
	CC_SAFE_DELETE(rk);
	return NULL;
}

void MapGame::update(float delta)
{
	for (auto wea : m_pWeapons)
	{
		if (wea && wea->isActive())
		{
			bool isShot = false;
			for (auto ene : m_pEnemies)
			{
				if (ene->isActive() && wea->isInsideArea(ene))
				{
					isShot = true;
					if (!wea->isHaveTarget())
					{
						wea->setTarget(ene);
					}
					wea->attack(true);
					break;
				}
			}

			if (!isShot)
			{
				wea->attack(false);
			}
		}
	}

	checkEnemyWithWeapons();
}

void MapGame::checkEnemyWithWeapons()
{
	vector<Vec2> listEnemies;
	for (auto ene : m_pEnemies)
	{
		if (ene && ene->isActive())
		{
			listEnemies.push_back(ene->getPosition());
			bool isShot = false;
			for (auto wea : m_pWeapons)
			{
				if (wea->isActive() && ene->isInsideArea(wea))
				{
					isShot = true;
					if (!ene->isHaveTarget())
					{
						ene->setTarget(wea);
					}
					ene->attack(true);
					break;
				}
			}
			if (!isShot)
			{
				ene->attack(false);
			}
		}
	}
	//m_pGameLayer->makeRadarCheck(VisibleRect::center(), listEnemies);
}

bool MapGame::init()
{
	if (!Layer::init())
	{
		return false;
	}
	LOGI("Init Map Start!");
	m_bFinishLoading = false;
	initBarItems();

	//adds contact event listener
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(MapGame::onContactBegin, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);
	LOGI("Init Map Finish!");
	return true;
}

void MapGame::openBarItems(Vec2 posAppear, int tag)
{
	BarItems::getInstance()->setPosition(posAppear);
	BarItems::getInstance()->setTypeMenu(tag);
	BarItems::getInstance()->loadButtonMenu();
	BarItems::getInstance()->openMenu();
}

bool MapGame::initBarItems()
{
	BarItems::getInstance(this);
	BarItems::getInstance()->setDelegate(this);
	return true;
}

bool MapGame::onContactBegin(PhysicsContact& contact)
{
	auto nodeA = contact.getShapeA()->getBody();
	auto nodeB = contact.getShapeB()->getBody();
	
	if (nodeA && nodeB)
	{
		if ((nodeA->getCollisionBitmask() == COLLISION_BITMASK_ENEMY && nodeB->getCollisionBitmask() == COLLISION_BITMASK_ISLAND) ||
			(nodeA->getCollisionBitmask() == COLLISION_BITMASK_ISLAND && nodeB->getCollisionBitmask() == COLLISION_BITMASK_ENEMY))
		{
			if (nodeA->getCollisionBitmask() == COLLISION_BITMASK_ENEMY)
			{
				getEnemyFromTag(nodeA->getNode()->getTag())->finish();
			}
			if (nodeB->getCollisionBitmask() == COLLISION_BITMASK_ENEMY)
			{
				getEnemyFromTag(nodeB->getNode()->getTag())->finish();
			}
		}
		else
		{
			return false;
		}
		return true;
	}
	return false;
}

GameEntity* MapGame::getEnemyFromTag(int tag)
{
	for (auto ene : m_pEnemies)
	{
		if (ene->getTag() == tag)
			return ene;
	}
	return NULL;
}

GameEntity* MapGame::getWeaponsFromTag(int tag)
{
	for (auto wea : m_pWeapons)
	{
		if (wea->getTag() == tag)
			return wea;
	}
	return NULL;
}

void MapGame::pauseGame()
{
	
	for (auto n : this->getChildren())
	{
		if (n->getTag() == TAG_DEFAULT)
		{
			n->pause();
		}
	}

	for (auto wea : m_pWeapons)
		wea->pause();

	for (auto ene : m_pEnemies)
		ene->pause();

	this->pause();
}

void MapGame::resumeGame()
{
	this->resume();

	for (auto wea : m_pWeapons)
		wea->resume();

	for (auto ene : m_pEnemies)
		ene->resume();

	for (auto n : this->getChildren())
	{
		if (n->getTag() == TAG_DEFAULT)
		{
			n->resume();
		}
	}
}

bool MapGame::onTouchBegin(Touch *touch, Event *event, int ind)
{
	p_OldTouch = touch->getLocation();
	auto position = touch->getLocation();

	if (BarItems::getInstance()->getState())
		BarItems::getInstance()->closeMenu();

	return true;
}

void MapGame::onTouchMove(Touch *touch, Event *event)
{
	//auto position = touch->getLocation();
	//auto moveDirec = position - p_OldTouch;
	//auto newPosMap = getPosition() + moveDirec;
	//auto rectView = Rect(0, 0, DESIGN_RESOLUTION_WIDTH, DESIGN_RESOLUTION_HEIGHT - FREEMIUM_BAR->getHeight());
	////
	//auto point1 = getPosition();
	//auto point2 = getPosition() + getSize();
	//auto caseX_Min = point1.x <= rectView.getMinX();
	//auto caseY_Min = point1.y <= rectView.getMinY();

	//auto caseX_Max = point1.x >= rectView.getMaxX();
	//auto caseY_Max = point1.y >= rectView.getMaxY();
	//auto b1 = point1.x > 0 || point1.y > 0;
	//auto b2 = point2.x < rectView.size.width || point2.y < rectView.size.height;

	//if (!caseX_Min)
	//	newPosMap.x = 0;
	//else if (!caseX_Max)
	//	newPosMap.x = rectView.getMaxX();

	//if (!caseY_Min)
	//	newPosMap.y = 0;
	//else if (!caseY_Max)
	//	newPosMap.y = rectView.getMaxY();

	//if (b1 || b2)
	//{
	//	//newPosMap = m_pMapGame->getPosition();
	//	//log("Go out view! ");
	//	//m_pMapGame->setPosition(Vec2(0, 0));
	//}
	//else
	//{
	//	runAction(MoveTo::create(0.3f, newPosMap));
	//	p_OldTouch = position;
	//}
}

void MapGame::onTouchEnd(Touch *touch, Event *event)
{

}

void MapGame::buildCallback(int id)
{
	GameEntity* item = 0;
	switch (id)
	{
	case ID_MACHINEGUN:
	{
						  if (!Utils::checkBuyItem(Utils::getValueById(ID_MACHINEGUN, true)))
						  {
							  m_pGameLayer->showNotice("Information", "You not enough cash!");
							  return;
						  }
						  item = EntityFactory::getInstance()->getObject(ID_MACHINEGUN);
						  item->setState(ACTIVE_ENTITY);
						  item->setPosition(m_pPositionBuild);
						  if (m_pWeapons.find(item) == m_pWeapons.end())
						  {
							  m_pWeapons.insert(item);
						  }
	}
		break;
	case ID_FOGGUN:
	{
					  if (!Utils::checkBuyItem(Utils::getValueById(ID_FOGGUN, true)))
					  {
						  m_pGameLayer->showNotice("Information", "You not enough cash!");
						  return;
					  }
					  item = EntityFactory::getInstance()->getObject(ID_FOGGUN);
					  item->setState(ACTIVE_ENTITY);
					  item->setPosition(m_pPositionBuild);
					  if (m_pWeapons.find(item) == m_pWeapons.end())
					  {
						  m_pWeapons.insert(item);
					  }
	}
		break;
	case ID_CANON37MM:
	{
						 if (!Utils::checkBuyItem(Utils::getValueById(ID_CANON37MM, true)))
						 {
							 m_pGameLayer->showNotice("Information", "You not enough cash!");
							 return;
						 }
						 item = EntityFactory::getInstance()->getObject(ID_CANON37MM);
						 item->setState(ACTIVE_ENTITY);
						 item->setPosition(m_pPositionBuild, m_pSizeBuild);
						 if (m_pWeapons.find(item) == m_pWeapons.end())
						 {
							 m_pWeapons.insert(item);
						 }
	}
		break;
	case ID_CANON203MM:
	{
						  if (!Utils::checkBuyItem(Utils::getValueById(ID_CANON203MM, true)))
						  {
							  m_pGameLayer->showNotice("Information", "You not enough cash!");
							  return;
						  }

						  item = EntityFactory::getInstance()->getObject(ID_CANON203MM);
						  item->setState(ACTIVE_ENTITY);
						  item->setPosition(m_pPositionBuild, m_pSizeBuild);
						  if (m_pWeapons.find(item) == m_pWeapons.end())
						  {
							  m_pWeapons.insert(item);
						  }
	}
	break;

	}
	if (item)
	{
		m_pCurrentButton->setUserData((Node*)item);
		m_pCurrentButton = 0;
	}
	BarItems::getInstance()->closeMenu();
}

void MapGame::startGame()
{
	float timer = 0;

	for (int i = 0; i < m_pGameLayer->m_pCurrentLevelInfo->m_pIdEnemies.size(); i++)
	{
		timer += m_pGameLayer->m_pCurrentLevelInfo->m_pTimes.at(i);

		this->scheduleOnce([this, i](float dt) {
			auto ene = EntityFactory::getInstance()->getObject(m_pGameLayer->m_pCurrentLevelInfo->m_pIdEnemies.at(i));
			ene->setDelegate(m_pGameLayer);
			ene->setPosition(m_pGameLayer->m_pCurrentLevelInfo->m_pMovingLines.at(i).at(0));
			ene->setTargetPoint(this->m_pIslandPosition);
			ene->setPathMoving(m_pGameLayer->m_pCurrentLevelInfo->m_pMovingLines.at(i));
			ene->createActionMoving();
			ene->createBarInfoMoving();
			ene->runMovingAction();
			if (m_pEnemies.find(ene) == m_pEnemies.end())
			{
				this->m_pEnemies.insert(ene);
			}
		}, timer, StringUtils::format("keyz%02d", i));
	}

}

void  MapGame::targetItemCallback(int idButton, GameEntity* target)
{
	switch (idButton)
	{
	case BTG_SELL:
	{
		m_pTargetItem = target;
		std::map<string, string> infors;
		infors.insert(std::pair<string, string>(KEY_GAMEUI_TITLE_TEXT, "Sell Item"));
		string text = "Do you want sell " + target->m_pStateItem->m_sName;
		text += "\n\t with cost " + StringUtils::toString(target->m_pStateItem->m_pValue) + "?";
		infors.insert(std::pair<string, string>(KEY_GAMEUI_BODY_TEXT, text));

		auto popup = PopUpManager::GetInstance(this)->getPopUp(PU_TRADE, infors);
		PopUpManager::GetInstance()->show(popup);
	}
		break;
	case BTG_UPGRADE:
		break;
	}
}

void MapGame::sellTargetItem(int &earnCash)
{
	if (m_pTargetItem)
	{
		earnCash += m_pTargetItem->m_pStateItem->m_pValue;
		auto cash = EffectFactory::getInstance()->getObject(ID_CASH);
		cash->setText(m_pTargetItem->m_pStateItem->m_pValue);
		cash->setPosition(m_pTargetItem->getPosition());
		m_pTargetItem->finish();
		m_pTargetItem = 0;
		m_pCurrentButton->setUserData(0);
	}
}

void MapGame::callSpecialBoom()
{
	for (auto ene : m_pEnemies)
	{
		if (ene->isActive())
			ene->takeDamage(100, ID_CANONBULLET);
	}

	auto plane1 = this->getChildByName("SPECIAL_ITEM_PLANE_1");
	if (!plane1)
	{
		plane1 = Sprite::create(SPRITE_ICON_WEAPON_JETPLANE);
		this->addChild(plane1, 5, "SPECIAL_ITEM_PLANE_1");
	}
	plane1->unscheduleAllCallbacks();
	plane1->stopAllActions();

	auto plane2 = this->getChildByName("SPECIAL_ITEM_PLANE_2");
	if (!plane2)
	{
		plane2 = Sprite::create(SPRITE_ICON_WEAPON_JETPLANE);
		this->addChild(plane2, 5, "SPECIAL_ITEM_PLANE_2");
	}
	plane2->stopAllActions();

	auto plane3 = this->getChildByName("SPECIAL_ITEM_PLANE_3");
	if (!plane3)
	{
		plane3 = Sprite::create(SPRITE_ICON_WEAPON_JETPLANE);
		this->addChild(plane3, 5, "SPECIAL_ITEM_PLANE_3");
	}
	plane3->stopAllActions();

	auto plane4 = this->getChildByName("SPECIAL_ITEM_PLANE_4");
	if (!plane4)
	{
		plane4 = Sprite::create(SPRITE_ICON_WEAPON_JETPLANE);
		this->addChild(plane4, 5, "SPECIAL_ITEM_PLANE_4");
	}
	plane4->stopAllActions();

	auto plane5 = this->getChildByName("SPECIAL_ITEM_PLANE_5");
	if (!plane5)
	{
		plane5 = Sprite::create(SPRITE_ICON_WEAPON_JETPLANE);
		this->addChild(plane5, 5, "SPECIAL_ITEM_PLANE_5");
	}
	plane5->stopAllActions();

	auto sizeMap = getSize();
	plane1->setPosition(Vec2(-30, sizeMap.y / 6));
	
	plane2->setPosition(Vec2(-160, sizeMap.y / 3));
	plane3->setPosition(Vec2(-30, sizeMap.y / 2));
	plane4->setPosition(Vec2(-160, 2 * sizeMap.y / 3));
	plane5->setPosition(Vec2(-30, 5 * sizeMap.y / 6));

	plane1->schedule([plane1, plane2, plane3, plane4, plane5](float dt){
		auto effectItem1 = EffectFactory::getInstance()->getObject(ID_CANONBULLET);
		effectItem1->setPosition(plane1->getPosition() - Vec2(100, 0));

		auto effectItem2 = EffectFactory::getInstance()->getObject(ID_CANONBULLET);
		effectItem2->setPosition(plane2->getPosition() - Vec2(100, 0));

		auto effectItem3 = EffectFactory::getInstance()->getObject(ID_CANONBULLET);
		effectItem3->setPosition(plane3->getPosition() - Vec2(100, 0));

		auto effectItem4 = EffectFactory::getInstance()->getObject(ID_CANONBULLET);
		effectItem4->setPosition(plane4->getPosition() - Vec2(100, 0));

		auto effectItem5 = EffectFactory::getInstance()->getObject(ID_CANONBULLET);
		effectItem5->setPosition(plane5->getPosition() - Vec2(100, 0));
	}, 0.5f, StringUtils::format("%dasdjkh%d", RandomHelper::random_int(0, 1234), RandomHelper::random_int(0, 1234)));

	auto action1 = MoveTo::create(5.0f, Vec2(sizeMap.x + 50, sizeMap.y / 6));
	auto func1 = CallFunc::create([plane1](){
		plane1->unscheduleAllCallbacks();
	});
	
	auto action2 = MoveTo::create(5.0f, Vec2(sizeMap.x + 50, sizeMap.y / 3));
	auto action3 = MoveTo::create(5.0f, Vec2(sizeMap.x + 50, sizeMap.y / 2));
	auto action4 = MoveTo::create(5.0f, Vec2(sizeMap.x + 50, 2 * sizeMap.y / 3));
	auto action5 = MoveTo::create(5.0f, Vec2(sizeMap.x + 50, 5 * sizeMap.y / 6));
	
	plane1->runAction(Sequence::create(action1, func1, NULL));
	plane2->runAction(action2);
	plane3->runAction(action3);
	plane4->runAction(action4);
	plane5->runAction(action5);
}

void MapGame::callSpecialHeathHelp()
{
	for (auto wea : m_pWeapons)
	{
		auto effectItem = EffectFactory::getInstance()->getObject(ID_HEART);
		effectItem->setPosition(wea->getPosition());
	}
}

bool MapGame::LoadInforMapGame(string pathFile)
{
	// Load Map TMX
	m_pMap = TMXTiledMap::create(pathFile);
	if (!m_pMap)
		return false;
	m_pMap->setTag(TAG_DEFAULT);
	addChild(m_pMap, LLG_MAP);

	// Load Center of Island
	m_pIslandPosition.x = m_pMap->getProperties().at(KEY_CENTER_X).asInt() * m_pMap->getTileSize().width;
	m_pIslandPosition.y = m_pMap->getProperties().at(KEY_CENTER_Y).asInt() * m_pMap->getTileSize().height;
	
	// Load Border Of Island
	auto objBorderCollision = m_pMap->getObjectGroup(KEY_OBJECT_COLLISSION);
	auto objLineGroups = objBorderCollision->getObjects();
	
	for (auto& objChild : objLineGroups)
	{
		PhysicsBody* bodyPhysic;
		Vec2	posStartPoint = Vec2::ZERO;

		auto properties = objChild.asValueMap();
		posStartPoint.x = properties.at("x").asFloat();
		posStartPoint.y = properties.at("y").asFloat();
		auto points = properties.at("points").asValueVector();
		
		Vec2* list = Utils::convertPathMoving(points, posStartPoint);

		bodyPhysic = PhysicsBody::createEdgePolygon(list, points.size(), PHYSICSBODY_MATERIAL_DEFAULT, 1);
		bodyPhysic->setCollisionBitmask(COLLISION_BITMASK_ISLAND);
		bodyPhysic->setContactTestBitmask(true);
		CC_SAFE_DELETE_ARRAY(list);

		auto bodyNodePhysic = Node::create();
		bodyNodePhysic->setPhysicsBody(bodyPhysic);
		bodyNodePhysic->setTag(TAG_DEFAULT);
		addChild(bodyNodePhysic, LLG_ENTITY_GROUND);
	}
	//
	
	return true;
}

bool MapGame::LoadInforLevelMap()
{
	if (!m_pMap)
		return false;

	// Load Infor Level
	auto propertiesLevel = m_pMap->getProperties();

	m_pGameLayer->m_pCurrentLevelInfo = new LevelInfo();
	m_pGameLayer->m_pCurrentLevelInfo->m_pEXP = propertiesLevel.at(KEY_EXP).asInt();
	m_pGameLayer->m_pCurrentLevelInfo->m_pGemBonus = propertiesLevel.at(KEY_EXP).asInt();
	m_pGameLayer->m_pCurrentLevelInfo->m_pName = propertiesLevel.at(KEY_NAME_ISLAND).asString();

	// Load Enemies
	auto objEnemies = m_pMap->getObjectGroup(KEY_OBJECT_ENEMIES);
	auto enemies = objEnemies->getObjects();
	for (auto& enemy : enemies)
	{
		auto propertiesEnemy = enemy.asValueMap();
		auto typeEnemy = propertiesEnemy.at(KEY_ENEMY_TYPE).asInt();
		m_pGameLayer->m_pCurrentLevelInfo->m_pIdEnemies.push_back(typeEnemy);
		m_pGameLayer->m_pCurrentLevelInfo->m_pTimes.push_back(propertiesEnemy.at(KEY_ENEMY_TIMESTART).asFloat());

		// Get Line Moving Of Enemy
		Vec2 enyStartPoint;
		enyStartPoint.x = propertiesEnemy.at("x").asFloat();
		enyStartPoint.y = propertiesEnemy.at("y").asFloat();

		cocos2d::ValueVector points;
		points = propertiesEnemy.at("polylinePoints").asValueVector();

		Vec2* list = Utils::convertPathMoving(points, enyStartPoint);
		vector<Vec2> linesEnemy;
		for (int i = 0; i < points.size(); i++)
			linesEnemy.push_back(list[i]);
		m_pGameLayer->m_pCurrentLevelInfo->m_pMovingLines.push_back(linesEnemy);

		CC_SAFE_DELETE_ARRAY(list);
	}
}

bool MapGame::LoadInforBuildArea()
{
	if (!m_pMap)
		return false;

	// Get Build Information
	auto objBuildArea = m_pMap->getObjectGroup(KEY_OBJECT_BUILD);
	auto objBuilds = objBuildArea->getObjects();
	float x, y;
	for (auto& object : objBuilds)
	{
		auto btnBuild = Utils::addButton(SPRITE_BUTTON_BUILD_ENABLE, SPRITE_BUTTON_BUILD_DISABLE, SPRITE_BUTTON_BUILD_DISABLE, [this](Ref* ref) {
			MenuItem* btn = (MenuItem*)ref;
			m_pCurrentButton = btn;
			m_pPositionBuild = m_pMap->getPosition() + btn->getPosition();
			m_pSizeBuild = Utils::getSizeFromTag(btn->getTag());

			GameEntity* target = (GameEntity*)m_pCurrentButton->getUserData();
			if (target && target->isActive())
			{
				BarItems::getInstance()->setTargetItem(target);
				openBarItems(m_pPositionBuild, MENU_ITEMS::MI_ITEM);
			}
			else
				openBarItems(m_pPositionBuild, btn->getTag());
		});

		auto properties = object.asValueMap();
		x = properties.at("x").asFloat();
		y = properties.at("y").asFloat();
		auto width = properties.at("width").asFloat();
		auto height = properties.at("height").asFloat();

		btnBuild->setTag(Utils::getTagItem(width, height));

		auto posX = x + width / 2;
		auto posY = y + height / 2;
		btnBuild->setPosition(posX, posY);
		m_pButtonsBuild.pushBack(btnBuild);
	}

	auto menu = Menu::createWithArray(m_pButtonsBuild);
	menu->setPosition(Vec2::ZERO);
	menu->setTag(TAG_DEFAULT);
	addChild(menu, 1, LLG_HUDITEM);

	return true;
}

void MapGame::InitGame(string pathMap)
{
	LOGI("Init MapGame 1!");
	loadWaterPlane();
	if (LoadInforMapGame(pathMap))
	{
		LoadInforLevelMap();
		LoadInforBuildArea();
		m_bFinishLoading = true;
	}
	LOGI("Init MapGame 2!");
}

MapGame::~MapGame()
{
	CC_SAFE_DELETE(m_pMap);
	CC_SAFE_DELETE(m_pGameLayer->m_pCurrentLevelInfo);
	m_pTargetItem = 0;
	m_pCurrentButton = 0;
	m_pGameLayer = 0;
	this->autorelease();
}

void MapGame::loadWaterPlane()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto sp = Sprite::create(SPRITE_LOGOSCENE_BACKGROUND, Rect(0, 0, visibleSize.width, visibleSize.height));
	addChild(sp, 0);
	sp->setPosition(Point(visibleSize / 2));

	auto TexCache = Director::getInstance()->getTextureCache();
	auto wave2 = TexCache->addImage("wave1.png");
	auto wave1 = TexCache->addImage("18.png");
	wave1->setTexParameters(Texture2D::TexParams{ GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT });
	wave2->setTexParameters(Texture2D::TexParams{ GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT });
	auto glprogram = GLProgram::createWithFilenames("water.vsh", "water.fsh");
	auto glprogramstate = GLProgramState::getOrCreateWithGLProgram(glprogram);
	sp->setGLProgramState(glprogramstate);

	glprogramstate->setUniformTexture("u_wave1", wave1);
	glprogramstate->setUniformTexture("u_wave2", wave2);
	glprogramstate->setUniformFloat("saturateValue", 1.2);
}
