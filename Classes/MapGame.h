#ifndef __MAPGAME_H__
#define __MAPGAME_H__

#include "Config.h"
#include "GameScene.h"

class GameEntity;

#define		KEY_OBJECT_COLLISSION	"Collision"
#define		KEY_OBJECT_BUILD		"Build"
#define		KEY_OBJECT_ENEMIES		"ListEnemies"

#define		KEY_CENTER_X			"TileX"
#define		KEY_CENTER_Y			"TileY"
#define		KEY_NAME_ISLAND			"Name"
#define		KEY_EXP					"EXP"
#define		KEY_GEM_BONUS			"GemBonus"

#define		KEY_ENEMY_TYPE			"type"
#define		KEY_ENEMY_TIMESTART		"TimeStart"

class MapGame : public cocos2d::Layer
{
public:
	static MapGame* create(GameScene* gamelayer);
	bool init();
	TMXTiledMap* getMap() { return m_pMap; }
	//void runAction(Action* action);
	int getWidth() { return m_pMap->getMapSize().width * m_pMap->getTileSize().width; }
	int getHeight() { return m_pMap->getMapSize().height * m_pMap->getTileSize().height; }
	Vec2 getSize() { return Vec2(getWidth(), getHeight()); }
	int getWidthTiles() { return m_pMap->getMapSize().width; }
	int getHeightTiles() { return m_pMap->getMapSize().height; }
	Vec2 getIslandPosition() { return m_pIslandPosition; }

	void update(float delta);
	void checkEnemyWithWeapons();

	// Event With Touch
	bool onTouchBegin(Touch *touch, Event *event, int ind);
	void onTouchMove(Touch *touch, Event *event);
	void onTouchEnd(Touch *touch, Event *event);
	//

	void startGame();
	bool isFinishLoading() { return m_bFinishLoading; }
	void pauseGame();
	void resumeGame();
	void buildCallback(int id);
	void targetItemCallback(int idButton, GameEntity* target);
	std::set<GameEntity*> getEnemies() { return m_pEnemies; }

	// Bar Items
	bool initBarItems();
	void openBarItems(Vec2 posAppear, int tag);
	void sellTargetItem(int &earnCash);
	
	// Function call Special Item
	void callSpecialBoom();
	void callSpecialHeathHelp();
	//

	// New function for Load Map
	void InitGame(string pathMap);
	bool LoadInforMapGame(string pathFile);
	bool LoadInforLevelMap();
	bool LoadInforBuildArea();
	~MapGame();
protected:
	bool onContactBegin(PhysicsContact& contact);
	GameEntity* getEnemyFromTag(int tag);
	GameEntity* getWeaponsFromTag(int tag);
	void	loadWaterPlane();

protected:
	TMXTiledMap* m_pMap;
	Layer* m_pLayer;
	GameScene* m_pGameLayer;
	int m_pZOrder;
	Vec2 p_OldTouch;
	cocos2d::Vector<MenuItem*> m_pButtonsBuild;
	
	std::set<GameEntity*> m_pWeapons;
	std::set<GameEntity*> m_pEnemies;
	bool m_bFinishLoading;

	Vec2 m_pPositionBuild;
	Vec2 m_pIslandPosition;
	Size m_pSizeBuild;
	MenuItem* m_pCurrentButton;
	GameEntity* m_pTargetItem;
};

#endif