#include "MenuScene.h"
#include "VisibleRect.h"
#include "Utils.h"
#include "LevelScene.h"
#include "ShopScene.h"
#include "PopUpManager.h"

#include "cocostudio\CocoStudio.h"
#include "ui\UIButton.h"

#ifdef SDKBOX_ENABLED
#include "Sdkbox/Sdkbox.h"
#include "PluginFacebook/PluginFacebook.h"
#include "PluginIAP/PluginIAP.h"
#endif

cocos2d::Scene* MenuScene::createScene()
{
	LOGI("Create Menu Scene!");
	auto sence = Scene::create();
	auto layer = MenuScene::create();
	sence->addChild(layer);
	return sence;
}

bool MenuScene::init()
{
	LOGI("Start Init MenuScene okie!");
	if (!Layer::init())
		return false;
	
	auto layoutMenu = CSLoader::createNode(PATH_GAMEUI_MENUSCENE_CSB);
	addChild(layoutMenu, 1, KEY_SCENE_UI_MENU);
	layoutMenu->setPosition(Vec2::ZERO);

	auto timeLine = CSLoader::createTimeline(PATH_GAMEUI_MENUSCENE_CSB);
	timeLine->play("idle", true);
	layoutMenu->runAction(timeLine);


	auto panelContain = layoutMenu->getChildByName(KEY_GAMEUI_PANEL_CONTAIN);
	auto bottomContain = panelContain->getChildByName(KEY_GAMEUI_PANEL_BOTTOM);

	auto btnCampain = static_cast<ui::Button*>(bottomContain->getChildByName(KEY_GAMEUI_BUTTON_CAMPAIGN));
	btnCampain->addClickEventListener([this](Ref* ref){
		auto scene = LevelScene::createScene();
		auto trans = TransitionFadeDown::create(0.5f, scene);
		Director::getInstance()->pushScene(trans);
	});
	
	auto btnEvent = static_cast<ui::Button*>(bottomContain->getChildByName(KEY_GAMEUI_BUTTON_EVENTS));
	btnEvent->addClickEventListener([this](Ref* ref) {
		/* -> Implement */
	});
	
	auto btnShop = static_cast<ui::Button*>(bottomContain->getChildByName(KEY_GAMEUI_BUTTON_SHOP));
	btnShop->addClickEventListener([this](Ref* ref) {
		auto scene = ShopScene::createScene();
		auto trans = TransitionFadeDown::create(0.5f, scene);
		Director::getInstance()->pushScene(trans);
	});
	LOGI("End Init MenuScene okie!");
	PopUpManager::GetInstance(this);
	return true;
}

MenuScene * MenuScene::create()
{
	auto scene = new MenuScene();
	if (scene->init())
	{
		scene->autorelease();
		return scene;
	}
	CC_SAFE_DELETE(scene);

	return NULL;
}