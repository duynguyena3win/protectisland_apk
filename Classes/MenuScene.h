#ifndef __MENUSCENE_H__
#define __MENUSCENE_H__

#include "Config.h"

class MenuScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	static MenuScene* create();
};


#endif