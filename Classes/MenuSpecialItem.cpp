#include "MenuSpecialItem.h"
#include "HudGame.h"
#include "Utils.h"
#include "VisibleRect.h"

MenuSpecialItem* MenuSpecialItem::s_pInstance = 0;

MenuSpecialItem* MenuSpecialItem::getInstance(HudGame* layer)
{
	if (!s_pInstance && layer)
	{
		s_pInstance = new MenuSpecialItem();
		s_pInstance->m_pHudLayer = layer;
		s_pInstance->m_pZOrder = LLG_HUDITEM;

		if (!s_pInstance->init())
		{
			CC_SAFE_DELETE(s_pInstance);
			return NULL;
		}
	}
	return s_pInstance;
}

bool MenuSpecialItem::init()
{
	m_pNode = Node::create();
	m_pNode->setUserData(this);
	m_pNode->setPosition(VisibleRect::rightBottom() - Vec2(100, -60));
	m_pHudLayer->addChild(m_pNode, m_pZOrder);

	loadItems();
	return true;
}

void MenuSpecialItem::loadItems()
{
	cocos2d::Vector<MenuItem*> listButtons;
	
	auto btn_Bomber = Utils::addButton(SPRITE_BUTTON_SPECIAL_BOOMER_ENABLE, SPRITE_BUTTON_SPECIAL_BOOMER_DISABLE, SPRITE_BUTTON_SPECIAL_BOOMER_DISABLE, [this](Ref* ref)
	{
		m_pHudLayer->callSpecialItem(IS_BOOMER);
	});
	auto size = btn_Bomber->getContentSize();
	btn_Bomber->setPosition(Vec2::ZERO);
	btn_Bomber->setScale(0.7f);
	listButtons.pushBack(btn_Bomber);

	auto btn_Help = Utils::addButton(SPRITE_BUTTON_SPECIAL_HELP_ENABLE, SPRITE_BUTTON_SPECIAL_HELP_DISABLE, SPRITE_BUTTON_SPECIAL_HELP_DISABLE, [this](Ref* ref)
	{
		m_pHudLayer->callSpecialItem(IS_HELP);
	});
	btn_Help->setPosition(-Vec2(size.width * 0.8f, 0));
	btn_Help->setScale(0.7f);
	listButtons.pushBack(btn_Help);

	auto btn_Surrender = Utils::addButton(SPRITE_BUTTON_SPECIAL_SURRENDER_ENABLE, SPRITE_BUTTON_SPECIAL_SURRENDER_DISABLE, SPRITE_BUTTON_SPECIAL_SURRENDER_DISABLE, [this](Ref* ref)
	{

	});
	btn_Surrender->setPosition(-Vec2(2 * size.width * 0.8f, 0));
	btn_Surrender->setScale(0.7f);
	listButtons.pushBack(btn_Surrender);

	auto menu = Menu::createWithArray(listButtons);
	menu->setPosition(Vec2::ZERO); // VisibleRect::rightBottom() - Vec2(100, -100));
	m_pNode->addChild(menu);
}

void MenuSpecialItem::openMenu()
{
}

void MenuSpecialItem::closeMenu()
{
}