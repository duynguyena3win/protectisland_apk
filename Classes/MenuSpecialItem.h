#ifndef __MENUSPECIALITEM_H__
#define __MENUSPECIALITEM_H__

#include "Config.h"

// Button Special Boomer
#define SPRITE_BUTTON_SPECIAL_BOOMER_ENABLE		"ButtonIcons/SpecialBoomer.png"
#define SPRITE_BUTTON_SPECIAL_BOOMER_DISABLE	"ButtonIcons/SpecialBoomer_Dis.png"

// Button Special Help
#define SPRITE_BUTTON_SPECIAL_HELP_ENABLE		"ButtonIcons/SpecialHelp.png"
#define SPRITE_BUTTON_SPECIAL_HELP_DISABLE		"ButtonIcons/SpecialHelp_Dis.png"

// Button Special Boomer
#define SPRITE_BUTTON_SPECIAL_SURRENDER_ENABLE	"ButtonIcons/SpecialSurrender.png"
#define SPRITE_BUTTON_SPECIAL_SURRENDER_DISABLE	"ButtonIcons/SpecialSurrender_Dis.png"

class HudGame;
class MenuSpecialItem
{
public:
	static MenuSpecialItem* getInstance(HudGame* layer = 0);
	void openMenu();
	void closeMenu();

private:
	virtual bool init();
	void loadItems();

private:
	static MenuSpecialItem* s_pInstance;
	Node* m_pNode;
	HudGame* m_pHudLayer;
	int m_pZOrder;
};

#endif