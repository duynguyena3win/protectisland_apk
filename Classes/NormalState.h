#ifndef __NORMALSTATE_H__
#define __NORMALSTATE_H__

#include "Config.h"
#include "ItemState.h"

class NormalState : public ItemState
{
public:
	NormalState();
	virtual ~NormalState();
};

#endif