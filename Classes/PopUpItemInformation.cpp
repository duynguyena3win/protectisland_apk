#include "PopUpItemInformation.h"
#include "VisibleRect.h"
#include "PopUpManager.h"

PopUpItemInformation*	PopUpItemInformation::create(Layer* layer)
{
	auto pu = new PopUpItemInformation();
	pu->m_pZOrder = LLG_POPUP;
	pu->m_pLayer = layer;
	pu->m_sKey = KEY_POPUP_ITEMINFORMATION;
	if (pu->init())
	{
		return pu;
	}
	CC_SAFE_DELETE(pu);
	return NULL;
}

bool PopUpItemInformation::init()
{
	if (!BasePopUp::init())
		return false;

	auto nodePU = CSLoader::createNode(PATH_POPUPUI_ITEMINFORMATION);
	auto containPanel = nodePU->getChildByName(KEY_GAMEUI_PANEL_CONTAIN);

	auto topPanel	 = containPanel->getChildByName(KEY_GAMEUI_PANEL_TOP);
	auto middlePanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_MIDDLE);
	auto bottomPanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_BOTTOM);

	m_pTitleText = static_cast<ui::Text*>(topPanel->getChildByName(KEY_GAMEUI_TITLE_TEXT));
	m_pTitleText->setText("Notice");
	
	auto itemPanel  = middlePanel->getChildByName(KEY_GAMEUI_PANEL_ITEM);
	m_pLevelText = static_cast<ui::Text*>(itemPanel->getChildByName(KEY_GAMEUI_USER_LEVEL_PANEL)->getChildByName(KEY_GAMEUI_USER_LEVEL_TEXT));
	m_pLevelText->setText("Lv. 2");

	auto inforPanel = middlePanel->getChildByName(KEY_GAMEUI_PANEL_INFORMATION);
	m_pHeathText = static_cast<ui::Text*>(inforPanel->getChildByName(KEY_GAMEUI_PANEL_HEATH)->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pHeathText->setText("500");

	m_pDamageText = static_cast<ui::Text*>(inforPanel->getChildByName(KEY_GAMEUI_PANEL_DAMAGE)->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pDamageText->setText("45");

	m_pShieldText = static_cast<ui::Text*>(inforPanel->getChildByName(KEY_GAMEUI_PANEL_SHEILD)->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pShieldText->setText("10");

	m_pRangeText = static_cast<ui::Text*>(inforPanel->getChildByName(KEY_GAMEUI_PANEL_RANGE)->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pRangeText->setText("350");

	m_pReloadText = static_cast<ui::Text*>(inforPanel->getChildByName(KEY_GAMEUI_PANEL_RELOAD)->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pReloadText->setText("2.5");

	m_pSpeedBulletText = static_cast<ui::Text*>(inforPanel->getChildByName(KEY_GAMEUI_PANEL_SPEEDBULLET)->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pSpeedBulletText->setText("400");

	auto btnOK = static_cast<ui::Button*>(bottomPanel->getChildByName(KEY_BUTTON_OK));
	btnOK->addClickEventListener(CC_CALLBACK_1(PopUpItemInformation::OnEvent, this));
	
	m_pNode->addChild(nodePU);
	m_pNode->setPosition(VisibleRect::center());
	m_pType = PU_ITEMINFORMATION;
	return true;
}

void PopUpItemInformation::OnEvent(cocos2d::Ref *ref)
{
	auto btn = (ui::Button*) ref;
	string cbName = btn->getCallbackName();

	if (cbName.compare("onClickOK") == 0)
	{
		
	}
	PopUpManager::GetInstance()->close();
}

bool PopUpItemInformation::setData(std::map<string, string> inforPopUp /*= std::map<string, string>()*/)
{
	for (auto item : inforPopUp)
	{
		if (item.first.compare(KEY_GAMEUI_TITLE_TEXT) == 0)
		{
			m_pTitleText->setText(item.second);
		}
		else if (item.first.compare(KEY_GAMEUI_USER_LEVEL_TEXT) == 0)
		{
			m_pLevelText->setText(item.second);
		}
		else if (item.first.compare(KEY_GAMEUI_PANEL_HEATH) == 0)
		{
			m_pHeathText->setText(item.second);
		}
		else if (item.first.compare(KEY_GAMEUI_PANEL_DAMAGE) == 0)
		{
			m_pDamageText->setText(item.second);
		}
		else if (item.first.compare(KEY_GAMEUI_PANEL_SHEILD) == 0)
		{
			m_pShieldText->setText(item.second);
		}
		else if (item.first.compare(KEY_GAMEUI_PANEL_RANGE) == 0)
		{
			m_pRangeText->setText(item.second);
		}
		else if (item.first.compare(KEY_GAMEUI_PANEL_RELOAD) == 0)
		{
			m_pReloadText->setText(item.second);
		}
		else if (item.first.compare(KEY_GAMEUI_PANEL_SPEEDBULLET) == 0)
		{
			m_pSpeedBulletText->setText(item.second);
		}
	}
	return true;
}