#ifndef __POPUPITEMINFORMATION_H__
#define __POPUPITEMINFORMATION_H__

#include "Config.h"
#include "BasePopUp.h"

class PopUpItemInformation	: public	BasePopUp
{
public:
	static	PopUpItemInformation*	create(Layer* layer);

	bool	init() override;
	bool	setData(std::map<string, string> inforPopUp = std::map<string, string>()) override;
	void	OnEvent(cocos2d::Ref *ref) override;

	ui::Text*		m_pTitleText;
	ui::Text*		m_pLevelText;

	ui::Text*		m_pHeathText;
	ui::Text*		m_pDamageText;
	ui::Text*		m_pShieldText;
	ui::Text*		m_pRangeText;
	ui::Text*		m_pReloadText;
	ui::Text*		m_pSpeedBulletText;
};

#endif // !
