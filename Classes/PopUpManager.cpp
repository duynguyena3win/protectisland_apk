#include "PopUpManager.h"
#include "PopUpSocial.h"
#include "PopUpNotice.h"
#include "PopUpResult.h"
#include "PopUpResource.h"
#include "PopUpItemInformation.h"
#include "PopUpTrade.h"

PopUpManager* PopUpManager::s_pInstance = 0;

PopUpManager* PopUpManager::GetInstance(Layer* layer)
{
	if (!s_pInstance)
	{
		s_pInstance = new PopUpManager();
	}
	if (layer)
		s_pInstance->setLayer(layer);
	return s_pInstance;
}

PopUpManager::PopUpManager()
{
	m_bIsHavePopUp = false;
}

void	PopUpManager::pushPopUp(BasePopUp* newItem)
{
	m_pListPopups.push(newItem);
}
void	PopUpManager::popPopUp()
{
	m_pListPopups.pop();
}

bool	PopUpManager::isEmpty()
{
	return	m_pListPopups.empty();
}

BasePopUp*	PopUpManager::getTopPopUp()
{
	auto outPut = m_pListPopups.top();
	return outPut;
}

BasePopUp*	PopUpManager::getPopUp(int ind, std::map<string, string> inforPopUp)
{
	BasePopUp* outPut = 0;
	for (int i = 0; i < m_pPoolPopups.size(); i++)
	{
		if (m_pPoolPopups[i]->getType() == ind)
			outPut = m_pPoolPopups[i];
	}
	
	if (!outPut)
		outPut = createPopUp(ind);
	else
	{
		if (m_pLayer != outPut->getLayer())
			outPut->setNewLayer(m_pLayer);
	}

	if (!inforPopUp.empty())
		outPut->setData(inforPopUp);
	return outPut;
}

bool PopUpManager::show(BasePopUp* newPopUp)
{
	if (m_bIsHavePopUp)
	{
		auto topPopUp = m_pListPopups.top();
		topPopUp->closePopUp();
	}

	if (newPopUp)
	{
		m_pListPopups.push(newPopUp);
		newPopUp->showPopUp();
	}
	else
	{
		if(m_pListPopups.size() == 0)
			return false;

		auto topPopUp = m_pListPopups.top();
		topPopUp->showPopUp();
	}

	m_bIsHavePopUp = true;
	return m_bIsHavePopUp;
}

bool PopUpManager::close()
{
	if (m_pListPopups.size() == 0)
		return false;
	auto topPopUp = m_pListPopups.top();
	topPopUp->closePopUp();

	m_pListPopups.pop();

	m_bIsHavePopUp = false;
	if (m_pListPopups.size() > 0)
		show();
}

BasePopUp* PopUpManager::createPopUp(int ind)
{
	switch (ind)
	{
	case PU_SOCIAL:
		return PopUpSocial::create(m_pLayer);
	case PU_RESULT:
		return PopUpResult::create(m_pLayer);
	case PU_NOTICE:
		return PopUpNotice::create(m_pLayer);
	case PU_RESOURCE:
		return PopUpResource::create(m_pLayer);
	case PU_ITEMINFORMATION:
		return PopUpItemInformation::create(m_pLayer);
	case PU_TRADE:
		return PopUpTrade::create(m_pLayer);
	default:
		return 0;
	}
}

BasePopUp* PopUpManager::getExistPopUp(int index)
{
	for (int i = 0; i < m_pPoolPopups.size(); i++)
	{
		if (m_pPoolPopups[i]->getType() == index)
			return m_pPoolPopups[i];
	}
	return 0;
}
