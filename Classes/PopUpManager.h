#ifndef __POPUPMANAGER_H__
#define __POPUPMANAGER_H__

#include "Config.h"
#include "BasePopUp.h"

#include <stack>

class PopUpManager
{
public:
	stack<BasePopUp*>	m_pListPopups;
	Layer* m_pLayer;

public:
	static		PopUpManager* GetInstance(Layer* layer = 0);
	static void	setLayer(Layer* layer) { s_pInstance->m_pLayer = layer; }
	void		pushPopUp(BasePopUp* newItem);
	void		popPopUp();
	bool		isEmpty();
	BasePopUp*	getTopPopUp();
	BasePopUp*	getPopUp(int ind, std::map<string, string> inforPopUp = std::map<string, string>());
	bool		show(BasePopUp* newPopUp = 0);
	bool		close();
	BasePopUp*	getExistPopUp(int index);
private:
	vector<BasePopUp*>	m_pPoolPopups;
	bool		m_bIsHavePopUp;
	static PopUpManager* s_pInstance;
	PopUpManager();
	BasePopUp* createPopUp(int ind);
};

#endif