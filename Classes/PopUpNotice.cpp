#include "PopUpNotice.h"
#include "VisibleRect.h"
#include "PopUpManager.h"

PopUpNotice*	PopUpNotice::create(Layer* layer)
{
	auto pu = new PopUpNotice();
	pu->m_pZOrder = LLG_POPUP;
	pu->m_pLayer = layer;
	pu->m_sKey = KEY_POPUP_NOTICE;
	if (pu->init())
	{
		return pu;
	}
	CC_SAFE_DELETE(pu);
	return NULL;
}

bool PopUpNotice::init()
{
	if (!BasePopUp::init())
		return false;

	auto nodePU = CSLoader::createNode(PATH_POPUPUI_NOTICE);
	auto containPanel = nodePU->getChildByName(KEY_GAMEUI_PANEL_CONTAIN);

	auto bottomPanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_BOTTOM);
	auto topPanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_TOP);

	m_pTitleText = static_cast<ui::Text*>(topPanel->getChildByName(KEY_GAMEUI_TITLE_TEXT));
	m_pTitleText->setText("Notice");

	m_pBodyText = static_cast<ui::Text*>(containPanel->getChildByName(KEY_GAMEUI_BODY_TEXT));
	m_pBodyText->setText("Notice");

	auto btnOK = static_cast<ui::Button*>(bottomPanel->getChildByName(KEY_BUTTON_OK));
	btnOK->addClickEventListener(CC_CALLBACK_1(PopUpNotice::OnEvent, this));

	m_pNode->addChild(nodePU);
	m_pNode->setPosition(VisibleRect::center());
	m_pType = PU_NOTICE;
	return true;
}

void PopUpNotice::OnEvent(cocos2d::Ref *ref)
{
	auto btn = (ui::Button*) ref;
	string cbName = btn->getCallbackName();

	if (cbName.compare("onClickOK") == 0)
	{
		PopUpManager::GetInstance()->close();
	}
}

bool PopUpNotice::setData(std::map<string, string> inforPopUp /*= std::map<string, string>()*/)
{
	for (auto item : inforPopUp)
	{
		if (item.first.compare(KEY_GAMEUI_TITLE_TEXT) == 0)
		{
			m_pTitleText->setText(item.second);
		}
		else if (item.first.compare(KEY_GAMEUI_BODY_TEXT) == 0)
		{
			m_pBodyText->setText(item.second);
		}
	}
	return true;
}