#ifndef __POPUPRESOURCE_H__
#define __POPUPRESOURCE_H__

#include "Config.h"
#include "BasePopUp.h"

class PopUpResource	: public	BasePopUp
{
public:
	static	PopUpResource*	create(Layer* layer);

	bool	init() override;
	bool	setData(std::map<string, string> inforPopUp = std::map<string, string>()) override;
	void	OnEvent(cocos2d::Ref *ref) override;

	ui::Text*		m_pTitleText;
	ui::Text*		m_pBodyText;
};

#endif // !
