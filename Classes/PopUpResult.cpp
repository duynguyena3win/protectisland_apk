#include "PopUpResult.h"
#include "VisibleRect.h"
#include "PopUpManager.h"

PopUpResult*	PopUpResult::create(Layer* layer)
{
	auto pu = new PopUpResult();
	pu->m_pZOrder = LLG_POPUP;
	pu->m_pLayer = layer;
	pu->m_sKey = KEY_POPUP_RESULT;
	if (pu->init())
	{
		return pu;
	}
	CC_SAFE_DELETE(pu);
	return NULL;
}

bool PopUpResult::init()
{
	if (!BasePopUp::init())
		return false;

	auto nodePU = CSLoader::createNode(PATH_POPUPUI_RESULT);
	auto containPanel = nodePU->getChildByName(KEY_GAMEUI_PANEL_CONTAIN);

	auto timeLine = CSLoader::createTimeline(PATH_POPUPUI_RESULT);
	timeLine->play("idle", true);
	nodePU->runAction(timeLine);

	auto topPanel	 = containPanel->getChildByName(KEY_GAMEUI_PANEL_TOP);
	auto middlePanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_MIDDLE);
	auto bottomPanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_BOTTOM);

	m_pTitleText = static_cast<ui::Text*>(topPanel->getChildByName(KEY_GAMEUI_TITLE_TEXT));
	m_pTitleText->setText("Mission Draw");

	auto gemPanel = middlePanel->getChildByName(KEY_GAMEUI_RESOURCE_GEM);
	m_pGemText = static_cast<ui::Text*>(gemPanel->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pGemText->setText("100");

	auto expPanel = middlePanel->getChildByName(KEY_GAMEUI_RESOURCE_EXP);
	m_pExpText = static_cast<ui::Text*>(expPanel->getChildByName(KEY_GAMEUI_RESOURCE_TEXT));
	m_pExpText->setText("324");

	m_pListStar.clear();
	auto starPanel = middlePanel->getChildByName(KEY_GAMEUI_RESOURCE_STAR);
	for (int i = 0; i < 3; i++)
	{
		auto star = static_cast<Sprite*>(starPanel->getChildByName(KEY_STAR(i)));
		m_pListStar.pushBack(star);
	}

	m_pReplay = static_cast<ui::Button*>(bottomPanel->getChildByName(KEY_BUTTON_REPLAY));
	m_pReplay->addClickEventListener(CC_CALLBACK_1(PopUpResult::OnEvent, this));

	m_pOK = static_cast<ui::Button*>(bottomPanel->getChildByName(KEY_BUTTON_OK));
	m_pOK->addClickEventListener(CC_CALLBACK_1(PopUpResult::OnEvent, this));

	m_pShare = static_cast<ui::Button*>(bottomPanel->getChildByName(KEY_BUTTON_SHARE));
	m_pShare->addClickEventListener(CC_CALLBACK_1(PopUpResult::OnEvent, this));

	m_pNode->addChild(nodePU);
	m_pNode->setPosition(VisibleRect::center());
	m_pType = PU_RESULT;
	return true;
}

void PopUpResult::OnEvent(cocos2d::Ref *ref)
{
	auto btn = (ui::Button*) ref;
	string cbName = btn->getCallbackName();

	if (cbName.compare("onClickReplay") == 0)
	{
		/*	Implement */
	}
	else if (cbName.compare("onClickOK") == 0)
	{
		/*	Implement */
	}
	else if (cbName.compare("onClickShare") == 0)
	{
		/*	Implement */
	}
	PopUpManager::GetInstance()->close();
}

bool PopUpResult::setData(std::map<string, string> inforPopUp /*= std::map<string, string>()*/)
{
	string keyGemText = KEY_GAMEUI_RESOURCE_GEM;	keyGemText += KEY_GAMEUI_RESOURCE_TEXT;
	string keyExpText = KEY_GAMEUI_RESOURCE_EXP;	keyExpText += KEY_GAMEUI_RESOURCE_TEXT;
	for (auto item : inforPopUp)
	{
		if (item.first.compare(KEY_GAMEUI_TITLE_TEXT) == 0)
		{
			m_pTitleText->setText(item.second);
		}
		else if (item.first.compare(keyGemText) == 0)
		{
			m_pGemText->setText(item.second);
		}
		else if (item.first.compare(keyExpText) == 0)
		{
			m_pExpText->setText(item.second);
		}
		else if (item.first.compare(KEY_GAMEUI_RESOURCE_STAR) == 0)
		{
			short stars = atoi(item.second.c_str());
			for (int i = 0; i < 3; i++)
				if (i < stars)
					m_pListStar.at(i)->setVisible(true);
				else
					m_pListStar.at(i)->setVisible(false);
		}
	}
	return true;
}