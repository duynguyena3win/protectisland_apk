#ifndef __POPUPRESULT_H__
#define __POPUPRESULT_H__

#include "Config.h"
#include "BasePopUp.h"


#define PATH_POPUP_SOCIAL					"PopUpUI/PU_Facebook.csb"


class PopUpResult	: public	BasePopUp
{
public:
	static	PopUpResult*	create(Layer* layer);

	bool	init() override;
	bool	setData(std::map<string, string> inforPopUp = std::map<string, string>()) override;
	void	OnEvent(cocos2d::Ref *ref) override;

private:
	cocos2d::Vector<Sprite*>	m_pListStar;

	ui::Text*		m_pTitleText;
	ui::Text*		m_pGemText;
	ui::Text*		m_pExpText;

	ui::Button*		m_pReplay;
	ui::Button*		m_pOK;
	ui::Button*		m_pShare;
};

#endif // !
