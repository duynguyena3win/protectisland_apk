#include "PopUpSocial.h"
#include "VisibleRect.h"
#include "PopUpManager.h"

#include "SocialFacebook.h"

PopUpSocial*	PopUpSocial::create(Layer* layer)
{
	auto pu = new PopUpSocial();
	pu->m_pZOrder = LLG_POPUP;
	pu->m_pLayer = layer;
	pu->m_sKey = KEY_POPUP_SOCIAL;
	if (pu->init())
	{
		return pu;
	}
	CC_SAFE_DELETE(pu);
	return NULL;
}

bool PopUpSocial::init()
{
	if (!BasePopUp::init())
		return false;

	auto nodePU = CSLoader::createNode(PATH_POPUPUI_FACEBOOK);
	auto containPanel = nodePU->getChildByName(KEY_GAMEUI_PANEL_CONTAIN);

	auto bottomPanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_BOTTOM);

	m_pFacebookConnectButton = static_cast<ui::Button*>(bottomPanel->getChildByName(KEY_BUTTON_CONNECT));
	m_pFacebookConnectButton->addClickEventListener(CC_CALLBACK_1(PopUpSocial::OnEvent, this));

	m_pFacebookDisconnectButton = static_cast<ui::Button*>(bottomPanel->getChildByName(KEY_BUTTON_DISCONNECT));
	m_pFacebookDisconnectButton->addClickEventListener(CC_CALLBACK_1(PopUpSocial::OnEvent, this));

	auto btnCancel = static_cast<ui::Button*>(bottomPanel->getChildByName(KEY_BUTTON_CANCEL));
	btnCancel->addClickEventListener(CC_CALLBACK_1(PopUpSocial::OnEvent, this));
	m_pNode->setPosition(VisibleRect::center());

	m_pNode->addChild(nodePU);
	m_pType = PU_SOCIAL;
	return true;
}

void PopUpSocial::OnEvent(cocos2d::Ref *ref)
{
	auto btn = (ui::Button*) ref;
	string cbName = btn->getCallbackName();

	if (cbName.compare("onClickCancel") == 0)
	{
		PopUpManager::GetInstance()->close();
	}
	else if (cbName.compare("onClickConnect") == 0)
	{
#ifdef SDKBOX_ENABLED
		SocialFacebook::GetInstance()->login();
#endif
	}
	else if (cbName.compare("onClickDisconnect") == 0)
	{
#ifdef SDKBOX_ENABLED
		if (SocialFacebook::GetInstance()->isLoggedIn())
			SocialFacebook::GetInstance()->logout();
#endif
	}
}

bool PopUpSocial::setData(std::map<string, string> inforPopUp /*= std::map<string, string>()*/)
{
	for (auto item : inforPopUp)
	{
		if (item.first.compare(KEY_TEXT_CONTAIN) == 0)
		{
			auto contain = m_pNode->getChildByName(KEY_PANEL_CONTAIN);
			auto text = static_cast<ui::Text*>(contain->getChildByName(KEY_POUPUI_BODY_TEXT));
			text->setString(item.second);
		}
	}
	return true;
}