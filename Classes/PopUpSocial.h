#ifndef __PopUpNotice_H__
#define __PopUpNotice_H__

#include "Config.h"
#include "BasePopUp.h"

class PopUpSocial : public	BasePopUp
{
public:
	static	PopUpSocial*	create(Layer* layer);

	bool	init() override;
	bool	setData(std::map<string, string> inforPopUp = std::map<string, string>()) override;
	void	OnEvent(cocos2d::Ref *ref) override;

	ui::Button*		m_pFacebookConnectButton;
	ui::Button*		m_pFacebookDisconnectButton;
};

#endif // !
