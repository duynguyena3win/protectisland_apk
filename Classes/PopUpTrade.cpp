#include "PopUpTrade.h"
#include "VisibleRect.h"
#include "PopUpManager.h"
#include "GameScene.h"

PopUpTrade*	PopUpTrade::create(Layer* layer)
{
	auto pu = new PopUpTrade();
	pu->m_pZOrder = LLG_POPUP;
	pu->m_pLayer = layer;
	pu->m_sKey = KEY_POPUP_NOTICE;
	if (pu->init())
	{
		return pu;
	}
	CC_SAFE_DELETE(pu);
	return NULL;
}

bool PopUpTrade::init()
{
	if (!BasePopUp::init())
		return false;

	auto nodePU = CSLoader::createNode(PATH_POPUPUI_TRADE);
	auto containPanel = nodePU->getChildByName(KEY_GAMEUI_PANEL_CONTAIN);

	auto bottomPanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_BOTTOM);
	auto topPanel = containPanel->getChildByName(KEY_GAMEUI_PANEL_TOP);

	m_pTitleText = static_cast<ui::Text*>(topPanel->getChildByName(KEY_GAMEUI_TITLE_TEXT));
	m_pTitleText->setText("Notice");

	m_pBodyText = static_cast<ui::Text*>(containPanel->getChildByName(KEY_GAMEUI_BODY_TEXT));
	m_pBodyText->setText("Notice");

	auto btnYes = static_cast<ui::Button*>(bottomPanel->getChildByName(KEY_BUTTON_YES));
	btnYes->addClickEventListener(CC_CALLBACK_1(PopUpTrade::OnEvent, this));

	auto btnNo = static_cast<ui::Button*>(bottomPanel->getChildByName(KEY_BUTTON_NO));
	btnNo->addClickEventListener(CC_CALLBACK_1(PopUpTrade::OnEvent, this));

	m_pNode->addChild(nodePU);
	m_pNode->setPosition(VisibleRect::center());
	m_pType = PU_TRADE;
	return true;
}

void PopUpTrade::OnEvent(cocos2d::Ref *ref)
{
	auto btn = (ui::Button*) ref;
	string cbName = btn->getCallbackName();

	if (cbName.compare("onClickYes") == 0)
	{
		GameScene::GetInstance()->callBackPopUpEvent(ID_YES, m_pType);
	}
	else if (cbName.compare("onClickNo") == 0)
	{
		
	}
	PopUpManager::GetInstance()->close();
}

bool PopUpTrade::setData(std::map<string, string> inforPopUp /*= std::map<string, string>()*/)
{
	for (auto item : inforPopUp)
	{
		if (item.first.compare(KEY_GAMEUI_TITLE_TEXT) == 0)
		{
			m_pTitleText->setText(item.second);
		}
		else if (item.first.compare(KEY_GAMEUI_BODY_TEXT) == 0)
		{
			m_pBodyText->setText(item.second);
		}
	}
	return true;
}