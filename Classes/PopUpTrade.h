#ifndef __POPUPTRADE_H__
#define __POPUPTRADE_H__

#include "Config.h"
#include "BasePopUp.h"

class PopUpTrade	: public	BasePopUp
{
public:
	static	PopUpTrade*	create(Layer* layer);

	bool	init() override;
	bool	setData(std::map<string, string> inforPopUp = std::map<string, string>()) override;
	void	OnEvent(cocos2d::Ref *ref) override;

	ui::Text*		m_pTitleText;
	ui::Text*		m_pBodyText;
};

#endif // !
