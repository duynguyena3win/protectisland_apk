#include "RocketS300.h"

RocketS300* RocketS300::create(Layer* layer)
{
	auto rk = new RocketS300();
	rk->m_pLayer = layer;
	if (rk->init())
	{
		return rk;
	}
	CC_SAFE_DELETE(rk);
	return NULL;
}

bool RocketS300::init()
{
	if (!GameEntity::init())
	{
		return false;
	}

	m_iID = ID_ROCKETS300;

	setActive(false);
	m_pNode->setPosition(Vec2(2000, 2000));
	return true;
}

void RocketS300::setRotate(Vec2 point)
{

}

Action* RocketS300::createAction(Vec2 target)
{
	return NULL;
}

void RocketS300::setState(int state)
{
	if (m_pState == state)
		return;

	m_pState = state;

	switch (state)
	{
	case IDLE_ENTITY:
		m_pTargetEnemy = 0;
		m_pInfo->reset();
		m_pStateItem->reset();
		setActive(false);
		break;

	case ACTIVE_ENTITY:
		setActive(true);
		break;

	case FINISHED_ENTITY:
	case DESTROY_ENTITY:
	{
		m_pNode->setPosition(Vec2(2000, 2000));
		m_pInfo->reset();
		m_pStateItem->reset();
		setActive(false);
		m_pTargetEnemy = 0;
	}
		break;
	default:
		break;
	}
}

void RocketS300::setTarget(GameEntity* target)
{
	if ((target->getId() == ID_AIRCRAFTCARRIERSHIP && target->isActive()) && !isHaveTarget())
	{
		m_pBarrelEntities.at(0)->unschedule("SHOT_KEY");
		GameEntity::setTarget(target);
	}
	else if ((target->getId() == ID_DESTROYER956 && target->isActive()) && !isHaveTarget())
	{
		m_pBarrelEntities.at(0)->unschedule("SHOT_KEY");
		GameEntity::setTarget(target);
	}
}

void RocketS300::setPosition(Vec2 pos, Size sizeItem)
{

}

void RocketS300::destroy()
{
	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	//
}


void RocketS300::finish()
{
	// stop action
	stopAllActions();
	m_pBarrelEntities.at(0)->stopAllActions();
	m_pBarrelEntities.at(0)->unscheduleAllCallbacks();
	//
}


void RocketS300::shooting()
{

}