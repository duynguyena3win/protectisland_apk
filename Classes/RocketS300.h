#ifndef __ROCKETS300_H__
#define __ROCKETS300_H__

#include "GameEntity.h"

class RocketS300 : public GameEntity
{
public:
	static RocketS300* create(Layer* layer);
	virtual bool init();
	void	setRotate(Vec2 point);
	void	setPosition(Vec2 pos, Size sizeItem = Size(1, 1));
	Action*	createAction(Vec2 target = Vec2(0,0));
	void	setState(int state);
	void	setTarget(GameEntity* target) override;
	virtual void destroy();
	virtual void finish();
	virtual void shooting();
};

#endif