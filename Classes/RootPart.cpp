#include "RootPart.h"

bool	RootPart::init()
{
	m_pNode = Node::create();
	m_pNode->setUserData(this);
	m_pNode->setTag(TAG_DEFAULT);

	m_vDirection = Vec2(0, 1);
	m_pParent->addChild(m_pNode, m_pZOrder);
	return true;
}

void	RootPart::setRotation(float rotation)
{
	m_pNode->setRotation(rotation);
}