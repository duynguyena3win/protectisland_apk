#ifndef __ROOTPART_H__
#define __ROOTPART_H__

#include "Config.h"

enum ENTITY_PART
{
	EP_UNKNOW	= 0,
	EP_BODY		= 1,
	EP_BARREL	= 2,
};

class RootPart
{
public:
	virtual	bool	init();
	virtual	void	setPosition(Vec2 pos)	{ m_pNode->setPosition(pos); }
	virtual void	setScale(float sca)		{ m_pNode->setScale(sca); }
	virtual	void	setRotation(float rotation);

	virtual	float	getRotation()	{ return m_pNode->getRotation(); }
	virtual int		getType()		{ return m_iType; }
	virtual	Vec2	getPosition()	{ return m_pNode->getPosition(); }
	virtual	float	getScale()		{ return m_pNode->getScale(); }
	virtual	Vec2	getDirection()	{ return m_vDirection; }

protected:
	Node*		m_pParent;
	Node*		m_pNode;
	int			m_pZOrder;

	int			m_iType;
	Vec2		m_vDirection;
};

#endif