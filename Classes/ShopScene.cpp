#include "ShopScene.h"
#include "Config.h"
#include "VisibleRect.h"
#include "Utils.h"
#include "GameIAP.h"
#include "FreemiumBar.h"

#include "cocostudio\CocoStudio.h"
#include "ui\UIListView.h"
#include "ui\UIButton.h"

cocos2d::Scene* ShopScene::createScene()
{
	auto sence = Scene::create();
	auto layer = ShopScene::create();
	sence->addChild(layer);
	return sence;
}

ShopScene * ShopScene::create()
{
	auto scene = new ShopScene();
	if (scene->init())
	{
		scene->autorelease();
		return scene;
	}
	CC_SAFE_DELETE(scene);

	return NULL;
}

bool ShopScene::init()
{
	if(!Layer::init())
	{
		return false;
	}

#ifdef SDKBOX_ENABLED	// Init IAP 
	LOGI("Init Game IAP");
	sdkbox::IAP::setDebug(true);
	sdkbox::IAP::setListener(this);
#endif

	// New loading Menu
	auto layoutMenu = CSLoader::createNode(PATH_GAMEUI_SHOPSCENE_CSB);
	addChild(layoutMenu, 1, KEY_SCENE_UI_SHOP);
	layoutMenu->setPosition(Vec2::ZERO);

	auto panelContain = layoutMenu->getChildByName(KEY_GAMEUI_PANEL_CONTAIN);

	auto topContain = panelContain->getChildByName(KEY_GAMEUI_PANEL_TOP);
	auto middleContain = panelContain->getChildByName(KEY_GAMEUI_PANEL_MIDDLE);

	m_pListGEMs = static_cast<ui::ListView*>(middleContain->getChildByName(KEY_GAMEUI_PANEL_LISTVIEW_GEM)->getChildByName(KEY_GAMEUI_LISTVIEW));
	m_pListEXPs = static_cast<ui::ListView*>(middleContain->getChildByName(KEY_GAMEUI_PANEL_LISTVIEW_EXP)->getChildByName(KEY_GAMEUI_LISTVIEW));

	if (!initGEMItems())
		return false;
	if (!initEXPItems())
		return false;

	auto btnBack = static_cast<ui::Button*>(topContain->getChildByName(KEY_GAMEUI_BUTTON_BACK));
	btnBack->addClickEventListener([this](Ref* sender) {
		Director::getInstance()->popScene();
	});

	return true;
}

bool	ShopScene::initGEMItems()
{
	auto m_pGameIAP = GameIAP::GetInstance();

	for (auto infoItem : m_pGameIAP->m_pListGEM)
	{
		auto item = CSLoader::createNode(PATH_ITEMIAP_CSB);
		
		auto timeLine = CSLoader::createTimeline(PATH_ITEMIAP_CSB);
		timeLine->play("idle", true);
		item->runAction(timeLine);

		auto panelContain = item->getChildByName(KEY_PANEL_CONTAIN);

		// text of name
		auto nameItem = static_cast<ui::Text*>(panelContain->getChildByName(KEY_TEXT_TITLE));
		nameItem->setString(infoItem->m_sNameItem);

		// text of cost
		auto valueItem = static_cast<ui::Text*>(panelContain->getChildByName(KEY_TEXT_VALUE));
		valueItem->setString(StringUtils::toString(infoItem->m_iValue));

		// function for Button
		auto btnBuyNow = static_cast<ui::Button*>(panelContain->getChildByName(KEY_BUTTON_BUYNOW));
		btnBuyNow->addClickEventListener([infoItem](Ref* ref) {
#ifdef SDKBOX_ENABLED
			sdkbox::IAP::purchase(infoItem->m_sIdItem);
#endif // SDKBOX_ENABLED
		});

		// push back to ListView
		auto layoutItem = ui::Layout::create();
		auto containSize = panelContain->getContentSize() + Size(60, 0);
		layoutItem->setContentSize(containSize);

		layoutItem->addChild(item);
		item->setPosition(Vec2(0, layoutItem->getContentSize().height));
		
		m_pListGEMs->pushBackCustomItem(layoutItem);
	}
	return true;
}

bool	ShopScene::initEXPItems()
{
	auto m_pGameIAP = GameIAP::GetInstance();

	for (auto infoItem : m_pGameIAP->m_pListEXP)
	{
		auto item = CSLoader::createNode(PATH_ITEMIAP_CSB);

		auto timeLine = CSLoader::createTimeline(PATH_ITEMIAP_CSB);
		timeLine->play("idle", true);
		item->runAction(timeLine);

		auto panelContain = item->getChildByName(KEY_PANEL_CONTAIN);

		// text of name
		auto nameItem = static_cast<ui::Text*>(panelContain->getChildByName(KEY_TEXT_TITLE));
		nameItem->setString(infoItem->m_sNameItem);

		// text of cost
		auto valueItem = static_cast<ui::Text*>(panelContain->getChildByName(KEY_TEXT_VALUE));
		valueItem->setString(StringUtils::toString(infoItem->m_iValue));

		// function for Button
		auto btnBuyNow = static_cast<ui::Button*>(panelContain->getChildByName(KEY_BUTTON_BUYNOW));
		btnBuyNow->addClickEventListener([infoItem](Ref* ref) {
#ifdef SDKBOX_ENABLED
			sdkbox::IAP::purchase(infoItem->m_sIdItem);
#endif // SDKBOX_ENABLED
		});

		// push back to ListView
		auto layoutItem = ui::Layout::create();
		auto containSize = panelContain->getContentSize() + Size(60, 0);
		layoutItem->setContentSize(containSize);

		layoutItem->addChild(item);
		item->setPosition(Vec2(0, layoutItem->getContentSize().height));

		m_pListEXPs->pushBackCustomItem(layoutItem);
	}
	return true;
}

#ifdef SDKBOX_ENABLED
void ShopScene::onInitialized(bool ok)
{
	LOGI("ShopScene :: onInitialized");
}

void ShopScene::onSuccess(sdkbox::Product const& p)
{
	LOGI("ShopScene :: onSuccess");
}

void ShopScene::onFailure(sdkbox::Product const& p, const std::string &msg)
{
	LOGI("ShopScene :: onFailure");
}

void ShopScene::onCanceled(sdkbox::Product const& p)
{
	LOGI("ShopScene :: onCanceled");
}

void ShopScene::onRestored(sdkbox::Product const& p)
{
	LOGI("ShopScene :: onRestored");
}

void ShopScene::onProductRequestSuccess(std::vector<sdkbox::Product> const &products)
{
	LOGI("ShopScene :: onProductRequestSuccess");
}

void ShopScene::onProductRequestFailure(const std::string &msg)
{
	LOGI("ShopScene :: onProductRequestFailure");
}

void ShopScene::onRestoreComplete(bool ok, const std::string &msg)
{
	LOGI("ShopScene :: onRestoreComplete");
}
#endif