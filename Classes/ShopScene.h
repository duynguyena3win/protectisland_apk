#ifndef __SHOPSCENE_H__
#define __SHOPSCENE_H__

#include "Config.h"
#include "cocostudio\CocoStudio.h"
#include "ui\UIListView.h"

#ifdef SDKBOX_ENABLED
#include "Sdkbox/Sdkbox.h"
#include "PluginIAP/PluginIAP.h"
#endif

#define SPRITE_SHOPSCENE_BACKGROUND				"ShopScene/bg_scene.png"
#define SPRITE_SHOPSCENE_SHOPITEM				"ShopScene/ShopItem.png"
#define SPRITE_SHOPSENCE_BUY_BUTTON_ENABLE		"Buttons/BtnBuy_Ena.png"
#define SPRITE_SHOPSENCE_BUY_BUTTON_DISABLE		"Buttons/BtnBuy_Dis.png"

enum LAYER_SHOPSCENE
{
	LSS_BACKGROUND,
	LSS_TITLE,
	LSS_BUTTON
};

class ShopScene : public cocos2d::Layer
#ifdef SDKBOX_ENABLED
	, public sdkbox::IAPListener
#endif
{
public:
	static cocos2d::Scene* createScene();
	static ShopScene* create();
	virtual bool init();

private:
	bool initGEMItems();
	bool initEXPItems();
	
private:
	Node* m_pTableItems;
	
	ui::ListView* m_pListGEMs;
	ui::ListView* m_pListEXPs;

	Vec2 p_OldTouch;
	int m_iMaxWidth;

#ifdef SDKBOX_ENABLED	// callback from IAPListener
	virtual void onInitialized(bool ok) override;
	virtual void onSuccess(sdkbox::Product const& p) override;
	virtual void onFailure(sdkbox::Product const& p, const std::string &msg) override;
	virtual void onCanceled(sdkbox::Product const& p) override;
	virtual void onRestored(sdkbox::Product const& p) override;
	virtual void onProductRequestSuccess(std::vector<sdkbox::Product> const &products) override;
	virtual void onProductRequestFailure(const std::string &msg) override;
	void onRestoreComplete(bool ok, const std::string &msg);
#endif
};

#endif // !__SHOPSCENE_H__
