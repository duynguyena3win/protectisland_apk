#include "SmallBulletExploding.h"
#include "Utils.h"

SmallBulletExploding* SmallBulletExploding::create(Layer* layer)
{
	auto gem = new SmallBulletExploding();
	gem->m_pLayer = layer;
	if (gem->init())
	{
		return gem;
	}
	CC_SAFE_DELETE(gem);
	return NULL;
}

bool SmallBulletExploding::init()
{
	if (!EffectEntity::init())
	{
		return false;
	}
	
	//Load effect exploding
	ArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo(SPRITE_SMALL_BULLET_EXPLODING, PLIST_SMALL_BULLET_EXPLODING, EXPORTJSON_SMALL_BULLET_EXPLODING);
	m_pArmExploding = Armature::create(NAME_SMALL_BULLET_EXPLODING);
	m_pArmExploding->setPosition(Vec2::ZERO);
	m_pArmExploding->setScale(1.5f);
	m_pNode->addChild(m_pArmExploding);


	m_pArmExploding->getAnimation()->setMovementEventCallFunc([this](Armature* arma, MovementEventType eventType, string moventID) {
		if (eventType == MovementEventType::COMPLETE)
			setActive(false);
	});

	m_iID = ID_SMALLBULLET;
	setActive(false);
	return true;
}

void SmallBulletExploding::setPosition(Vec2 pos)
{
	EffectEntity::setPosition(pos);
	runAction();
}

Action* SmallBulletExploding::createAction()
{
	return NULL;
}

void SmallBulletExploding::runAction()
{
	vector<string> animation;
	animation.push_back("exploding");
	setActive(true);
	m_pArmExploding->getAnimation()->playWithNames(animation, -1, false);
}