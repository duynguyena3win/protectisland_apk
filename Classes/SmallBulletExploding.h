#ifndef __SMALLBULLETEXPLODING_H__
#define __SMALLBULLETEXPLODING_H__

#include "EffectEntity.h"

#define	NAME_SMALL_BULLET_EXPLODING			"SmallBulletExploding"
#define SPRITE_SMALL_BULLET_EXPLODING		"Effects/SmallBulletExploding/SmallBulletExploding0.png"
#define PLIST_SMALL_BULLET_EXPLODING		"Effects/SmallBulletExploding/SmallBulletExploding0.plist"
#define EXPORTJSON_SMALL_BULLET_EXPLODING	"Effects/SmallBulletExploding/SmallBulletExploding.ExportJson"

class SmallBulletExploding : public EffectEntity
{
public:
	static SmallBulletExploding* create(Layer* layer);
	bool init();
	void setPosition(Vec2 pos);
	void runAction();
	Action* createAction();
};

#endif