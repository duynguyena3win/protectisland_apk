#ifdef SDKBOX_ENABLED
#include "SocialFacebook.h"
#include "UserProfile.h"
#include "PopUpManager.h"
#include "PopUpSocial.h"

SocialFacebook* SocialFacebook::s_pInstance = 0;

SocialFacebook* SocialFacebook::GetInstance()
{
	if(!s_pInstance)
		s_pInstance = new SocialFacebook();
	return s_pInstance;
}

SocialFacebook::SocialFacebook()
{
	LOGI("Init SocialFacebook");
	sdkbox::PluginFacebook::setListener(this);
}

void SocialFacebook::onLogin(bool isLogin, const std::string& msg)
{
	if(isLogin)
	{
		LOGI("onLogin");
		UserProfile::Get()->m_pGem += 100;

		std::map<string, string> infors;
		infors.insert(std::pair<string, string>(KEY_GAMEUI_TITLE_TEXT, "Welcome"));
		infors.insert(std::pair<string, string>(KEY_GAMEUI_BODY_TEXT, "You already logged in Facebook!"));
		auto Popup = PopUpManager::GetInstance()->getPopUp(PU_NOTICE, infors);
		PopUpManager::GetInstance()->show(Popup);

		PopUpSocial* socialPopUp = static_cast<PopUpSocial*>(PopUpManager::GetInstance()->getExistPopUp(PU_SOCIAL));
		socialPopUp->m_pFacebookConnectButton->setVisible(false);
		socialPopUp->m_pFacebookDisconnectButton->setVisible(true);
	}
}

void SocialFacebook::onSharedSuccess(const std::string& message)
{
	LOGI("SocialFacebook :: onSharedSuccess");
}

void SocialFacebook::onSharedFailed(const std::string& message)
{
	LOGI("SocialFacebook :: onSharedFailed");
}

void SocialFacebook::onSharedCancel()
{
	LOGI("SocialFacebook :: onSharedCancel");
}

void SocialFacebook::onAPI(const std::string& key, const std::string& jsonData)
{
	LOGI("SocialFacebook :: onAPI");
}

void SocialFacebook::onPermission(bool isLogin, const std::string& msg)
{
	LOGI("SocialFacebook :: onPermission");
}

void SocialFacebook::onFetchFriends(bool ok, const std::string& msg)
{
	LOGI("SocialFacebook :: onFetchFriends");
}

void SocialFacebook::onRequestInvitableFriends(const sdkbox::FBInvitableFriendsInfo& friends)
{
	LOGI("SocialFacebook :: onRequestInvitableFriends");
}

void SocialFacebook::onInviteFriendsWithInviteIdsResult(bool result, const std::string& msg)
{
	LOGI("SocialFacebook :: onInviteFriendsWithInviteIdsResult");
}

void SocialFacebook::onInviteFriendsResult(bool result, const std::string& msg)
{
	LOGI("SocialFacebook :: onInviteFriendsResult");
}

void SocialFacebook::onGetUserInfo(const sdkbox::FBGraphUser& userInfo)
{
	LOGI("SocialFacebook :: onGetUserInfo");
}

void SocialFacebook::login()
{
	std::vector<std::string> permissions;
	permissions.push_back(sdkbox::FB_PERM_READ_PUBLIC_PROFILE);
	permissions.push_back(sdkbox::FB_PERM_READ_EMAIL);
	permissions.push_back(sdkbox::FB_PERM_PUBLISH_POST);
	sdkbox::PluginFacebook::login();
	LOGI("Login Facebook");
}

void SocialFacebook::logout()
{
	sdkbox::PluginFacebook::logout();
	LOGI("LogOut Facebook");
}

bool SocialFacebook::isLoggedIn()
{
	LOGI("Check Facebook is LoggedIn");
	return sdkbox::PluginFacebook::isLoggedIn();
}

#endif