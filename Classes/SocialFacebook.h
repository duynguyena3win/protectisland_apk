#ifdef SDKBOX_ENABLED

#ifndef __SOCIALFACEBOOK_H__
#define __SOCIALFACEBOOK_H__

#include "Config.h"
#include "Sdkbox/Sdkbox.h"
#include "PluginFacebook/PluginFacebook.h"


class SocialFacebook : public sdkbox::FacebookListener
{
public:
	static SocialFacebook* GetInstance();
	void	login();
	void	logout();
	bool	isLoggedIn();
private:
	SocialFacebook();
	virtual void onLogin(bool isLogin, const std::string& msg);
	virtual void onSharedSuccess(const std::string& message);
	virtual void onSharedFailed(const std::string& message);
	virtual void onSharedCancel();
	virtual void onAPI(const std::string& key, const std::string& jsonData);
	virtual void onPermission(bool isLogin, const std::string& msg);
	virtual void onFetchFriends(bool ok, const std::string& msg);
	virtual void onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo& friends );
	virtual void onInviteFriendsWithInviteIdsResult( bool result, const std::string& msg );
	virtual void onInviteFriendsResult( bool result, const std::string& msg );

	virtual void onGetUserInfo( const sdkbox::FBGraphUser& userInfo );
private:
	static SocialFacebook* s_pInstance;
};

#endif

#endif