#include "SpineHelper.h"

static SpineHelper* _spineHelperInstance = nullptr;

bool SpineHelper::lazyInit(std::string prefix, std::string name)
{
	_atlasFile = prefix + "/" + name + ".atlas";
	_dataFile = prefix + "/" + name + ".json";
	return true;
}

SpineHelper* SpineHelper::getInstance()
{
	if (_spineHelperInstance == nullptr)
	{
		_spineHelperInstance = new SpineHelper();
	}
	return _spineHelperInstance;
}

void SpineHelper::destroyInstance()
{
	if (_spineHelperInstance != nullptr)
	{
		delete _spineHelperInstance;
		_spineHelperInstance = nullptr;
	}
}
