#ifndef _SPINE_HELPER_H_
#define _SPINE_HELPER_H_

#include <string>

class SpineHelper
{
public:
	static SpineHelper* getInstance();

	bool lazyInit(std::string prefix, std::string name);

	std::string _dataFile;
	std::string _atlasFile;

	static void destroyInstance();
private:
	SpineHelper() {}
};

#endif