#include "UpgradeScene.h"
#include "Config.h"
#include "VisibleRect.h"
#include "Utils.h"
#include "UserProfile.h"
#include "PopUpManager.h"
#include "LevelScene.h"
#include "FreemiumBar.h"

cocos2d::Scene* UpgradeScene::createScene()
{
	auto scene = Scene::create();
	auto layer = UpgradeScene::create();
	scene->addChild(layer);

	return scene;
}

UpgradeScene * UpgradeScene::create()
{
	auto scene = new UpgradeScene();
	if (scene->init())
	{
		scene->autorelease();
		return scene;
	}
	CC_SAFE_DELETE(scene);

	return NULL;
}

bool UpgradeScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto background = Sprite::create(SPRITE_UPGRADESCENE_BACKGROUND);
	Vec2 sizeBG = background->getContentSize();
	addChild(background);

	background->setPosition(VisibleRect::center());

	m_pUserProfile = UserProfile::Get();

	if (!initItems())
		return false;

	auto btnBack = Utils::addButton(SPRITE_BUTTON_BACK_ENABLE, SPRITE_BUTTON_BACK_DISABLE, SPRITE_BUTTON_BACK_DISABLE, [this](Ref* ref){
		auto scene = LevelScene::createScene();
		auto trans = TransitionFadeDown::create(0.5f, scene);
		Director::getInstance()->popScene();
	});
	btnBack->setPosition(sizeBG - Vec2(70, sizeBG.y - 70));

	auto m_pMenu = Menu::createWithItem(btnBack);
	m_pMenu->setPosition(0, 0);
	addChild(m_pMenu);
	initTouch();
	this->scheduleUpdate();

	FreemiumBar::BeginLevel(this);
	//FREEMIUM_BAR->setVisible(FI_SUBTABLE, true);
	//FREEMIUM_BAR->setVisible(FI_EXP, true);
	//FREEMIUM_BAR->setVisible(FI_HEATHBAR, false);
	//FREEMIUM_BAR->setVisible(FI_ENEMYBAR, false);
	//FREEMIUM_BAR->setTitle("Upgrade Scene");
	PopUpManager::GetInstance(this);
	return true;
}

void UpgradeScene::update(float delta)
{
	auto point1 = m_pTableItems->getPosition();
	auto size = Vec2(m_iMaxWidth, 0);
	auto point2 = m_pTableItems->getPosition() + size;
	Vec2 newPos = Vec2(0, 0);

	auto b1 = point1.x > 0;
	auto b2 = point2.x < DESIGN_RESOLUTION_WIDTH;

	if (point1.x > 0)
	{
		newPos.x = -1;
	}
	
	if (point2.x < DESIGN_RESOLUTION_WIDTH)
	{
		newPos.x = -(m_iMaxWidth - DESIGN_RESOLUTION_WIDTH) + 10;
	}
	
	if (b1 || b2)
	{
		m_pTableItems->stopAllActions();
		m_pTableItems->setPosition(newPos);
	}
}

void UpgradeScene::initTouch()
{
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	auto listener = EventListenerTouchOneByOne::create();

	listener->onTouchBegan = CC_CALLBACK_2(UpgradeScene::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(UpgradeScene::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(UpgradeScene::onTouchEnded, this);

	dispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	auto pKeybackListener = EventListenerKeyboard::create();
	pKeybackListener->onKeyReleased = CC_CALLBACK_2(UpgradeScene::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(pKeybackListener, this);
}

void UpgradeScene::onTouchMoved(Touch *touch, Event *event)
{
	auto position = Vec2(touch->getLocation().x, 0);
	auto moveDirec = position - p_OldTouch;
	auto newPosMap = m_pTableItems->getPosition() + moveDirec;

	//
	auto point1 = m_pTableItems->getPosition();
	auto point2 = m_pTableItems->getPosition() + Vec2(m_iMaxWidth, 0);
	auto b1 = point1.x > 0;
	auto b2 = point2.x < DESIGN_RESOLUTION_WIDTH;
	if (b1 || b2)
	{
		//newPosMap = m_pMapGame->getPosition();
		//log("Go out view! ");
		//m_pMapGame->setPosition(Vec2(0, 0));
	}
	else
	{
		m_pTableItems->runAction(MoveTo::create(0.3f, newPosMap));
		p_OldTouch = position;
	}
}

bool UpgradeScene::onTouchBegan(Touch *touch, Event *event)
{
	p_OldTouch = touch->getLocation();
	p_OldTouch.y = 0;
	return true;
}

void UpgradeScene::onTouchEnded(cocos2d::Touch *touch, Event *event)
{
	
}

bool UpgradeScene::initItems()
{
	m_pTableItems = Node::create();
	m_pTableItems->setPosition(0, 0);
	Vec2 sizeTable = Vec2(200, 267);
	auto posInfoButton = sizeTable - Vec2(sizeTable.x / 8, 1.5 * sizeTable.y / 8);
	auto posUpButton = Vec2(1.35 * sizeTable.x / 8, 0.97 * sizeTable.y / 8);
	auto posTitle = Vec2(sizeTable.x / 2, 7.35 * sizeTable.y / 8);
	auto posCost = Vec2(sizeTable.x / 2, 0.97 * sizeTable.y / 8);

	auto posCashIcon = Vec2(6.5 * sizeTable.x / 8, 0.97 * sizeTable.y / 8);
	auto posItem = VisibleRect::left() + Vec2(250, 0);
	
	vector<string> listNames;
	vector<int> listValues;
	vector<int> listIds;

	for (auto id : m_pUserProfile->m_pIdWeapons)
	{
		if (m_pUserProfile->getLevelById(id) <= 0)
			continue;
		listIds.push_back(id);
		listNames.push_back(Utils::getNameById(id));
		listValues.push_back(Utils::getValueById(id));
	}

	for (int i = 0; i < listIds.size(); i++)
	{
		
		cocos2d::Vector<MenuItem*> listButtons;
		auto itemBar = Sprite::create(SPRITE_UPGRADE_ITEM_BACKGROUND);
		
		// Title of Item --> Name
		auto title = Label::createWithBMFont(FONT_GAME, listNames[i]);
		title->setColor(Color3B::BLUE);
		title->setScale(0.7f);
		title->setPosition(posTitle);
		itemBar->addChild(title, LUS_TITLE);

		// Cost of Item --> Value
		auto cost = Label::createWithBMFont(FONT_GAME, StringUtils::format("%d", listValues[i]));
		cost->setScale(0.8f);
		cost->setColor(Color3B::YELLOW);
		cost->setPosition(posCost);
		m_pMapValueCost.insert(pair<int, Label*>(listIds[i], cost));
		itemBar->addChild(cost, LUS_TITLE, "KEY_VALUE_" + StringUtils::toString(listIds[i]));

		// icon of Cash
		auto cash = Sprite::create(SPRITE_EFFECT_EXP_ITEM);
		cash->setScale(0.9f);
		cash->setColor(Color3B::GREEN);
		cash->setPosition(posCashIcon);
		itemBar->addChild(cash, LUS_TITLE);

		// Button Information
		auto btn_Info = Utils::addButton(SPRITE_UPGRADE_INFO_BUTTON_ENABLE, SPRITE_UPGRADE_INFO_BUTTON_DISABLE, SPRITE_UPGRADE_INFO_BUTTON_DISABLE, [this](Ref* ref)
		{
			auto but = (MenuItem*) ref;
			showInfoItem(but->getTag(), 1);
		});
		btn_Info->setTag(listIds[i]);
		btn_Info->setPosition(posInfoButton);

		listButtons.pushBack(btn_Info);

		// Button Up
		auto btn_Up = Utils::addButton(SPRITE_UPGRADE_UP_BUTTON_ENABLE, SPRITE_UPGRADE_UP_BUTTON_DISABLE, SPRITE_UPGRADE_UP_BUTTON_DISABLE, [this](Ref* ref)
		{
			auto but = (MenuItem*)ref;
			auto id = but->getTag();
			
			auto cost = ((String)m_pMapValueCost[id]->getString()).intValue();
			if (m_pUserProfile->m_pEXP < cost)
			{
				// show Pop-up dont have enough
				std::map<string, string> infors;
				string text = "\tYou  not  enough  " + Utils::getNameResourceByType(ID_EXP) + "!\nYou  need  " + StringUtils::format("%d", cost - m_pUserProfile->m_pEXP) + "  " + Utils::getNameResourceByType(ID_EXP);
				text += "\n\tPurchase it or Watch Video to get free EXP! ";
				infors.insert(std::pair<string, string>(KEY_GAMEUI_BODY_TEXT, text));

				auto Popup = PopUpManager::GetInstance(this)->getPopUp(PU_RESOURCE, infors);
				PopUpManager::GetInstance()->show(Popup);
			}
			else
			{
				m_pMapValueCost[id]->setString(StringUtils::format("%d", Utils::getValueById(id)));
				m_pUserProfile->updateItem(m_pUserProfile->getLevelById(id) + 1, id);
				m_pUserProfile->m_pEXP -= cost;
				m_pUserProfile->updateItem(ID_EXP);
			}
		});

		btn_Up->setTag(listIds[i]);
		btn_Up->setPosition(posUpButton);
		listButtons.pushBack(btn_Up);

		auto menu = Menu::createWithArray(listButtons);
		menu->setPosition(Vec2(0, 0));
		itemBar->addChild(menu, LUS_BUTTON);
		m_pTableItems->addChild(itemBar, 1, "KEY_ITEMBAR_" + StringUtils::toString(listIds[i]));
		itemBar->setAnchorPoint(Vec2(0, 0.5f));
		itemBar->setPosition(posItem.x * i + 50.0f, posItem.y);
	}
	m_iMaxWidth = (posItem.x * NUMBER_WEAPONS + 50 * NUMBER_WEAPONS);
	addChild(m_pTableItems);
	return true;
}

void UpgradeScene::showInfoItem(int idItem, int level)
{
	auto popup = PopUpManager::GetInstance(this)->getPopUp(PU_ITEMINFORMATION, Utils::genItemInformation(idItem));
	PopUpManager::GetInstance()->show(popup);
}