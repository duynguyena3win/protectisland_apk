#ifndef __UPGRADESCENE_H__
#define __UPGRADESCENE_H__

#include "Config.h"

#define SPRITE_UPGRADE_ITEM_BACKGROUND		"UpgradeScene/ItemBar.png"
#define SPRITE_UPGRADE_UP_BUTTON_ENABLE		"Buttons/BtnUp_Ena.png"
#define SPRITE_UPGRADE_UP_BUTTON_DISABLE	"Buttons/BtnUp_Dis.png"
#define SPRITE_UPGRADE_INFO_BUTTON_ENABLE	"Buttons/BtnInfo_Ena.png"
#define SPRITE_UPGRADE_INFO_BUTTON_DISABLE	"Buttons/BtnInfo_Dis.png"

enum LAYER_UPGRADESCENE
{
	LUS_BACKGROUND,
	LUS_TITLE,
	LUS_BUTTON
};

class UserProfile;

class UpgradeScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	static UpgradeScene* create();
	void update(float delta);

protected:
	bool initItems();
	void showInfoItem(int idItem, int level);

private:
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchMoved(cocos2d::Touch *touch, Event *event);
	void onTouchEnded(cocos2d::Touch *touch, Event *event);
	void initTouch();
	UserProfile* m_pUserProfile;
	int m_iMaxWidth;
	Vec2 p_OldTouch;
	map<int, Label*> m_pMapValueCost;
	Node* m_pTableItems;
};


#endif