#include "UserProfile.h"
#include "Utils.h"
#include "FreemiumBar.h"

UserProfile* UserProfile::s_pInstance = 0;

UserProfile::UserProfile()
{
	m_pCurrentMission = 0;
	m_pCurrntLevel = 2;

	m_pIdWeapons.push_back(ID_MACHINEGUN);
	m_pLevelWepons.push_back(1);

	m_pIdWeapons.push_back(ID_FOGGUN);
	m_pLevelWepons.push_back(1);

	m_pIdWeapons.push_back(ID_CANON37MM);
	m_pLevelWepons.push_back(1);

	m_pIdWeapons.push_back(ID_CANON203MM);
	m_pLevelWepons.push_back(0);

	m_pIdWeapons.push_back(ID_ROCKETS300);
	m_pLevelWepons.push_back(1);

	m_pLevelStars.clear();
	for (int i = 0; i < MAX_GAME_LEVEL * MAX_GAME_MISSION; i++)
	{
		m_pLevelStars.push_back(0);
	}

	LOGI("User Profile Constructer");
	if (!checkData())
		resetData();
	loadData();
}

UserProfile* UserProfile::Get()
{
	if (!s_pInstance)
		s_pInstance = new UserProfile();
	return s_pInstance;
}

bool UserProfile::checkData()
{
	auto str_result = UserDefault::getInstance()->getStringForKey(KEY_DP_SAVE);
	auto result = Utils::decodeInt(str_result.c_str());
	return (result == 1);
}

void UserProfile::loadData()
{
	LOGI("Load User Profile!");
	m_pCurrentMission = Utils::decodeInt(UserDefault::getInstance()->getStringForKey(KEY_DP_MISSION).c_str());
	m_pCurrntLevel = Utils::decodeInt(UserDefault::getInstance()->getStringForKey(KEY_DP_LEVEL).c_str());
	m_pGem = Utils::decodeInt(UserDefault::getInstance()->getStringForKey(KEY_DP_GEM).c_str());
	m_pEXP = Utils::decodeInt(UserDefault::getInstance()->getStringForKey(KEY_DP_EXP).c_str());
	m_iNumWeapons = 0;
	for (int i = 0; i < m_pIdWeapons.size(); i++)
	{
		string levelWeaponKey = KEY_DP_LEVELWEAPONS + StringUtils::toString(m_pIdWeapons[i]);
		m_pLevelWepons[i] = Utils::decodeInt(UserDefault::getInstance()->getStringForKey(levelWeaponKey.c_str()).c_str());
		if (m_pLevelWepons[i] > 0)
			m_iNumWeapons++;
	}
	/*string saveString = UserDefault::getInstance()->getStringForKey(KEY_DP_EXP).c_str();
	auto listValues = Utils::split(saveString, ',');
	m_pLevelStars.clear();
	for (int i = 0; i < listValues.size(); i++)
	{
		m_pLevelStars.push_back(atoi(listValues[i].c_str()));
	}*/
	LOGI("Finish Load User Profile!");
}

void UserProfile::resetData()
{
	LOGI("Reset User Profile!");
	// Gamer information
	UserDefault::getInstance()->setStringForKey(KEY_DP_MISSION, Utils::encodeInt(0));
	UserDefault::getInstance()->setStringForKey(KEY_DP_LEVEL, Utils::encodeInt(2));
	UserDefault::getInstance()->setStringForKey(KEY_DP_GEM, Utils::encodeInt(100));
	UserDefault::getInstance()->setStringForKey(KEY_DP_EXP, Utils::encodeInt(500));
	
	m_pLevelStars.clear();
	for (int i = 0; i < MAX_GAME_LEVEL * MAX_GAME_MISSION; i++)
	{
		m_pLevelStars.push_back(0);
	}
	string saveString = "";
	for (int i = 0; i < MAX_GAME_MISSION; i++)
	{
		for (int j = 0; j < MAX_GAME_LEVEL; j++)
		{
			saveString += StringUtils::toString(m_pLevelStars[i * MAX_GAME_LEVEL + j]) + ",";
		}
	}
	UserDefault::getInstance()->setStringForKey(KEY_DP_LEVELSTARS, saveString);
	//

	// Weapons Info
	for (int i = 0; i < m_pIdWeapons.size(); i++)
	{
		string levelWeaponKey = KEY_DP_LEVELWEAPONS + StringUtils::toString(m_pIdWeapons[i]);
		if (m_pIdWeapons[i] == ID_CANON203MM)
			UserDefault::getInstance()->setStringForKey(levelWeaponKey.c_str(), Utils::encodeInt(0));
		else
			UserDefault::getInstance()->setStringForKey(levelWeaponKey.c_str(), Utils::encodeInt(1));
	}
	LOGI("Finish Reset User Profile!");
}

void UserProfile::removeData()
{
	UserDefault::getInstance()->setBoolForKey(KEY_DP_SAVE, false);
}

void UserProfile::saveData()
{
	auto strEncode = Utils::encodeInt(1);
	UserDefault::getInstance()->setStringForKey(KEY_DP_SAVE, strEncode);
	CC_SAFE_DELETE(strEncode);

	strEncode = Utils::encodeInt(m_pCurrentMission);
	UserDefault::getInstance()->setStringForKey(KEY_DP_MISSION, strEncode);
	CC_SAFE_DELETE(strEncode);

	strEncode = Utils::encodeInt(m_pCurrntLevel);
	UserDefault::getInstance()->setStringForKey(KEY_DP_LEVEL, strEncode);
	CC_SAFE_DELETE(strEncode);

	strEncode = Utils::encodeInt(m_pGem);
	UserDefault::getInstance()->setStringForKey(KEY_DP_GEM, strEncode);
	CC_SAFE_DELETE(strEncode);

	strEncode = Utils::encodeInt(m_pEXP);
	UserDefault::getInstance()->setStringForKey(KEY_DP_EXP, strEncode);
	CC_SAFE_DELETE(strEncode);

	for (int i = 0; i < m_pIdWeapons.size(); i++)
	{
		string levelWeaponKey = KEY_DP_LEVELWEAPONS + StringUtils::toString(m_pIdWeapons[i]);
		strEncode = Utils::encodeInt(m_pLevelWepons[i]);
		UserDefault::getInstance()->setStringForKey(levelWeaponKey.c_str(), strEncode);
		CC_SAFE_DELETE(strEncode);
	}

	string saveString = "";
	for (int i = 0; i < MAX_GAME_MISSION; i++)
	{
		for (int j = 0; j < MAX_GAME_LEVEL; j++)
		{
			saveString += StringUtils::toString(m_pLevelStars[i * MAX_GAME_LEVEL + j]) + ",";
		}
	}
	UserDefault::getInstance()->setStringForKey(KEY_DP_LEVELSTARS, saveString);
}

void UserProfile::loadDataFromJson(string jsonFile)
{

}

void UserProfile::updateMission(int mission)
{
	if (mission != m_pCurrentMission)
	{
		auto strEncode = Utils::encodeInt(m_pCurrentMission);
		UserDefault::getInstance()->setStringForKey(KEY_DP_MISSION, strEncode);
		CC_SAFE_DELETE(strEncode);
	}
}

void UserProfile::updateLevel(int level)
{
	if (level != m_pCurrntLevel)
	{
		auto strEncode = Utils::encodeInt(m_pCurrntLevel);
		UserDefault::getInstance()->setStringForKey(KEY_DP_LEVEL, strEncode);
		CC_SAFE_DELETE(strEncode);
	}
}

void UserProfile::updateItem(int idItem, int value)
{
	auto freemiumBar = FREEMIUM_BAR;

	switch (idItem)
	{
	case ID_GEM:
		if (value != m_pGem)
		{
			if (value == -1)
				value = m_pGem;
			auto strEncode = Utils::encodeInt(value);
			UserDefault::getInstance()->setStringForKey(KEY_DP_GEM, strEncode);
			CC_SAFE_DELETE(strEncode);
			if (freemiumBar)
				freemiumBar->setValue(FI_GEM, value);
		}
		break;
	case ID_EXP:
		if (value != m_pEXP)
		{
			if (value == -1)
				value = m_pEXP;
			auto strEncode = Utils::encodeInt(value);
			UserDefault::getInstance()->setStringForKey(KEY_DP_EXP, strEncode);
			CC_SAFE_DELETE(strEncode);
			if (freemiumBar)
				freemiumBar->setValue(FI_EXP, value);
		}
		break;
	case ID_MACHINEGUN:
	case ID_CANON203MM:
	case ID_CANON37MM:
	case ID_FOGGUN:
	case ID_ROCKETS300:
	{
						  int currentLevel = getLevelById(idItem);
						  if (value != currentLevel)
						  {
							  string levelWeaponKey = KEY_DP_LEVELWEAPONS + StringUtils::toString(idItem);
							  setLevelById(idItem, value);
							  auto strEncode = Utils::encodeInt(value);
							  UserDefault::getInstance()->setStringForKey(levelWeaponKey.c_str(), strEncode);
							  CC_SAFE_DELETE(strEncode);
						  }
						  break;
	}
	}
}

int UserProfile::getLevelById(int id)
{
	for (int i = 0; i < m_pIdWeapons.size(); i++)
		if (m_pIdWeapons[i] == id)
			return m_pLevelWepons[i];
	return -1;
}

void UserProfile::setLevelById(int id, int value)
{
	for (int i = 0; i < m_pIdWeapons.size(); i++)
	if (m_pIdWeapons[i] == id)
		m_pLevelWepons[i] = value;
}