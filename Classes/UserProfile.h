#ifndef __USER_PROFILE_H__
#define __USER_PROFILE_H__

#include "Config.h"

#define KEY_DP_SAVE				"ID_DP_SAVE"
#define KEY_DP_MISSION			"ID_DP_MISSION"
#define KEY_DP_LEVEL			"ID_DP_LEVEL"
#define KEY_DP_GEM				"ID_DP_GEM"
#define KEY_DP_EXP				"ID_DP_EXP"
#define KEY_DP_NUMWEAPONS		"KEY_DP_NUMWEAPONS"
#define KEY_DP_IDWEAPONS		"KEY_DP_IDWEAPONS_"
#define KEY_DP_LEVELWEAPONS		"KEY_DP_LEVELWEAPONS"
#define KEY_DP_LEVELSTARS		"KEY_DP_LEVELSTARS"

#define KEY_DP_MACHINEGUN		"ID_DP_MACHINEGUN"
#define KEY_DP_CANON37MM		"ID_DP_CANON37MM"
#define KEY_DP_CANON203MM		"ID_DP_CANON203MM"
#define KEY_DP_ROCKETS300		"ID_DP_ROCKETS300"

#define KEY_LEVEL_STAR(mission, level)	"ID_LEVEL_STAR" + StringUtils::toString(mission) + StringUtils::toString(level)
#define MAX_GAME_MISSION		3
#define MAX_GAME_LEVEL			9

class UserProfile
{
public:
	static UserProfile* Get();

	int m_pCurrentMission;
	int m_pCurrntLevel;
	int m_pGem;
	int m_pEXP;
	int m_pRadianRadar;
	int m_iNumWeapons;

	std::vector<int> m_pLevelWepons;
	std::vector<int> m_pIdWeapons;
	std::vector<int> m_pLevelStars;

	void resetData();
	void removeData();
	void saveData();

	void updateMission(int mission);
	void updateLevel(int level);
	void updateItem(int idItem, int value = -1);
	int  getLevelById(int id);
	void setLevelById(int id, int value);
	void loadDataFromJson(string jsonFile);

private:
	bool checkData();
	void loadData();
	static UserProfile* s_pInstance;
	UserProfile();
};

#endif