#include "Utils.h"
#include "VisibleRect.h"
#include "MapGame.h"
#include "UserProfile.h"
#include "FreemiumBar.h"

std::vector<ItemData*> Utils::ITEM_DATA;

vector<string> Utils::split(string str, char delimiter)
{
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;

	while (getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}

	return internal;
}

MenuItemSprite* Utils::addButton(std::string up, std::string dn, std::string dis, ccMenuCallback callback)
{
	Sprite *itemUp, *itemDn, *itemDis;
	itemUp = Sprite::create(up);
	itemDn = Sprite::create(dn);
	itemDis = Sprite::create(dis);
	return MenuItemSprite::create(itemUp, itemDn, itemDis, callback);
}

vector<Vec2> Utils::get5Direction(Vec2 center, float distan)
{
	vector<Vec2> outPut;

	auto vec_Up = Vec2(0, 1);

	for (int i = 0; i < 5; i++)
	{
		if (i != 0)
			vec_Up.rotate(Vec2(0, 0), CC_DEGREES_TO_RADIANS(72));
		auto item = vec_Up * distan;
		outPut.push_back(item + center);
	}
	return outPut;
}

float Utils::angle2Vector(Vec2 vec1, Vec2 vec2)
{
	return vec1.dot(vec2) / (2 * vec1.length() * vec2.length());
}

float Utils::calculateDamage(float armor, float damage)
{
	auto randPercent = RandomHelper::random_int(2, 7);
	auto outPut = damage - armor * randPercent / 10;
	return (outPut > 0 ? outPut : 0.0f);
}

Vec2 Utils::convertIntToVec2(MapGame* map, int index)
{
	auto width = map->getWidth();
	auto height = map->getWidth();
	switch (index)
	{
	case 0:		// Top Left
		return Vec2(0, height);
	case 1:		// Top
		return Vec2(width / 2, height);
	case 2:		// Top Right
		return Vec2(width, height);
	case 3:		// Right
		return Vec2(width, height/2);
	case 4:		// Bottom Right
		return Vec2(width, 0);
	case 5:		// Bottom
		return Vec2(width / 2, 0);
	case 6:		// Bottom Left
		return Vec2(0, 0);
	case 7:		// Left
		return Vec2(0, height / 2);
	default:
		return Vec2(width / 2, height / 2);
	}
}

char* Utils::encodeInt(int score)
{
	char *out;
	std::string str = StringUtils::format("%d", score);
	size_t ret = 10;
	base64Encode((unsigned char*)str.c_str(), (unsigned int)ret, &out);
	char *_out = new char[18];
	_out[0] = out[0];
	_out[1] = 'q';
	_out[2] = 'K';
	for (int i = 3; i < 18; i++)
		_out[i] = out[i - 2];
	return _out;
}

int Utils::decodeInt(const char* out)
{
	char *_out = new char[16];
	_out[0] = out[0];
	for (int i = 1; i < 16; i++)
		_out[i] = out[i + 2];
	unsigned char * decodedData = nullptr;
	int decodedDataLen = base64Decode((unsigned char*)_out, (unsigned int)strlen(_out), &decodedData);
	if (!is_number((char*)decodedData)) return 0;
	return atoi((char*)decodedData);
}

int Utils::getTagItem(float width, float height)
{
	int X = width / 35;
	int Y = height / 35;
	if (X == 1 && Y == 1)
		return MI_1X1;
	else if (X == 2 && Y == 2)
		return MI_2X2;
	else if (X == 2 && Y == 1)
		return MI_2X1;
	else
		return MI_1X2;
}

Size Utils::getSizeFromTag(int tag)
{
	switch (tag)
	{
	case MI_1X1:
		return Size(1, 1);
	case MI_1X2:
		return Size(1, 2);
	case MI_2X1:
		return Size(2, 1);
	case MI_2X2:
		return Size(2, 2);
	}
}

bool Utils::is_number(char * s)
{
	if (strlen(s) <= 0) return false;

	for (int i = 0; i < strlen(s); i++)
	if (s[i] < -1 && s[i] > 255 && !isdigit(s[i])) return false;

	return true;
}

void Utils::getInfoItem()
{
	if (Utils::ITEM_DATA.size() != 0)
		return;
	
	auto data = FileUtils::getInstance()->getValueMapFromFile("Weapons/Weapons.plist");

	auto NumItems = data.at("NumberItem").asInt();
	auto enemyTypes = data.at("Items").asValueVector();
	for (int i = 0, imax = NumItems; i < imax; i++)
	{
		auto enemy = enemyTypes.at(i).asValueMap();
		Utils::ITEM_DATA.push_back(genWeaponData(enemy));
	}
}

ItemData* Utils::genWeaponData(ValueMap enemy)
{
	ItemData* d = new ItemData();
	d->Id = convertStringToId(enemy.at("Id").asString());
	d->Name = enemy.at("Name").asString();
	d->Value = enemy.at("Value").asInt();
	d->Range = enemy.at("Range").asInt();
	d->SpeedShoot = enemy.at("SpeedShoot").asInt();
	d->SpeedMove = enemy.at("SpeedMove").asInt();
	d->SpeedBullet = enemy.at("SpeedBullet").asInt();
	d->Damage = enemy.at("Damage").asInt();
	d->MaxHeath = enemy.at("MaxHeath").asInt();
	d->Armor = enemy.at("Armor").asInt();
	return d;
}

ItemData* Utils::getItemById(int id)
{
	Utils::getInfoItem();
	for (auto item : ITEM_DATA)
	{
		if (item->Id == id)
			return item;
	}
	return 0;
}

map<string, string>		Utils::genItemInformation(int idItem)
{
	auto item = getItemById(idItem);
	std::map<string, string> mapInfor;
	mapInfor.insert(std::pair<string, string>(KEY_GAMEUI_TITLE_TEXT, item->Name));	
	mapInfor.insert(std::pair<string, string>(KEY_GAMEUI_PANEL_RANGE, StringUtils::toString(item->Range)));
	mapInfor.insert(std::pair<string, string>(KEY_GAMEUI_PANEL_RELOAD, StringUtils::toString(item->SpeedShoot)));
	mapInfor.insert(std::pair<string, string>(KEY_GAMEUI_PANEL_SPEEDBULLET, StringUtils::toString(item->SpeedBullet)));
	mapInfor.insert(std::pair<string, string>(KEY_GAMEUI_PANEL_DAMAGE, StringUtils::toString(item->Damage)));
	mapInfor.insert(std::pair<string, string>(KEY_GAMEUI_PANEL_HEATH, StringUtils::toString(item->MaxHeath)));
	mapInfor.insert(std::pair<string, string>(KEY_GAMEUI_PANEL_SHEILD, StringUtils::toString(item->Armor)));
	return mapInfor;
}

std::map<string, string> Utils::getItemDataById(int id)
{
	std::map<string, string> out;

	ItemData* item = Utils::getItemById(id);

	out.insert(std::pair<string, string>("Name", item->Name));
	out.insert(std::pair<string, string>("Range", StringUtils::toString(item->Range)));
	out.insert(std::pair<string, string>("Speed Shot", StringUtils::toString(item->SpeedShoot)));
	out.insert(std::pair<string, string>("Speed Move", StringUtils::toString(item->SpeedMove)));
	out.insert(std::pair<string, string>("Speed Bullet", StringUtils::toString(item->SpeedBullet)));
	out.insert(std::pair<string, string>("Damage", StringUtils::toString(item->Damage)));
	out.insert(std::pair<string, string>("Max Heath", StringUtils::toString(item->MaxHeath)));
	out.insert(std::pair<string, string>("Armor", StringUtils::toString(item->Armor)));

	return out;
}

int Utils::convertStringToId(string name)
{
	if (name.compare("ID_FISHINGBOAT") == 0)
		return ID_FISHINGBOAT;
	else if (name.compare("ID_COASTGUARD") == 0)
		return ID_COASTGUARD;
	else if (name.compare("ID_DESTROYER956") == 0)
		return ID_DESTROYER956;
	else if (name.compare("ID_AIRCRAFTCARRIERSHIP") == 0)
		return ID_AIRCRAFTCARRIERSHIP;
	else if (name.compare("ID_CANON37MM") == 0)
		return ID_CANON37MM;
	else if (name.compare("ID_CANON203MM") == 0)
		return ID_CANON203MM;
	else if (name.compare("ID_MACHINEGUN") == 0)
		return ID_MACHINEGUN;
	else if (name.compare("ID_ROCKETS300") == 0)
		return ID_ROCKETS300;
	else if (name.compare("ID_FOGGUN") == 0)
		return ID_FOGGUN;
	return -1;
}

Vec2* Utils::convertPathMoving(ValueVector &points, Vec2 pointStart)
{
	Vec2 *outPut = new Vec2[points.size()];
	int n = 0;
	for (auto point : points)
	{
		auto value = point.asValueMap();
		outPut[n++] = Vec2(pointStart.x + value["x"].asFloat(), pointStart.y + value["y"].asFloat());
	}

	for (int i = 0; i < n; i++)
	{
		if (outPut[i].y > pointStart.y)
			outPut[i].y = pointStart.y - abs(outPut[i].y - pointStart.y);
		else
			outPut[i].y = pointStart.y + abs(outPut[i].y - pointStart.y);
	}
	return outPut;
}

std::vector<Vec2> Utils::getPositionRound(Size table, int numItems)
{
	std::vector<Vec2> listPos;
	auto angle = 360.0 / numItems;
	Vec2 origin = Vec2(-table.width / 2, 0);
	for (int i = 0; i < numItems; i++)
	{
		listPos.push_back(origin.rotateByAngle(Vec2::ZERO, CC_DEGREES_TO_RADIANS(i * angle)));
	}
	return listPos;
}

void Utils::loadSpriteCache()
{
	for (int i = 0; i < NUMBER_BULLETS_TYPE; i++)
	{
		string strTexture = SPRITE_SUBITEMS_BULLET_PREFIX + StringUtils::toString(i) + ".png";
		auto temp = Sprite::create(strTexture);
		auto size = temp->getContentSize();
		auto frame = SpriteFrame::create(strTexture, Rect(0, 0, size.width, size.height)); //we assume that the sprites' dimentions are 40*40 rectangles.
		SpriteFrameCache::getInstance()->addSpriteFrame(frame, KEY_BULLET_TYPE(i));
	}
}

float Utils::angleTwoVector(Vec2 pos, Vec2 target)
{
	auto vec_huyen = target - pos;
	vec_huyen.normalize();
	auto vec_vuong = Vec2(0, 1);
	auto goc = CC_RADIANS_TO_DEGREES(ccpAngle(vec_huyen, vec_vuong));
	return goc;
}

string Utils::getSpriteNameBullet(int id)
{
	switch (id)
	{
	case ID_SMALLBULLET:
		return "BulletType0";
	case ID_FOGBULLET:
		return "BulletType1";
	case ID_CANONBULLET:
		return "BulletType1";
	case ID_ROCKET:
		return "BulletType4";
	default:
		return "BulletType0";
	}
	return "BulletType0";
}

string Utils::getNameById(int id)
{
	if (Utils::ITEM_DATA.size() == 0)
		Utils::getInfoItem();

	for (auto item : ITEM_DATA)
	{
		if (item->Id == id)
			return item->Name;
	}
	return "";
}

int Utils::getValueById(int id, bool origin)
{
	if (Utils::ITEM_DATA.size() == 0)
		Utils::getInfoItem();

	for (auto item : ITEM_DATA)
	{
		if (item->Id == id)
		{
			if (origin)
				return item->Value;
			int currentLevel = UserProfile::Get()->getLevelById(id);
			return getValueOldest(item->Value, currentLevel + 1);
		}
	}
	return -1;
}

int Utils::getValueOldest(int value, int level)
{
	int outValue = value;
	for (int i = 1; i <= level; i++)
	{
		if (i == 1)
			continue;
		outValue += outValue / 2;
	}
	return outValue;
}

string Utils::getNameResourceByType(int type)
{
	switch (type)
	{
	case ID_GEM:
		return "GEM";
	case ID_EXP:
		return "EXP";
	case ID_CASH:
		return "CASH";
	}
}

bool Utils::checkBuyItem(int costItem)
{
	auto cash = FREEMIUM_BAR->getValue(FI_CASH) - costItem;
	if (cash < 0)
		return false;
	else
	{
		FREEMIUM_BAR->setValue(FI_CASH, cash);
		return true;
	}

}