#ifndef __UTILS_H__
#define __UTILS_H__

#include "Config.h"

class MapGame;
class RadarGame;

struct ItemData
{
	int Id;
	string	Name;
	int		Range;
	int		Value;
	int		SpeedShoot;
	int		SpeedMove;
	int		SpeedBullet;
	float	Damage;
	float	MaxHeath;
	float	Armor;

	ItemData() { }
};

class Utils
{
public:
	static vector<string>			split(string str, char delimiter);
	static MenuItemSprite*			addButton(std::string up, std::string dn, std::string dis, ccMenuCallback callback);
	static std::vector<Vec2>		get5Direction(Vec2 center, float distan);
	static float					angle2Vector(Vec2 vec1, Vec2 vec2);
	static float					calculateDamage(float arrmor, float damage);
	static Vec2						convertIntToVec2(MapGame* map, int index);
	
	// For Data
	static char*					encodeInt(int score);
	static int						decodeInt(const char* score);
	static bool						is_number(char * s);
	static ItemData*				genWeaponData(ValueMap enemy);
	static void						getInfoItem();
	static std::vector<ItemData*>	ITEM_DATA;
	static std::vector<Vec2>		getPositionRound(Size table, int numItems);
	static int						convertStringToId(string name);
	static ItemData*				getItemById(int id);
	static std::map<string, string> getItemDataById(int id);
	static int						getTagItem(float width, float height);
	static Size						getSizeFromTag(int tag);
	static void						loadSpriteCache();
	static float					angleTwoVector(Vec2 pos, Vec2 target);
	static string					getSpriteNameBullet(int id);
	static string					getNameById(int id);
	static int						getValueById(int id, bool origin=false);
	static int						getValueOldest(int value, int level);
	static string					getNameResourceByType(int type);
	static bool						checkBuyItem(int costItem);
	static map<string, string>		genItemInformation(int idItem);
	static Vec2*					convertPathMoving(ValueVector &points, Vec2 pointStart);
};

#endif