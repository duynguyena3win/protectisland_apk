#include "WarningEffect.h"
#include"Utils.h"

WarningEffect* WarningEffect::create(Layer* layer)
{
	auto gem = new WarningEffect();
	gem->m_pLayer = layer;
	if (gem->init())
	{
		return gem;
	}
	CC_SAFE_DELETE(gem);
	return NULL;
}

bool WarningEffect::init()
{
	if (!EffectEntity::init())
	{
		return false;
	}
	auto image = Sprite::create(SPRITE_EFFECT_CASH_ITEM);
	image->setScale(SCALE_EFFECT_SPRITE);
	m_pNode->addChild(image);
	m_pNode->setContentSize(image->getContentSize());

	m_pLabelText = Label::createWithBMFont(FONT_GAME, StringUtils::format("000"));
	m_pLabelText->setScale(FONT_GAME_SCALE);
	m_pLabelText->setPosition(Vec2(38.0f, 0.0f));
	m_pNode->addChild(m_pLabelText);

	m_iID = ID_CASH;
	setActive(false);
	return true;
}

void WarningEffect::setPosition(Vec2 pos)
{
	EffectEntity::setPosition(pos);
	EffectEntity::runAction(createAction());
}

Action* WarningEffect::createAction()
{
	Vec2 pos = getPosition();
	m_pNode->setPosition(pos + Vec2(20, 5));
	setActive(true);
	m_pNode->setScale(0.6f);

	auto scaleIn = ScaleTo::create(0.8f, 1.0f);
	auto scaleOut = ScaleTo::create(0.8f, 1.2f);
	return Repeat::create(Sequence::create(scaleIn, scaleOut, NULL), 10);
}

void WarningEffect::addEvents()
{
	auto listener = cocos2d::EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);

	listener->onTouchBegan = [&](cocos2d::Touch* touch, cocos2d::Event* event)
	{
		Vec2 p = touch->getLocation();
		Rect rect = m_pNode->getBoundingBox();

		if (rect.containsPoint(p))
		{
			return true; // to indicate that we have consumed it.
		}

		return false; // we did not consume this event, pass thru.
	};

	listener->onTouchEnded = [=](cocos2d::Touch* touch, cocos2d::Event* event)
	{
		WarningEffect::touchEvent();
	};

	cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(listener, 30);
}

void WarningEffect::touchEvent()
{
	m_pNode->stopAllActions();
	setActive(false);
}