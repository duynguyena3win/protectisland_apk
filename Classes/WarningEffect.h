#ifndef __WARNINGEFFECT_H__
#define __WARNINGEFFECT_H__

#include "EffectEntity.h"

class WarningEffect : public EffectEntity
{
public:
	static WarningEffect* create(Layer* layer);
	bool init();
	void setPosition(Vec2 pos);
	Action* createAction();
	void addEvents();
	void runAction() { }
	void touchEvent();
};

#endif